USE [master]
GO
/****** Object:  Database [rentaAutoSensacionDB]    Script Date: 19/4/2017 03:11:32 ******/
CREATE DATABASE [rentaAutoSensacionDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'rentaAutoSensacionDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\rentaAutoSensacionDB.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'rentaAutoSensacionDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\rentaAutoSensacionDB_log.ldf' , SIZE = 1072KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [rentaAutoSensacionDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [rentaAutoSensacionDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [rentaAutoSensacionDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET RECOVERY FULL 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET  MULTI_USER 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [rentaAutoSensacionDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [rentaAutoSensacionDB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'rentaAutoSensacionDB', N'ON'
GO
USE [rentaAutoSensacionDB]
GO
USE [rentaAutoSensacionDB]
GO
/****** Object:  Sequence [dbo].[SecuenciaFacturas]    Script Date: 19/4/2017 03:11:32 ******/
CREATE SEQUENCE [dbo].[SecuenciaFacturas] 
 AS [bigint]
 START WITH 100
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
/****** Object:  Table [dbo].[automoviles]    Script Date: 19/4/2017 03:11:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[automoviles](
	[placa] [varchar](6) NOT NULL,
	[VIN] [nvarchar](17) NOT NULL,
	[idMarcaAuto] [int] NOT NULL,
	[modelo] [nvarchar](50) NOT NULL,
	[cilindraje] [decimal](5, 0) NOT NULL,
	[numPasajeros] [decimal](2, 0) NOT NULL,
	[idTipoAuto] [int] NOT NULL,
	[estaActivo] [bit] NOT NULL,
 CONSTRAINT [PKAutomoviles] PRIMARY KEY CLUSTERED 
(
	[placa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cliente]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cliente](
	[idPersona] [nvarchar](20) NOT NULL,
	[fechaIngreso] [date] NOT NULL,
	[idRolUsuario] [int] NOT NULL,
	[contrasenna] [nvarchar](15) NOT NULL,
 CONSTRAINT [PKCliente] PRIMARY KEY CLUSTERED 
(
	[idPersona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[consultas]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[consultas](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombreCompleto] [nvarchar](150) NOT NULL,
	[correo] [nvarchar](100) NOT NULL,
	[consulta] [nvarchar](500) NOT NULL,
 CONSTRAINT [PKConsultas] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[detalleFactura]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[detalleFactura](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idAutomovil] [varchar](6) NOT NULL,
	[idEncabezadoFactura] [int] NOT NULL,
	[cantidadDias] [int] NOT NULL,
	[precio] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PKDetalleFactura] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[empleado]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[empleado](
	[idPersona] [nvarchar](20) NOT NULL,
	[fechaContratacion] [date] NOT NULL,
	[estaActivo] [bit] NOT NULL,
	[idRolUsuario] [int] NOT NULL,
	[contrasenna] [nvarchar](15) NOT NULL,
 CONSTRAINT [PKEmpleado] PRIMARY KEY CLUSTERED 
(
	[idPersona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[encabezadoFactura]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[encabezadoFactura](
	[id] [int] NOT NULL,
	[idEmpleado] [nvarchar](20) NOT NULL,
	[idCliente] [nvarchar](20) NOT NULL,
	[fecha] [date] NOT NULL,
	[idNumTarjeta] [numeric](16, 0) NULL,
 CONSTRAINT [PKEncabezadoFactura] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[gasto]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gasto](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [nvarchar](200) NOT NULL,
	[fechaGasto] [date] NOT NULL,
	[fechaRegistro] [date] NOT NULL,
	[idEmpleado] [nvarchar](20) NOT NULL,
	[idTipoGasto] [int] NOT NULL,
 CONSTRAINT [PKGasto] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[marcaAuto]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[marcaAuto](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [nvarchar](100) NOT NULL,
 CONSTRAINT [PKMarcaAutoprimary] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[persona]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[persona](
	[id] [nvarchar](20) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Apellido1] [nvarchar](50) NOT NULL,
	[Apellido2] [nvarchar](50) NOT NULL,
	[fechaNacimiento] [date] NOT NULL,
	[telContacto] [nvarchar](8) NOT NULL,
	[correo] [nvarchar](100) NOT NULL,
 CONSTRAINT [PKPersona] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[reservacion]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[reservacion](
	[id] [int] IDENTITY(1,100) NOT NULL,
	[idCliente] [nvarchar](20) NOT NULL,
	[fechaReservacion] [date] NOT NULL,
	[fechaEntrega] [date] NOT NULL,
	[fechaDevolucion] [date] NOT NULL,
	[idAuto] [varchar](6) NOT NULL,
	[totalReservación] [decimal](18, 0) NOT NULL,
	[cancelada] [bit] NOT NULL,
	[vencida] [bit] NOT NULL,
 CONSTRAINT [PKReservacion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rolUsuario]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rolUsuario](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [nvarchar](100) NOT NULL,
 CONSTRAINT [PKRolUsuario] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tarjetaPago]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tarjetaPago](
	[numeroTarjeta] [numeric](16, 0) NOT NULL,
	[nombreTitular] [nvarchar](70) NULL,
	[mes] [int] NULL,
	[ano] [int] NULL,
	[CVV] [nchar](10) NULL,
 CONSTRAINT [PK_tarjetaPago] PRIMARY KEY CLUSTERED 
(
	[numeroTarjeta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tipoAuto]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tipoAuto](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [nvarchar](55) NOT NULL,
	[precio] [money] NULL,
 CONSTRAINT [PKTipoAuto] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tipoGasto]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tipoGasto](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [nvarchar](100) NOT NULL,
 CONSTRAINT [PKTipoGasto] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[automoviles]  WITH CHECK ADD  CONSTRAINT [FK_automoviles_marcaAuto] FOREIGN KEY([idMarcaAuto])
REFERENCES [dbo].[marcaAuto] ([id])
GO
ALTER TABLE [dbo].[automoviles] CHECK CONSTRAINT [FK_automoviles_marcaAuto]
GO
ALTER TABLE [dbo].[automoviles]  WITH CHECK ADD  CONSTRAINT [FK_automoviles_tipoAuto] FOREIGN KEY([idTipoAuto])
REFERENCES [dbo].[tipoAuto] ([id])
GO
ALTER TABLE [dbo].[automoviles] CHECK CONSTRAINT [FK_automoviles_tipoAuto]
GO
ALTER TABLE [dbo].[cliente]  WITH CHECK ADD  CONSTRAINT [FK_cliente_rolUsuario] FOREIGN KEY([idRolUsuario])
REFERENCES [dbo].[rolUsuario] ([id])
GO
ALTER TABLE [dbo].[cliente] CHECK CONSTRAINT [FK_cliente_rolUsuario]
GO
ALTER TABLE [dbo].[detalleFactura]  WITH CHECK ADD  CONSTRAINT [FK_detalleFactura_automoviles] FOREIGN KEY([idAutomovil])
REFERENCES [dbo].[automoviles] ([placa])
GO
ALTER TABLE [dbo].[detalleFactura] CHECK CONSTRAINT [FK_detalleFactura_automoviles]
GO
ALTER TABLE [dbo].[detalleFactura]  WITH CHECK ADD  CONSTRAINT [FK_detalleFactura_encabezadoFactura] FOREIGN KEY([idEncabezadoFactura])
REFERENCES [dbo].[encabezadoFactura] ([id])
GO
ALTER TABLE [dbo].[detalleFactura] CHECK CONSTRAINT [FK_detalleFactura_encabezadoFactura]
GO
ALTER TABLE [dbo].[empleado]  WITH CHECK ADD  CONSTRAINT [FK_empleado_rolUsuario] FOREIGN KEY([idRolUsuario])
REFERENCES [dbo].[rolUsuario] ([id])
GO
ALTER TABLE [dbo].[empleado] CHECK CONSTRAINT [FK_empleado_rolUsuario]
GO
ALTER TABLE [dbo].[encabezadoFactura]  WITH CHECK ADD  CONSTRAINT [FK_encabezadoFactura_cliente] FOREIGN KEY([idCliente])
REFERENCES [dbo].[cliente] ([idPersona])
GO
ALTER TABLE [dbo].[encabezadoFactura] CHECK CONSTRAINT [FK_encabezadoFactura_cliente]
GO
ALTER TABLE [dbo].[encabezadoFactura]  WITH CHECK ADD  CONSTRAINT [FK_encabezadoFactura_empleado] FOREIGN KEY([idEmpleado])
REFERENCES [dbo].[empleado] ([idPersona])
GO
ALTER TABLE [dbo].[encabezadoFactura] CHECK CONSTRAINT [FK_encabezadoFactura_empleado]
GO
ALTER TABLE [dbo].[encabezadoFactura]  WITH CHECK ADD  CONSTRAINT [FK_encabezadoFactura_tarjetaPago] FOREIGN KEY([idNumTarjeta])
REFERENCES [dbo].[tarjetaPago] ([numeroTarjeta])
GO
ALTER TABLE [dbo].[encabezadoFactura] CHECK CONSTRAINT [FK_encabezadoFactura_tarjetaPago]
GO
ALTER TABLE [dbo].[gasto]  WITH CHECK ADD  CONSTRAINT [FK_gasto_empleado] FOREIGN KEY([idEmpleado])
REFERENCES [dbo].[empleado] ([idPersona])
GO
ALTER TABLE [dbo].[gasto] CHECK CONSTRAINT [FK_gasto_empleado]
GO
ALTER TABLE [dbo].[gasto]  WITH CHECK ADD  CONSTRAINT [FK_gasto_tipoGasto] FOREIGN KEY([idTipoGasto])
REFERENCES [dbo].[tipoGasto] ([id])
GO
ALTER TABLE [dbo].[gasto] CHECK CONSTRAINT [FK_gasto_tipoGasto]
GO
ALTER TABLE [dbo].[reservacion]  WITH CHECK ADD  CONSTRAINT [FK_reservacion_automoviles] FOREIGN KEY([idAuto])
REFERENCES [dbo].[automoviles] ([placa])
GO
ALTER TABLE [dbo].[reservacion] CHECK CONSTRAINT [FK_reservacion_automoviles]
GO
ALTER TABLE [dbo].[reservacion]  WITH CHECK ADD  CONSTRAINT [FK_reservacion_cliente] FOREIGN KEY([idCliente])
REFERENCES [dbo].[cliente] ([idPersona])
GO
ALTER TABLE [dbo].[reservacion] CHECK CONSTRAINT [FK_reservacion_cliente]
GO
/****** Object:  StoredProcedure [dbo].[PA_ConsecutivoFactura]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PA_ConsecutivoFactura]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT NEXT VALUE FOR SecuenciaFacturas
END

GO
/****** Object:  StoredProcedure [dbo].[PA_ExisteTarjetaPago]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[PA_ExisteTarjetaPago]
@numeroTarjeta numeric(16,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT count(numeroTarjeta) from tarjetaPago  where numeroTarjeta=@numeroTarjeta
END

GO
/****** Object:  StoredProcedure [dbo].[PA_GuardarTipoGasto]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PA_GuardarTipoGasto]
	@descripcion nvarchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


INSERT INTO [dbo].[tipoGasto]
           ([descripcion])
     VALUES
           (@descripcion)



END


GO
/****** Object:  StoredProcedure [dbo].[PA_InsertarAutomoviles]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PA_InsertarAutomoviles]
	-- Add the parameters for the stored procedure here
	        @placa varchar(6)
           ,@VIN nvarchar(17)
           ,@idMarcaAuto int
           ,@modelo nvarchar(50)
           ,@cilindraje decimal(5,0)
           ,@numPasajeros decimal(2,0)
           ,@idTipoAuto int
           ,@estaActivo bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

INSERT INTO [dbo].[automoviles]
           ([placa]
           ,[VIN]
           ,[idMarcaAuto]
           ,[modelo]
           ,[cilindraje]
           ,[numPasajeros]
           ,[idTipoAuto]
           ,[estaActivo])
     VALUES
           (@placa
		   ,@VIN
		   ,@idMarcaAuto
		   ,@modelo
		   ,@cilindraje
		   ,@numPasajeros
		   ,@idTipoAuto
		   ,@estaActivo)

END


GO
/****** Object:  StoredProcedure [dbo].[PA_InsertarCliente]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PA_InsertarCliente]
	-- Add the parameters for the stored procedure here
	       @idPersona nvarchar(20),
           @fechaIngreso date, 
           @idRolUsuario int,
           @contrasenna nvarchar(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

INSERT INTO [dbo].[cliente]
           ([idPersona]
           ,[fechaIngreso]
           ,[idRolUsuario]
           ,[contrasenna])
     VALUES
           (@idPersona
		   ,@fechaIngreso
		   ,@idRolUsuario
		   ,@contrasenna)

END


GO
/****** Object:  StoredProcedure [dbo].[PA_InsertarDetalleFact]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[PA_InsertarDetalleFact]
	-- Add the parameters for the stored procedure here
	        @id int
           ,@idAutomovil nvarchar(6)
           ,@idEncabezadoFactura int
           ,@cantidadDias int
           ,@precio numeric(18,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

INSERT INTO [dbo].[detalleFactura]
           (id
           ,idAutomovil
           ,idEncabezadoFactura
           ,cantidadDias
           ,precio)
     VALUES
			( @id 
           ,@idAutomovil  
           ,@idEncabezadoFactura  
           ,@cantidadDias  
           ,@precio  )


END


GO
/****** Object:  StoredProcedure [dbo].[PA_InsertarEmpleado]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PA_InsertarEmpleado]
	-- Add the parameters for the stored procedure here
	        @idPersona nvarchar(20)
           ,@fechaContratacion date
           ,@estaActivo bit
           ,@idRolUsuario int
           ,@contrasenna nvarchar(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


INSERT INTO [dbo].[empleado]
           ([idPersona]
           ,[fechaContratacion]
           ,[estaActivo]
           ,[idRolUsuario]
           ,[contrasenna])
     VALUES
           (@idPersona
		   ,@fechaContratacion
		   ,@estaActivo
		   ,@idRolUsuario
		   ,@contrasenna)




END


GO
/****** Object:  StoredProcedure [dbo].[PA_InsertarEncabezadoFact]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PA_InsertarEncabezadoFact]
	-- Add the parameters for the stored procedure here
	        @id int
           ,@idEmpleado nvarchar(20)
           ,@idCliente nvarchar(20)
           ,@fecha date
           ,@idNumTarjeta numeric(16,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

INSERT INTO [dbo].[encabezadoFactura]
           (id
           ,idEmpleado
           ,idCliente
           ,fecha
           ,idNumTarjeta)
     VALUES
			(@id 
           ,@idEmpleado 
           ,@idCliente 
           ,@fecha
           ,@idNumTarjeta )


END


GO
/****** Object:  StoredProcedure [dbo].[PA_InsertarGasto]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PA_InsertarGasto]
	-- Add the parameters for the stored procedure here
	        @descripcion nvarchar(200)
           ,@fechaGasto date
           ,@fechaRegistro date
           ,@idEmpleado nvarchar(20)
           ,@idTipoGasto int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


INSERT INTO [dbo].[gasto]
           ([descripcion]
           ,[fechaGasto]
           ,[fechaRegistro]
           ,[idEmpleado]
           ,[idTipoGasto])
     VALUES
           (@descripcion
		   ,@fechaGasto
		   ,@fechaRegistro
		   ,@idEmpleado
		   ,@idTipoGasto)




END


GO
/****** Object:  StoredProcedure [dbo].[PA_InsertarPersona]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PA_InsertarPersona]
	-- Add the parameters for the stored procedure here
	        @id nvarchar(20)
           ,@Nombre nvarchar(50)
           ,@Apellido1 nvarchar(50)
           ,@Apellido2 nvarchar(50)
           ,@fechaNacimiento date
           ,@telContacto nvarchar(8)
           ,@correo nvarchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

INSERT INTO [dbo].[persona]
           ([id]
           ,[Nombre]
           ,[Apellido1]
           ,[Apellido2]
           ,[fechaNacimiento]
           ,[telContacto]
           ,[correo])
     VALUES
           (@id
		   ,@Nombre
		   ,@Apellido1
		   ,@Apellido2
		   ,@fechaNacimiento
		   ,@telContacto
		   ,@correo)


END


GO
/****** Object:  StoredProcedure [dbo].[PA_InsertarReservacion]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PA_InsertarReservacion]
	-- Add the parameters for the stored procedure here
	        @idCliente nvarchar(20)
           ,@fechaReservacion date
           ,@fechaEntrega date
           ,@fechaDevolucion date
           ,@idAuto varchar(6)
           ,@totalReservacion decimal(18,0)
           ,@cancelada bit
           ,@vencida bit
		   ,@idFactura int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

INSERT INTO [dbo].[reservacion]
           ([idCliente]
           ,[fechaReservacion]
           ,[fechaEntrega]
           ,[fechaDevolucion]
           ,[idAuto]
           ,[totalReservación]
           ,[cancelada]
           ,[vencida]
		   ,[idFactura])
     VALUES
           (@idCliente
		   ,@fechaReservacion
		   ,@fechaEntrega
		   ,@fechaDevolucion
		   ,@idAuto
		   ,@totalReservacion
		   ,@cancelada
		   ,@vencida
		   ,@idFactura)


END
GO
/****** Object:  StoredProcedure [dbo].[PA_InsertarRoles]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PA_InsertarRoles]
	-- Add the parameters for the stored procedure here
	@descripcion nvarchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

INSERT INTO [dbo].[rolUsuario]
           ([descripcion])
     VALUES
           (@descripcion)


END


GO
/****** Object:  StoredProcedure [dbo].[PA_InsertarTarjetaPago]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PA_InsertarTarjetaPago]
	-- Add the parameters for the stored procedure here
	        @numTarjeta numeric(16,0)
           ,@nombreTitular nvarchar(70)
           ,@mes int
           ,@ano int
           ,@CVV int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

INSERT INTO [dbo].[tarjetaPago]
           (numeroTarjeta
           ,nombreTitular
           ,mes
           ,ano
           ,CVV)
     VALUES
			( @numTarjeta  
           ,@nombreTitular  
           ,@mes  
           ,@ano  
           ,@CVV)
END


GO
/****** Object:  StoredProcedure [dbo].[PA_ModificarAutos]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PA_ModificarAutos]
	-- Add the parameters for the stored procedure here
	@placa varchar(6),
	@idMarcaAuto int,
	@modelo nvarchar(50),
	@cilindraje decimal(5,0),
	@numPasajeros decimal(2,0),
	@idTipoAuto int,
	@estaActivo bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


UPDATE [dbo].[automoviles]
   SET [idMarcaAuto] = @idMarcaAuto
      ,[modelo] = @modelo
      ,[cilindraje] = @cilindraje
      ,[numPasajeros] = @numPasajeros
      ,[idTipoAuto] = @idTipoAuto
      ,[estaActivo] = @estaActivo
 WHERE placa=@placa





END


GO
/****** Object:  StoredProcedure [dbo].[PA_ModificarRoles]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PA_ModificarRoles]
	-- Add the parameters for the stored procedure here
	@id int,
	@descripcion nvarchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE [dbo].[rolUsuario]
   SET [descripcion] = @descripcion
 WHERE id=@id


END


GO
/****** Object:  StoredProcedure [dbo].[PA_ModificarTarjetaPago]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PA_ModificarTarjetaPago]
	-- Add the parameters for the stored procedure here
	        @numTarjeta numeric(16,0)
           ,@nombreTitular nvarchar(70)
           ,@mes int
           ,@ano int
           ,@CVV int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE  [dbo].[tarjetaPago] SET nombreTitular=@nombreTitular
           ,mes=@mes
           ,ano=@ano
           ,CVV=@CVV WHERE numeroTarjeta= @numTarjeta

END


GO

GO
/****** Object:  StoredProcedure [dbo].[PA_InsertarReservacion]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PA_ModificarReservacion]
	-- Add the parameters for the stored procedure here
	        @idReservacion int
           ,@fechaEntrega date
           ,@fechaDevolucion date
           ,@idAuto varchar(6)
           ,@totalReservacion decimal(18,0)
		   ,@idFactura int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

update reservacion set 
           [fechaEntrega]=@fechaEntrega
           ,[fechaDevolucion]=@fechaDevolucion
           ,[idAuto]=@idAuto
           ,[totalReservación]=@totalReservacion
		   ,[idFactura]=@idFactura where id=@idReservacion

END
GO
/****** Object:  StoredProcedure [dbo].[PA_ModificarTarjetaPago]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PA_ModificarFactura_Anular]
	-- Add the parameters for the stored procedure here
	        @idFactura int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE  [dbo].[encabezadoFactura] SET activa=0 where encabezadoFactura.id=@idFactura
END



/****** Object:  StoredProcedure [dbo].[PA_ModificarTarjetaPago]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PA_SeleccionarTarjetaXIdFactura]
	-- Add the parameters for the stored procedure here
	        @idFactura int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT        tarjetaPago.numeroTarjeta
FROM            encabezadoFactura INNER JOIN
                         tarjetaPago ON encabezadoFactura.idNumTarjeta = tarjetaPago.numeroTarjeta where encabezadoFactura.id=@idFactura



END


GO

/****** Object:  StoredProcedure [dbo].[PA_ModificarTipoGastos]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PA_ModificarTipoGastos]
	-- Add the parameters for the stored procedure here
	@id int,
	@descripcion nvarchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE [dbo].[tipoGasto]
   SET [descripcion] = @descripcion
 WHERE id=@id

END


GO
/****** Object:  StoredProcedure [dbo].[PA_SeleccionarAutoPlaca]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PA_SeleccionarAutoPlaca]
	-- Add the parameters for the stored procedure here
	@placa varchar(6)
	,@VIN nvarchar(17) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT [placa]
      ,[VIN]
      ,[idMarcaAuto]
      ,[modelo]
      ,[cilindraje]
      ,[numPasajeros]
      ,[idTipoAuto]
      ,[estaActivo]
  FROM [dbo].[automoviles]

  where placa=@placa or VIN=@VIN




END
GO
/****** Object:  StoredProcedure [dbo].[PA_SeleccionarAutoXId]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PA_SeleccionarAutoXId]
	-- Add the parameters for the stored procedure here
	@id varchar(6)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT        automoviles.placa, automoviles.VIN, marcaAuto.descripcion, automoviles.modelo, automoviles.cilindraje, automoviles.numPasajeros, tipoAuto.descripcion AS tipoAuto,
 automoviles.estaActivo, automoviles.idTipoAuto
FROM            automoviles INNER JOIN
                         marcaAuto ON automoviles.idMarcaAuto = marcaAuto.id INNER JOIN
                         tipoAuto ON automoviles.idTipoAuto = tipoAuto.id where placa=@id

END


GO




/****** Object:  StoredProcedure [dbo].[PA_SeleccionarClientesIngreso]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PA_SeleccionarClientesIngreso]
	-- Add the parameters for the stored procedure here
	@Nombre nvarchar(50),
	@contrasenna nvarchar(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT        persona.Nombre, cliente.contrasenna
FROM            persona INNER JOIN  cliente ON cliente.idPersona = persona.id
where persona.Nombre=@Nombre and cliente.contrasenna=@contrasenna

END


GO
/****** Object:  StoredProcedure [dbo].[PA_SeleccionarEmpleadosIngreso]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PA_SeleccionarEmpleadosIngreso]
	-- Add the parameters for the stored procedure here
	@Nombre nvarchar(50),
	@contrasenna nvarchar(15)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT        persona.Nombre,persona.Apellido1, empleado.contrasenna,empleado.idRolUsuario, empleado.estaActivo
FROM            persona INNER JOIN  empleado ON empleado.idPersona = persona.id
WHERE  persona.Nombre=@Nombre  and empleado.contrasenna=@contrasenna

END


GO
/****** Object:  StoredProcedure [dbo].[PA_SeleccionarMarca]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PA_SeleccionarMarca]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


SELECT [id]
      ,[descripcion]
  FROM [dbo].[marcaAuto]



END


GO
/****** Object:  StoredProcedure [dbo].[PA_SeleccionarPersonaId]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PA_SeleccionarPersonaId]
	-- Add the parameters for the stored procedure here
	@id nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT [id]
      ,[Nombre]
      
  FROM [dbo].[persona]
where id=@id

END


GO
/****** Object:  StoredProcedure [dbo].[PA_SeleccionarPersonaXId]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PA_SeleccionarPersonaXId]
	-- Add the parameters for the stored procedure here
	@id nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT        id, Nombre, Apellido1, Apellido2, fechaNacimiento, telContacto AS telefono, correo
FROM            persona where id=@id
END


GO
/****** Object:  StoredProcedure [dbo].[PA_SeleccionarPorTipoAutoReservar]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PA_SeleccionarPorTipoAutoReservar]
(@tipoAuto int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT        automoviles.placa, automoviles.VIN, marcaAuto.descripcion, automoviles.modelo, automoviles.cilindraje, automoviles.numPasajeros, tipoAuto.descripcion AS tipoAuto
FROM            automoviles INNER JOIN
                         marcaAuto ON automoviles.idMarcaAuto = marcaAuto.id INNER JOIN
                         tipoAuto ON automoviles.idTipoAuto = tipoAuto.id WHERE tipoAuto.id=@tipoAuto and automoviles.estaActivo=1
    -- Insert statements for procedure here
END
GO
/****** Object:  StoredProcedure [dbo].[PA_SeleccionarTiGasFiltro]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PA_SeleccionarTiGasFiltro]
	-- Add the parameters for the stored procedure here
@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


SELECT [id]
      ,[descripcion]
  FROM [dbo].[tipoGasto]

  where id=@id

END


GO
/****** Object:  StoredProcedure [dbo].[PA_SeleccionarTipoAuto]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PA_SeleccionarTipoAuto]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT [id]
      ,[descripcion],[precio]
  FROM [dbo].[tipoAuto]

END


GO
/****** Object:  StoredProcedure [dbo].[PA_SeleccionarTipoAutoXId]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PA_SeleccionarTipoAutoXId]
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT [id]
      ,[descripcion],[precio]
  FROM [dbo].[tipoAuto] where id=@id

END


GO
/****** Object:  StoredProcedure [dbo].[PA_SeleccionarTodosAutos]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PA_SeleccionarTodosAutos]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT        automoviles.placa, automoviles.VIN, marcaAuto.descripcion, automoviles.modelo, automoviles.cilindraje, automoviles.numPasajeros, tipoAuto.descripcion AS tipoAuto, automoviles.estaActivo
FROM            automoviles INNER JOIN
                         marcaAuto ON automoviles.idMarcaAuto = marcaAuto.id INNER JOIN
                         tipoAuto ON automoviles.idTipoAuto = tipoAuto.id

END


GO
/****** Object:  StoredProcedure [dbo].[PA_SeleccionarTodosAutosReservar]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PA_SeleccionarTodosAutosReservar]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT        automoviles.placa, automoviles.VIN, marcaAuto.descripcion, automoviles.modelo, automoviles.cilindraje, automoviles.numPasajeros, tipoAuto.descripcion AS tipoAuto
FROM            automoviles INNER JOIN
                         marcaAuto ON automoviles.idMarcaAuto = marcaAuto.id INNER JOIN
                         tipoAuto ON automoviles.idTipoAuto = tipoAuto.id where estaActivo=1
    -- Insert statements for procedure here
END

GO
/****** Object:  StoredProcedure [dbo].[PA_SeleccionarTodosClientes]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PA_SeleccionarTodosClientes]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT        persona.*, cliente.contrasenna
FROM            persona INNER JOIN  cliente ON cliente.idPersona = persona.id

END


GO
/****** Object:  StoredProcedure [dbo].[PA_SeleccionarTodosEmpleados]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PA_SeleccionarTodosEmpleados]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT        persona.*, empleado.fechaContratacion, empleado.estaActivo
FROM            persona INNER JOIN  empleado ON empleado.idPersona = persona.id

END


GO
/****** Object:  StoredProcedure [dbo].[PA_SeleccionarTodosRoles]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PA_SeleccionarTodosRoles]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT [id]
      ,[descripcion]
  FROM [dbo].[rolUsuario]

END


GO
/****** Object:  StoredProcedure [dbo].[PA_SeleccionarTodosTipoGastos]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PA_SeleccionarTodosTipoGastos]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


SELECT [id]
      ,[descripcion]
  FROM [dbo].[tipoGasto]
  END



-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PA_CancelarReservacion]
	-- Add the parameters for the stored procedure here
	@id int,
	@cancelar bit
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE reservacion SET cancelada=@cancelar  WHERE reservacion.id=@id
  END



-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
alter PROCEDURE [dbo].[PA_SeleccionarReservacionesNoCanceladasPorCliente]
	-- Add the parameters for the stored procedure here
	@idCliente nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT        reservacion.id as Id, reservacion.fechaReservacion , reservacion.fechaEntrega as[Entrega],
reservacion.fechaDevolucion as [Devolucion], reservacion.totalReservación as Total,
 concat('Auto: ',tipoAuto.descripcion,', ', marcaAuto.descripcion,', ',automoviles.modelo) as descripcion, reservacion.cancelada
FROM            reservacion INNER JOIN
                         automoviles ON reservacion.idAuto = automoviles.placa INNER JOIN
                         tipoAuto ON automoviles.idTipoAuto = tipoAuto.id INNER JOIN
                         marcaAuto ON automoviles.idMarcaAuto = marcaAuto.id
WHERE        (reservacion.idCliente = @idCliente)
  END


-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PA_SeleccionarReservacionesPorCliente]
	-- Add the parameters for the stored procedure here
	@idCliente nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT        reservacion.id as Id, reservacion.fechaReservacion , reservacion.fechaEntrega as[Entrega],
reservacion.fechaDevolucion as [Devolucion], reservacion.totalReservación as Total,
 concat('Auto: ',tipoAuto.descripcion,', ', marcaAuto.descripcion,', ',automoviles.modelo) as descripcion, reservacion.cancelada
FROM            reservacion INNER JOIN
                         automoviles ON reservacion.idAuto = automoviles.placa INNER JOIN
                         tipoAuto ON automoviles.idTipoAuto = tipoAuto.id INNER JOIN
                         marcaAuto ON automoviles.idMarcaAuto = marcaAuto.id
WHERE        (reservacion.idCliente = @idCliente and reservacion.cancelada=0 and fechaDevolucion>GETDATE())
  END


/****** Object:  StoredProcedure [dbo].[PA_SeleccionarAutoXId]    Script Date: 19/4/2017 03:11:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PA_SeleccionarReservacionXId]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT id as Id, idCliente as IdCliente, fechaReservacion, fechaEntrega as Entrega, 
fechaDevolucion as Devolucion, idAuto as IdAuto, totalReservación as Total, cancelada, vencida, idFactura
FROM reservacion	   where id=@id


END


GO