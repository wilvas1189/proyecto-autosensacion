﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using autoLogicaNegocio;
using autoEntidades;

namespace autoWebServices
{
    /// <summary>
    /// Summary description for autoWebServiceReport
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class autoWebServiceReport : System.Web.Services.WebService
    {

        [WebMethod]
        public  List<DTOReporteFactCliente> ReporteFacturasPorCliente(string idCliente, DateTime inicio, DateTime fin)
        {


            return encabezadoFacturaLogica.ReporteFacturasPorCliente(idCliente).Where(s => s.fechaFactura >= inicio && s.fechaFactura <= fin).ToList();
        }



        [WebMethod]
        public  List<DTOReporteReservacionesCliente> ReporteReservacionesPorCliente(string idCliente, DateTime inicio, DateTime fin)
        {
            return reservacionLogica.ReporteReservacionesPorCliente(idCliente).Where(s => s.fechaReservacion >= inicio && s.fechaReservacion <= fin).ToList(); 
        }


        [WebMethod]
        public List<DTOReporteIngresosAuto> ReporteIngresosPorAuto(string placa, DateTime inicio, DateTime fin)
        {
            return automovilesLogica.ReporteIngresosPorAuto(placa).Where(s => s.FechaFactura >= inicio && s.FechaFactura <= fin).ToList(); ;
        }


        [WebMethod]
        public List<gastoEntidad> ObtenerGastoFiltro(int idTipoGasto, DateTime inicio, DateTime fin)
        {
            gastoLogica gastos = new gastoLogica();
            return gastos.ObtenerGastoFiltro(idTipoGasto).Where(s => s.fechaGasto >= inicio && s.fechaGasto <= fin).ToList();
        }

    }
}
