﻿using autoDatos;
using autoEntidades;
using autoLogicaNegocio;
using autosDatos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace autoWebServices
{
    /// <summary>
    /// Descripción breve de AutoWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class AutoWebService : System.Web.Services.WebService
    {

        // 
        /// METODS PARA GASTOS
        /// 
        [WebMethod]
        public List<tipoGastoEntidad> ObtenerTodosTipoGasto()
        {
            tipoGastosLogica gastos = new tipoGastosLogica();
            return gastos.ObtenerTodos();
        }



        [WebMethod]
        public tipoGastoEntidad ObtenerTodosTipoGastoFiltro(int id)
        {
            tipoGastosLogica gastos = new tipoGastosLogica();
            return gastos.ObtenerTodosFiltro(id);
        }



        [WebMethod]
        public void insertarTipoGasto(tipoGastoEntidad tipoGasto)
        {
            tipoGastosLogica tipo = new tipoGastosLogica();
            tipo.Nuevo(tipoGasto);
        }


        [WebMethod]
        public void modificarTipoGasto(tipoGastoEntidad tipo)
        {
            tipoGastosLogica tipoGasto = new tipoGastosLogica();
            tipoGasto.Modificar(tipo);
        }

        [WebMethod]
        public void insertarGasto(gastoEntidad gasto)
        {
            gastoLogica gas = new gastoLogica();
            gas.Insertar(gasto);
        }



        ///
        ///METODOS PARA AUTOS
        ///
        [WebMethod]
        public List<marcaAutoEntidad> ObtenerTodasMarcas()
        {
            marcaAutosLogica marca = new marcaAutosLogica();
            return marca.ObtenerTodos();
        }

        [WebMethod]
        public List<tipoAutoEntidad> ObtenerTodosTipoAuto()
        {
            tipoAutoLogica tipo = new tipoAutoLogica();
            return tipo.ObtenerTodos();
        }


        [WebMethod]
        public List<tipoAutoEntidad> ObtenerTodosTipo()
        {
            tipoAutoLogica tipo = new tipoAutoLogica();
            return tipo.ObtenerTodos();
        }


        [WebMethod]
        public List<tipoAutoEntidad> SeleccionarTipoAutoPorid(int id)
        {
            tipoAutoLogica tipo = new tipoAutoLogica();
            return tipo.SeleccionarTipoAutoPorid(id);
        }



        [WebMethod]
        public void insertarAutos(automovilEntidad auto)
        {
            automovilesLogica autos = new automovilesLogica();
            autos.Insertar(auto);
        }



        [WebMethod]
        public List<automovilEntidad> ObtenerTodosAutos()
        {
            automovilesLogica autos = new automovilesLogica();
            return autos.ObtenerTodosAutos();
        }



        [WebMethod]
        public List<automovilEntidad> SeleccionarAutoXId(string id)
        {
            automovilesLogica autos = new automovilesLogica();
            return autos.SeleccionarAutoXId(id);
        }



        [WebMethod]
        public automovilEntidad ObtenerAutoPlaca(string placa, string vin)
        {
            automovilesLogica autos = new automovilesLogica();
            return autos.ObtenerAutoPlaca(placa, vin);

        }


        [WebMethod]
        public void modificarAutomoviles(automovilEntidad auto)
        {
            automovilesLogica autos = new automovilesLogica();
            autos.Modificar(auto);
        }



        [WebMethod]
        public List<automovilEntidad> SeleccionarTodosAutosReservar()
        {
            automovilesLogica autLog = new automovilesLogica();
            return autLog.ObtenerTodosAutosReservar();
        }
        [WebMethod]
        public List<automovilEntidad> SeleccionarPotTipoAutoReservar(int idTipoAuto)
        {
            automovilesLogica autLog = new automovilesLogica();
            return autLog.ObtenerAutosPorTipoReservar(idTipoAuto);
        }

        [WebMethod]
        public byte[] SeleccionarFotoAuto(string placa)
        {
            automovilesLogica autLog = new automovilesLogica();
            return autLog.SeleccionarFotoAuto(placa);
        }
        /// 
        /// METODOS PARA EMPLEADOS Y CLIENTES
        /// 

        [WebMethod]
        public void insertarPersona(personaEntidad persona)
        {
            personaLogica per = new personaLogica();
            per.Insertar(persona);
        }



        [WebMethod]
        public List<personaEntidad> SeleccionarPersonaXId(string id)
        {
            personaLogica persona = new personaLogica();
            return persona.SeleccionarPersonaXId(id);
        }


        [WebMethod]
        public void insertarEmpleado(empleadoEntidad empleado)
        {
            empleadoLogica emp = new empleadoLogica();
            emp.Insertar(empleado);
        }


        [WebMethod]
        public List<personaEntidad> ObtenerTodosEmpleados()
        {
            personaLogica persona = new personaLogica();
            return persona.ObtenerTodos();
        }

        [WebMethod]
        public void insertarCliente(clienteEntidad cliente)
        {
            clienteLogica clientes = new clienteLogica();
            clientes.Insertar(cliente);
        }
        [WebMethod]
        public List<personaEntidad> ObtenerTodosClientes()
        {
            personaLogica persona = new personaLogica();
            return persona.ObtenerTodosClien();
        }


        [WebMethod]
        public personaEntidad ObtenerPersona(string id)
        {
            personaLogica per = new personaLogica();
            return per.ObtenerPerId(id);
        }

        [WebMethod]
        public personaEntidad ObtenerCliente(string nombre, string contra)
        {
            personaLogica per = new personaLogica();
            return per.ObtenerUsuario(nombre, contra);
        }

        [WebMethod]
        public empleadoEntidad ObtenerEmpleado(string id, string contra)
        {
            empleadoLogica empl = new empleadoLogica();
            return empl.ObtenerUsuario(id, contra);
        }

        [WebMethod]
        public void modificarClientes(personaEntidad persona)
        {
            personaLogica per = new personaLogica();
            per.ModificarCliente(persona);
        }

        [WebMethod]
        public void modificarEmpleados(personaEntidad persona, empleadoEntidad emp)
        {
            empleadoLogica per = new empleadoLogica();
            per.ModificarEmplado(persona, emp);
        }

        ////////////////////////////////////METODOS PARA FACTURACION//////////////////////////
        [WebMethod]
        public  void insertarFactura(encabezadoFacturaEntidad factura, tarjetaPagoEntidad tarjeta)
        {
            encabezadoFacturaLogica.insertarFactura(factura, tarjeta);

        }

        [WebMethod]
        public void InsertarFacturaModificacion(encabezadoFacturaEntidad factura)
        {
            encabezadoFacturaLogica.InsertarFacturaModificacion(factura);
        }


        [WebMethod]
        public int ObtnerSigNumeroFactura()
        {
            return encabezadoFacturaLogica.ObtnerSigNumeroFactura();

        }


        [WebMethod]
        public Decimal SeleccionarTarjetaXIdFactura( int idFactura)
        {
            return encabezadoFacturaLogica.SeleccionarTarjetaXIdFactura(idFactura);

        }

        [WebMethod]
        public  void ModificarFacturaAnular(int idFactura)
        {
            encabezadoFacturaLogica.ModificarFacturaAnular(idFactura);
        }
        ////////////////////////////////////METODOS PARA RESERVACION//////////////////////////

        [WebMethod]
        public  void insertarReservacion(reservacionEntidad reservacion)
        {

            reservacionLogica.Insertar(reservacion);
        }

        [WebMethod]
        public  void ModificarReservacion(reservacionEntidad reservacion)
        {
            reservacionLogica.ModificarReservacion(reservacion);
        }

        [WebMethod]
        public void CancelarReservacion(int id, bool cancelada)
        {

            reservacionLogica.CancelarReservacion(id,cancelada);
        }

        [WebMethod]
        public List<reservacionEntidad> SeleccionarReservacionesNoCanceladasPorCliente(string id)
        {

            return reservacionLogica.SeleccionarReservacionesNoCanceladasPorCliente(id);
        }

        [WebMethod]
        public  List<reservacionEntidad> SeleccionarReservacionesPorCliente(string id)
        {

            return reservacionLogica.SeleccionarReservacionesPorCliente(id);
        }



        [WebMethod]
        public reservacionEntidad seleccionarReservacionXId(int id)
        {

            return reservacionLogica.seleccionarReservacionXId(id);
        }




        //////////////////////////////////////////////////////////////////////////////////////////////


        //[WebMethod]
        //public DataSet ReporteFacturasPorCliente(string idCliente, DateTime inicio, DateTime fin)
        //{
        //    return encabezadoFacturaDatos.ReporteFacturasPorCliente(idCliente);
        //}



        //[WebMethod]
        //public DataSet ReporteReservacionesPorCliente(string idCliente, DateTime inicio, DateTime fin)
        //{
        //    return reservacionDatos.ReporteReservacionesPorCliente(idCliente, inicio, fin);
        //}


        //[WebMethod]
        //public DataSet ReporteIngresosPorAuto(string placa, DateTime inicio, DateTime fin)
        //{
        //    return automovilesDatos.ReporteIngresosPorAuto(placa, inicio, fin);
        //}




        //////////////////////////////////////////////////////
        [WebMethod]
        public List<reservacionEntidad> ObtenerAlquilerFiltro(string placa, DateTime inicio, DateTime fin)
        {
            reservacionLogica reserva = new reservacionLogica();
            return reserva.ObtenerAlquilerFiltro(placa).Where(s => s.fechaReservacion >= inicio && s.fechaReservacion <= fin).ToList();
        }

    }
}
