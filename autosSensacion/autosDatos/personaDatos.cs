﻿using autoEntidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoDatos
{
   public class personaDatos
    {
        public static void Insertar(personaEntidad persona)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_InsertarPersona");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@id", persona.id);
            comando.Parameters.AddWithValue("@Nombre", persona.Nombre);
            comando.Parameters.AddWithValue("@Apellido1", persona.Apellido1);
            comando.Parameters.AddWithValue("@Apellido2", persona.Apellido2);
            comando.Parameters.AddWithValue("@fechaNacimiento", persona.fechaNacimiento);
            comando.Parameters.AddWithValue("@telContacto", persona.telContacto);
            comando.Parameters.AddWithValue("@correo", persona.correo);
            db.ExecuteNonQuery(comando);
        }

        public static DataSet SeleccionarTodos()
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_SeleccionarTodosEmpleados");
            comando.CommandType = CommandType.StoredProcedure;

            DataSet ds = db.ExecuteReader(comando, "persona");
            return ds;
        }

        public static DataSet SeleccionarPersonaId(string id)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_SeleccionarPersonaId");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@id", id);

            DataSet ds = db.ExecuteReader(comando, "persona");
            return ds;
        }


        public static DataSet SeleccionarPersonaXId(string id)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_SeleccionarPersonaXId");
            comando.Parameters.AddWithValue("@id", id);
            comando.CommandType = CommandType.StoredProcedure;
            DataSet ds = db.ExecuteReader(comando, "persona");
            return ds;
        }


        public static DataSet SeleccionarTodosCli()
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_SeleccionarTodosClientes");
            comando.CommandType = CommandType.StoredProcedure;

            DataSet ds = db.ExecuteReader(comando, "persona");
            return ds;
        }

        public static DataSet SeleccionarCliIngreso(string id, string password)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_SeleccionarClientesIngreso");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@id", id);
            comando.Parameters.AddWithValue("@contrasenna", password);
            DataSet ds = db.ExecuteReader(comando, "persona");
            return ds;
        }

        public static void ModificarCliente(personaEntidad persona)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("[PA_ModificarClientes]");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@id", persona.id);
            comando.Parameters.AddWithValue("@Nombre", persona.Nombre);
            comando.Parameters.AddWithValue("@telContacto", persona.telContacto);
            comando.Parameters.AddWithValue("@correo", persona.correo);
            
            db.ExecuteNonQuery(comando);
        }

    }
}
