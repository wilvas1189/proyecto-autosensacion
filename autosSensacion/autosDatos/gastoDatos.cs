﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using autoEntidades;

namespace autoDatos
{
   public class gastoDatos
    {
        public static void Insertar(gastoEntidad gastos)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_InsertarGasto");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@monto", gastos.monto);
            comando.Parameters.AddWithValue("@descripcion", gastos.descripcion);
            comando.Parameters.AddWithValue("@fechaGasto", gastos.fechaGasto);
            comando.Parameters.AddWithValue("@fechaRegistro", gastos.fechaRegistro);
            comando.Parameters.AddWithValue("@idEmpleado", gastos.idEmpleado);
            comando.Parameters.AddWithValue("@idTipoGasto", gastos.idTipoGasto);

            db.ExecuteNonQuery(comando);
        }

        public static DataSet SeleccionarTodosGastos()
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_SeleccionarTGastos");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;

            DataSet ds = db.ExecuteReader(comando, "gasto");
            return ds;
        }

        public static DataSet SeleccionarGastoFiltro(int idTipoGasto)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_SeleccionarFGastos");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@idTipoGasto", idTipoGasto);

            DataSet ds = db.ExecuteReader(comando, "gasto");
            return ds;
        }
    }
}
