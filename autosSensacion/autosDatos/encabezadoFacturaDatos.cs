﻿using autoDatos;
using autoEntidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autosDatos
{
    public class encabezadoFacturaDatos
    {


        public static void InsertarFactura(encabezadoFacturaEntidad factura,tarjetaPagoEntidad tarjeta)
        {
            //primero inserto la tarjeta
            if (ExisteTarjetaPago(tarjeta.numTarjeta) > 0)
            {

                ModificarTarjetaPago(tarjeta);

            }
            else
            {
                InsertarTarjetaPago(tarjeta);
            }

            //luego se guarda el encabezado
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_InsertarEncabezadoFact");
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@id", factura.id);
            comando.Parameters.AddWithValue("@idEmpleado", factura.idEmpleado);
            comando.Parameters.AddWithValue("@idCliente", factura.idCliente);
            comando.Parameters.AddWithValue("@fecha", factura.fecha);
            comando.Parameters.AddWithValue("@idNumTarjeta", factura.idNumTarjeta);
            db.ExecuteNonQuery(comando);

            //se guardan las lineas de detalle

            foreach (detalleFacturaEntidad detalle in factura.detallesFactura) {
                detalle.idEncabezadoFactura = factura.id;
                InsertarDetalleFact(detalle);
            }

        }

        public static void InsertarFacturaModificacion(encabezadoFacturaEntidad factura)
        {

            //luego se guarda el encabezado
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_InsertarEncabezadoFact");
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@id", factura.id);
            comando.Parameters.AddWithValue("@idEmpleado", factura.idEmpleado);
            comando.Parameters.AddWithValue("@idCliente", factura.idCliente);
            comando.Parameters.AddWithValue("@fecha", factura.fecha);
            comando.Parameters.AddWithValue("@idNumTarjeta", factura.idNumTarjeta);
            db.ExecuteNonQuery(comando);

            //se guardan las lineas de detalle

            foreach (detalleFacturaEntidad detalle in factura.detallesFactura)
            {
                detalle.idEncabezadoFactura = factura.id;
                InsertarDetalleFact(detalle);
            }

        }

        public static void ModificarFacturaAnular(int idFactura)
        {
           

            //luego se guarda el encabezado
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_ModificarFactura_Anular");
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@idFactura", idFactura);
            db.ExecuteNonQuery(comando);
        }

        public static void InsertarTarjetaPago(tarjetaPagoEntidad tarjeta)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_InsertarTarjetaPago");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@numTarjeta", tarjeta.numTarjeta);
            comando.Parameters.AddWithValue("@nombreTitular", tarjeta.nombreTitular);
            comando.Parameters.AddWithValue("@mes", tarjeta.mes);
            comando.Parameters.AddWithValue("@ano", tarjeta.ano);
            comando.Parameters.AddWithValue("@CVV", tarjeta.cvv);
            db.ExecuteNonQuery(comando);
        }



        public static void ModificarTarjetaPago(tarjetaPagoEntidad tarjeta)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_ModificarTarjetaPago");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@numTarjeta", tarjeta.numTarjeta);
            comando.Parameters.AddWithValue("@nombreTitular", tarjeta.nombreTitular);
            comando.Parameters.AddWithValue("@mes", tarjeta.mes);
            comando.Parameters.AddWithValue("@ano", tarjeta.ano);
            comando.Parameters.AddWithValue("@CVV", tarjeta.cvv);
            db.ExecuteNonQuery(comando);
        }

        public static void InsertarDetalleFact(detalleFacturaEntidad detalle)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_InsertarDetalleFact");
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@idAutomovil", detalle.idAutomovil);
            comando.Parameters.AddWithValue("@idEncabezadoFactura", detalle.idEncabezadoFactura);
            comando.Parameters.AddWithValue("@cantidadDias", detalle.cantidadDias);
            comando.Parameters.AddWithValue("@precio", detalle.precio);
            db.ExecuteNonQuery(comando);
        }

        //METODO QUE RETORNA EL CONSECUTIVO PARA UNA FACTURA
        static public int ConsecutivoFactura()
        {

            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_ConsecutivoFactura");
            comando.CommandType = CommandType.StoredProcedure;
            DataSet ds = db.ExecuteReader(comando, "tipoAuto");
            // Extraer la tabla 
            DataTable dt = ds.Tables[0];

            return int.Parse(dt.Rows[0][0].ToString());
        }


        static public int ExisteTarjetaPago(decimal numTarjeta) { 
       

            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_ExisteTarjetaPago");
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@numeroTarjeta", numTarjeta);
            DataSet ds = db.ExecuteReader(comando, "tarjetaPago");
            // Extraer la tabla 
            DataTable dt = ds.Tables[0];

            return int.Parse(dt.Rows[0][0].ToString());
        }

        static public Decimal SeleccionarTarjetaXIdFactura(int idFactura) {

            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_SeleccionarTarjetaXIdFactura");
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@idFactura", idFactura);
            DataSet ds = db.ExecuteReader(comando, "tarjetaPago");
            // Extraer la tabla 
            DataTable dt = ds.Tables[0];

            return Decimal.Parse(dt.Rows[0][0].ToString());
        }


        ///////////////////////////////REPORTES//////////////////////////////////////////////
        public static DataSet ReporteFacturasPorCliente(string idCliente)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_ReporteFacturasPorCliente");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@idCliente", idCliente);
            DataSet ds = db.ExecuteReader(comando, "rentaAutoSensacionDB");
            return ds;
        }
    }
}
