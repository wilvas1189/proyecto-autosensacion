﻿using autoEntidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoDatos
{
   public class clienteDatos
    {
        public static void Insertar(clienteEntidad cliente)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_InsertarCliente");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@idPersona", cliente.idPersona);
            comando.Parameters.AddWithValue("@fechaIngreso", cliente.fechaIngreso);
            comando.Parameters.AddWithValue("@idRolUsuario", cliente.idRolUsuario);
            comando.Parameters.AddWithValue("@contrasenna", cliente.contrasenna);

            db.ExecuteNonQuery(comando);
        }

        
    }
}
