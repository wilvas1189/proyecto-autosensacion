﻿using autoEntidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoDatos
{
   public class automovilesDatos
    {
        public static void Insertar(automovilEntidad auto)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_InsertarAutomoviles");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@placa", auto.placa);
            comando.Parameters.AddWithValue("@VIN",auto.VIN);
            comando.Parameters.AddWithValue("@idMarcaAuto",auto.idMarcaAuto);
            comando.Parameters.AddWithValue("@modelo",auto.modelo);
            comando.Parameters.AddWithValue("@cilindraje", auto.cilindraje);
            comando.Parameters.AddWithValue("@numPasajeros",auto.numPasajeros);
            comando.Parameters.AddWithValue("@idTipoAuto",auto.idTipoAuto);
            comando.Parameters.AddWithValue("@estaActivo",auto.estaActivo);
            SqlParameter imageParam = comando.Parameters.Add("@imagen", System.Data.SqlDbType.Image);
            imageParam.Value = auto.fotografia;

            db.ExecuteNonQuery(comando);
        }

        public static DataSet SeleccionarTodosAutos()
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_SeleccionarTodosAutos");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;

            DataSet ds = db.ExecuteReader(comando, "rentaAutoSensacionDB");
            return ds;
        }


        public static DataSet SeleccionarTodosAutosReservar()
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_SeleccionarTodosAutosReservar");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;

            DataSet ds = db.ExecuteReader(comando, "rentaAutoSensacionDB");
            return ds;
        }

        public static DataSet SeleccionarPotTipoAutoReservar(int tipoAuto)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_SeleccionarPorTipoAutoReservar");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@tipoAuto", tipoAuto);
            DataSet ds = db.ExecuteReader(comando, "rentaAutoSensacionDB");
            return ds;
        }



        public static DataSet SeleccionarAutoPlaca(string placa, string Vin)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_SeleccionarAutoPlaca");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@placa", placa);
            comando.Parameters.AddWithValue("@VIN", Vin);
            DataSet ds = db.ExecuteReader(comando, "automoviles");
            return ds;
        }


        public static DataSet SeleccionarAutoXId(string id)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("[PA_SeleccionarAutoXId]");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@id", id);
            DataSet ds = db.ExecuteReader(comando, "rentaAutoSensacionDB");
            return ds;
        }


        public static void Modificar(automovilEntidad auto)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("[PA_ModificarAutos]");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@placa", auto.placa);
            comando.Parameters.AddWithValue("@idMarcaAuto", auto.idMarcaAuto);
            comando.Parameters.AddWithValue("@modelo", auto.modelo);
            comando.Parameters.AddWithValue("@cilindraje", auto.cilindraje);
            comando.Parameters.AddWithValue("@numPasajeros", auto.numPasajeros);
            comando.Parameters.AddWithValue("@idTipoAuto", auto.idTipoAuto);
            comando.Parameters.AddWithValue("@estaActivo", auto.estaActivo);
            SqlParameter imageParam = comando.Parameters.Add("@imagen", System.Data.SqlDbType.Image);
            imageParam.Value = auto.fotografia;
            db.ExecuteNonQuery(comando);
        }


        public static void ModificarSinFoto(automovilEntidad auto)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("[PA_ModificarAutosSinFoto]");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@placa", auto.placa);
            comando.Parameters.AddWithValue("@idMarcaAuto", auto.idMarcaAuto);
            comando.Parameters.AddWithValue("@modelo", auto.modelo);
            comando.Parameters.AddWithValue("@cilindraje", auto.cilindraje);
            comando.Parameters.AddWithValue("@numPasajeros", auto.numPasajeros);
            comando.Parameters.AddWithValue("@idTipoAuto", auto.idTipoAuto);
            comando.Parameters.AddWithValue("@estaActivo", auto.estaActivo);
            db.ExecuteNonQuery(comando);
        }

        static public byte[] SeleccionarFotoAuto(string placa)
        {

            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_SeleccionarFotoAuto");
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@placa", placa);
            DataSet ds = db.ExecuteReader(comando, "automoviles");
            // Extraer la tabla 
            DataTable dt = ds.Tables[0];

            return (byte[])dt.Rows[0][0];
        }

        ///////////////////////////////REPORTES//////////////////////////////////////////////
        public static DataSet ReporteIngresosPorAuto(string placa)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("[PA_ReporteIngresosPorAuto]");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@placa", placa);
            DataSet ds = db.ExecuteReader(comando, "rentaAutoSensacionDB");
            return ds;
        }
    }
}
