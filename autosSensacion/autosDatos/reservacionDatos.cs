﻿using autoEntidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoDatos
{
   public class reservacionDatos
    {
        public static void Insertar(reservacionEntidad reservacion)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_InsertarReservacion");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@idCliente", reservacion.idCliente);
            comando.Parameters.AddWithValue("@fechaReservacion", reservacion.fechaReservacion);
            comando.Parameters.AddWithValue("@fechaEntrega", reservacion.fechaEntrega);
            comando.Parameters.AddWithValue("@fechaDevolucion", reservacion.fechaDevolucion);
            comando.Parameters.AddWithValue("@idAuto", reservacion.idAuto);
            comando.Parameters.AddWithValue("@totalReservacion", reservacion.totalReservacion);
            comando.Parameters.AddWithValue("@cancelada", reservacion.cancelada);
            comando.Parameters.AddWithValue("@vencida", reservacion.vencida);
            comando.Parameters.AddWithValue("@idFactura", reservacion.idFactura);
            db.ExecuteNonQuery(comando);
        }

        public static void ModificarReservacion(reservacionEntidad reservacion)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_ModificarReservacion");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@idReservacion", reservacion.id);
            comando.Parameters.AddWithValue("@fechaEntrega", reservacion.fechaEntrega);
            comando.Parameters.AddWithValue("@fechaDevolucion", reservacion.fechaDevolucion);
            comando.Parameters.AddWithValue("@idAuto", reservacion.idAuto);
            comando.Parameters.AddWithValue("@totalReservacion", reservacion.totalReservacion);
            comando.Parameters.AddWithValue("@idFactura", reservacion.idFactura);
            db.ExecuteNonQuery(comando);
        }


        public static void CancelarReservacion(int  id, bool cancelada)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("[PA_CancelarReservacion]");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@id", id);
            comando.Parameters.AddWithValue("@cancelar", cancelada);
            db.ExecuteNonQuery(comando);
        }

        public static DataSet SeleccionarReservacionesNoCanceladasPorCliente( string id) {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_SeleccionarReservacionesNoCanceladasPorCliente");
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@idCliente", id);
            DataSet ds = db.ExecuteReader(comando, "reservacion");
            return ds;
        }



        public static DataSet SeleccionarReservacionesPorCliente(string idCliente)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("[PA_SeleccionarReservacionesPorCliente]");
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@idCliente", idCliente);
            DataSet ds = db.ExecuteReader(comando, "reservacion");
            return ds;
        }



        public static DataSet seleccionarReservacionXId(int id)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_SeleccionarReservacionXId");
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@id", id);
            DataSet ds = db.ExecuteReader(comando, "reservacion");
            return ds;
        }


        ///////////////////////////////REPORTES//////////////////////////////////////////////
        public static DataSet ReporteReservacionesPorCliente(string idCliente)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("[PA_ReporteReservacionesPorCliente]");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@idCliente", idCliente);
            DataSet ds = db.ExecuteReader(comando, "rentaAutoSensacionDB");
            return ds;
        }

        public static DataSet SeleccionarAlquilerFiltro(string placa)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_SeleccionarAlquilFiltro");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@placa", placa);

            DataSet ds = db.ExecuteReader(comando, "reservacion");
            return ds;
        }

    }
}

