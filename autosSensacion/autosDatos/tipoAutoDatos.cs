﻿using autoDatos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autosDatos
{
   public class tipoAutoDatos
    {
        public static DataSet SeleccionarTodos()
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("[PA_SeleccionarTipoAuto]");
            comando.CommandType = CommandType.StoredProcedure;

            DataSet ds = db.ExecuteReader(comando, "tipoAuto");
            return ds;
        }

        public static DataSet SeleccionarTipoAutoPorid(int id)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("[PA_SeleccionarTipoAutoXId]");
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@id", id);

            DataSet ds = db.ExecuteReader(comando, "tipoAuto");
            return ds;
        }
    }
}
