﻿using autoEntidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoDatos
{
   public class empleadoDatos
    {
        public static void Insertar(empleadoEntidad empleado)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_InsertarEmpleado");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@idPersona",empleado.idPersona);
            comando.Parameters.AddWithValue("@fechaContratacion", empleado.fechaContratacion);
            comando.Parameters.AddWithValue("@estaActivo", empleado.estaActivo);
            comando.Parameters.AddWithValue("@idRolUsuario", empleado.idRolUsuario);
            comando.Parameters.AddWithValue("@contrasenna", empleado.contrasenna);

            db.ExecuteNonQuery(comando);
        }
        public static DataSet SeleccionarEmplIngreso(string id, string password)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_SeleccionarEmpleadosIngreso");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@id", id);
            comando.Parameters.AddWithValue("@contrasenna", password);
            DataSet ds = db.ExecuteReader(comando, "persona");
            return ds;
        }

        public static void ModificarEmpleado(personaEntidad persona, empleadoEntidad empl)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("PA_ModificarEmpleado");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@id", persona.id);
            comando.Parameters.AddWithValue("@Nombre", persona.Nombre);
            comando.Parameters.AddWithValue("@telContacto", persona.telContacto);
            comando.Parameters.AddWithValue("@correo", persona.correo);
            comando.Parameters.AddWithValue("@estaActivo", empl.estaActivo);
            db.ExecuteNonQuery(comando);
        }
    }
}
