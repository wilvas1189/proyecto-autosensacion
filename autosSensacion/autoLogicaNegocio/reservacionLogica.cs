﻿using autoDatos;
using autoEntidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoLogicaNegocio
{
    public class reservacionLogica
    {


        public static void Insertar(reservacionEntidad reservacion)
        {

            reservacionDatos.Insertar(reservacion);
        }


        public static void ModificarReservacion(reservacionEntidad reservacion) {
            reservacionDatos.ModificarReservacion(reservacion);
        }

        public static void CancelarReservacion(int id, bool cancelada)
        {

            reservacionDatos.CancelarReservacion( id,  cancelada);
        }



        public static List<reservacionEntidad> SeleccionarReservacionesNoCanceladasPorCliente(string id)
        {
            List<reservacionEntidad> lista = new List<reservacionEntidad>();

            DataSet ds = reservacionDatos.SeleccionarReservacionesNoCanceladasPorCliente(id);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                reservacionEntidad reserv = new reservacionEntidad();
                reserv.id =Convert.ToInt16( row["Id"].ToString());
                reserv.fechaReservacion = Convert.ToDateTime(row["fechaReservacion"].ToString());
                reserv.fechaEntrega = Convert.ToDateTime(row["Entrega"].ToString());
                reserv.fechaDevolucion = Convert.ToDateTime(row["Devolucion"].ToString());
                reserv.totalReservacion = Convert.ToDecimal(row["Total"].ToString());
                reserv.descripcion = row["descripcion"].ToString();
                reserv.cancelada = Convert.ToBoolean(row["cancelada"].ToString());
                if (reserv.cancelada == true)
                {
                    reserv.stringCancelada = "Sí";
                }
                else
                {
                    reserv.stringCancelada = "No";
                }

                lista.Add(reserv);
            }

            return lista;
        }




        public static List<reservacionEntidad> SeleccionarReservacionesPorCliente(string id)
        {
            List<reservacionEntidad> lista = new List<reservacionEntidad>();

            DataSet ds = reservacionDatos.SeleccionarReservacionesPorCliente(id);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                reservacionEntidad reserv = new reservacionEntidad();
                reserv.id = Convert.ToInt16(row["Id"].ToString());
                reserv.fechaReservacion = Convert.ToDateTime(row["fechaReservacion"].ToString());
                reserv.fechaEntrega = Convert.ToDateTime(row["Entrega"].ToString());
                reserv.fechaDevolucion = Convert.ToDateTime(row["Devolucion"].ToString());
                reserv.totalReservacion = Convert.ToDecimal(row["Total"].ToString());
                reserv.descripcion = row["descripcion"].ToString();
                reserv.cancelada = Convert.ToBoolean(row["cancelada"].ToString());
                lista.Add(reserv);
            }

            return lista;
        }


        public static reservacionEntidad seleccionarReservacionXId(int id)
        {


            DataSet ds = reservacionDatos.seleccionarReservacionXId(id);
            reservacionEntidad reserv = new reservacionEntidad();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                reserv.id = Convert.ToInt16(row["Id"].ToString());
                reserv.idCliente = row["IdCliente"].ToString();
                reserv.fechaReservacion = Convert.ToDateTime(row["fechaReservacion"].ToString());
                reserv.fechaEntrega = Convert.ToDateTime(row["Entrega"].ToString());
                reserv.fechaDevolucion = Convert.ToDateTime(row["Devolucion"].ToString());
                reserv.fechaDevolucion = Convert.ToDateTime(row["Devolucion"].ToString());
                reserv.idAuto = row["IdAuto"].ToString();
                reserv.totalReservacion = Convert.ToDecimal(row["Total"].ToString());
                reserv.cancelada = Convert.ToBoolean(row["cancelada"].ToString());
                reserv.vencida= Convert.ToBoolean(row["vencida"].ToString());
                reserv.idFactura = Convert.ToInt16(row["idFactura"].ToString());
                if (reserv.cancelada == true)
                {
                    reserv.stringCancelada = "Sí";
                }
                else
                {
                    reserv.stringCancelada = "No";
                }
            }
            return reserv;
        }



        ///////////////////////////////REPORTES//////////////////////////////////////////////
        public static List<DTOReporteReservacionesCliente> ReporteReservacionesPorCliente(string idCliente)
        {
            List<DTOReporteReservacionesCliente> lista = new List<DTOReporteReservacionesCliente>();

            DataSet ds = reservacionDatos.ReporteReservacionesPorCliente(idCliente);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                DTOReporteReservacionesCliente report = new DTOReporteReservacionesCliente();
                report.idCliente = row["idCliente"].ToString();
                report.Nombre = row["Nombre"].ToString();
                report.Apellido1 = row["Apellido1"].ToString();
                report.Apellido2 = row["Apellido2"].ToString();
                report.idReservacion = row["idReservacion"].ToString();
                report.fechaReservacion = DateTime.Parse(row["fechaReservacion"].ToString());
                report.fechaEntrega = row["fechaEntrega"].ToString();
                report.fechaDevolion = row["fechaDevolucion"].ToString();
                report.placa = row["placa"].ToString();
                report.marcaAuto = row["marcaAuto"].ToString();
                report.modeloAuto = row["modeloAuto"].ToString();
                report.tipoAuto = row["tipoAuto"].ToString();
                report.tipoAuto = row["tipoAuto"].ToString();
                report.totalReservacion = Decimal.Parse(row["totalReservación"].ToString());
                lista.Add(report);
            }
            return lista;
        }








        public List<reservacionEntidad> ObtenerAlquilerFiltro(string placa)
        {
            List<reservacionEntidad> lista = new List<reservacionEntidad>();
            DataSet ds = reservacionDatos.SeleccionarAlquilerFiltro(placa);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                reservacionEntidad alquiler = new reservacionEntidad();
                alquiler.idAuto = row["idAuto"].ToString();
                alquiler.marcaAuto = row["MarcaAuto"].ToString();
                alquiler.tipoAuto = row["TipoAuto"].ToString();
                alquiler.fechaReservacion = Convert.ToDateTime(row["fechaReservacion"].ToString());
                alquiler.cantDias = row["Cantidad"].ToString();
                alquiler.totalReservacion = Convert.ToDecimal(row["totalReservación"].ToString());

                lista.Add(alquiler);
            }
            return lista;
        }
    }

    
}
