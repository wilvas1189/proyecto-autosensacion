﻿using autoEntidades;
using autosDatos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoLogicaNegocio
{
    public class encabezadoFacturaLogica
    {

        public static void insertarFactura(encabezadoFacturaEntidad factura, tarjetaPagoEntidad tarjeta) {
            encabezadoFacturaDatos.InsertarFactura(factura,tarjeta);

        }

        public static void InsertarFacturaModificacion(encabezadoFacturaEntidad factura)
        {
            encabezadoFacturaDatos.InsertarFacturaModificacion(factura);
        }

        public static int ObtnerSigNumeroFactura() {
            return encabezadoFacturaDatos.ConsecutivoFactura();
        }

        public static void ModificarFacturaAnular(int idFactura) {
            encabezadoFacturaDatos.ModificarFacturaAnular(idFactura);
        }


        static public Decimal SeleccionarTarjetaXIdFactura(int idFactura) {
            return encabezadoFacturaDatos.SeleccionarTarjetaXIdFactura(idFactura);
        }





        ///////////////////////////////REPORTES//////////////////////////////////////////////
        public static List<DTOReporteFactCliente> ReporteFacturasPorCliente(string idCliente)
        {
            List<DTOReporteFactCliente> lista = new List<DTOReporteFactCliente>();

            DataSet ds = encabezadoFacturaDatos.ReporteFacturasPorCliente(idCliente);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                DTOReporteFactCliente report = new DTOReporteFactCliente();
                report.idCliente = row["idCliente"].ToString();
                report.Nombre = row["Nombre"].ToString();
                report.Apellido1 = row["Apellido1"].ToString();
                report.Apellido2 = row["Apellido2"].ToString();
                report.telContacto = row["telContacto"].ToString();
                report.correo = row["correo"].ToString();
                report.numFactura = row["numFactura"].ToString();
                report.fechaFactura = DateTime.Parse(row["fechaFactura"].ToString());
                report.idEmpleado = row["idEmpleado"].ToString();
                report.numeroTarjeta = row["idNumTarjeta"].ToString();
                report.idDetalle = row["idDetalle"].ToString();
                report.idAutomovil = row["idAutomovil"].ToString();
                report.tipoAuto = row["tipoAuto"].ToString();
                report.marca = row["marca"].ToString();
                report.modelo = row["modelo"].ToString();
                report.activa = Boolean.Parse( row["activa"].ToString())?"Si":"No";
                report.monto = decimal.Parse(row["Total"].ToString());
                lista.Add(report);
            }
            return lista;
        }
    }
}
