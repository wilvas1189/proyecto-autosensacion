﻿using autoDatos;
using autoEntidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoLogicaNegocio
{
   public class gastoLogica
    {
        public void Insertar(gastoEntidad gasto)
        {
            gastoDatos.Insertar(gasto);
        }

        public List<gastoEntidad> ObtenerTodos()
        {
            List<gastoEntidad> lista = new List<gastoEntidad>();

            DataSet ds = gastoDatos.SeleccionarTodosGastos();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                gastoEntidad gasto = new gastoEntidad();
                gasto.monto =Convert.ToDouble( row["monto"].ToString());
                gasto.descripcion = row["descripcion"].ToString();
                gasto.fechaGasto = Convert.ToDateTime(row["fechaGasto"].ToString());
                gasto.tipoGasto = row["tipoGasto"].ToString();
                gasto.Nombre = row["Nombre"].ToString();
                
               
                lista.Add(gasto);
            }

            return lista;
        }

        public List<gastoEntidad> ObtenerGastoFiltro(int idTipoGasto)
        {
           List<gastoEntidad> lista = new List<gastoEntidad>();
            DataSet ds = gastoDatos.SeleccionarGastoFiltro(idTipoGasto);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                gastoEntidad gasto = new gastoEntidad();
                gasto.monto = Convert.ToDouble(row["monto"].ToString());
                gasto.descripcion = row["descripcion"].ToString();
                gasto.fechaGasto = Convert.ToDateTime(row["fechaGasto"].ToString());
                gasto.tipoGasto = row["tipoGasto"].ToString();
                gasto.Nombre = row["Nombre"].ToString();

                lista.Add(gasto);
            }
            return lista;
        }
    }
}
