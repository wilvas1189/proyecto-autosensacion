﻿using autoDatos;
using autoEntidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoLogicaNegocio
{
    public class personaLogica
    {
        public void Insertar(personaEntidad persona)
        {
            personaDatos.Insertar(persona);
        }

        public List<personaEntidad> ObtenerTodos()
        {
            List<personaEntidad> lista = new List<personaEntidad>();

            DataSet ds = personaDatos.SeleccionarTodos();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                personaEntidad per = new personaEntidad();
                per.id = row["id"].ToString();
                per.Nombre = row["Nombre"].ToString();
                per.Apellido1 = row["Apellido1"].ToString();
                per.Apellido2 = row["Apellido2"].ToString();
                per.fechaNacimiento = Convert.ToDateTime(row["fechaNacimiento"].ToString());
                per.telContacto = row["telContacto"].ToString();
                per.correo = row["correo"].ToString();
                per.activo = Convert.ToBoolean(row["estaActivo"].ToString());
                if (per.activo == true)
                {
                    per.estaActivo = "Sí";
                }
                else
                {
                    per.estaActivo = "No";
                }

                lista.Add(per);
            }
            return lista;
        }

        public List<personaEntidad> ObtenerTodosClien()
        {
            List<personaEntidad> lista = new List<personaEntidad>();

            DataSet ds = personaDatos.SeleccionarTodosCli();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                personaEntidad per = new personaEntidad();
                per.id = row["id"].ToString();
                per.Nombre = row["Nombre"].ToString();
                per.Apellido1 = row["Apellido1"].ToString();
                per.Apellido2 = row["Apellido2"].ToString();
                per.fechaNacimiento = Convert.ToDateTime(row["fechaNacimiento"].ToString());
                per.telContacto = row["telContacto"].ToString();
                per.correo = row["correo"].ToString();
                per.contrasenna=row["contrasenna"].ToString();

                lista.Add(per);
            }
            return lista;
        }
        public personaEntidad ObtenerPerId(string id)
        {
            personaEntidad persona = new personaEntidad();
            DataSet ds = personaDatos.SeleccionarPersonaId(id);

            foreach (DataRow row in ds.Tables[0].Rows)
            {

                persona.id = (row["id"].ToString());
                persona.Nombre = row["Nombre"].ToString();
               
            }
            return persona;
        }


        public List<personaEntidad> SeleccionarPersonaXId(String id)
        {
            List<personaEntidad> lista = new List<personaEntidad>();

            DataSet ds = personaDatos.SeleccionarPersonaXId(id);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                personaEntidad persona = new personaEntidad();
                persona.id = row["id"].ToString();
                persona.Nombre = row["Nombre"].ToString();
                persona.Apellido1 = row["Apellido1"].ToString();
                persona.Apellido2 = row["Apellido2"].ToString();
                persona.fechaNacimiento = Convert.ToDateTime(row["fechaNacimiento"].ToString());
                persona.telContacto = row["telefono"].ToString();
                persona.correo = row["correo"].ToString();

                lista.Add(persona);
            }

            return lista;
        }


        public personaEntidad ObtenerUsuario(string id, string pass)
        {
            personaEntidad cliente = new personaEntidad();
            DataSet ds = personaDatos.SeleccionarCliIngreso(id, pass);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                cliente.Nombre = row["Nombre"].ToString();
                cliente.id = row["idPersona"].ToString();

            }
            return cliente;
        }

        public void ModificarCliente(personaEntidad persona)
        {
            personaDatos.ModificarCliente(persona);
        }

    }

}
