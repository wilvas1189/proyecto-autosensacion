﻿using autoEntidades;
using autosDatos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoLogicaNegocio
{
    public class tipoAutoLogica
    {
        public List<tipoAutoEntidad> ObtenerTodos()
        {
            List<tipoAutoEntidad> lista = new List<tipoAutoEntidad>();

            DataSet ds = tipoAutoDatos.SeleccionarTodos();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                tipoAutoEntidad tipo = new tipoAutoEntidad();
                tipo.id = Convert.ToInt16(row["id"].ToString());
                tipo.descripcion = row["descripcion"].ToString();
                tipo.precio = Convert.ToDecimal(row["precio"].ToString());
                lista.Add(tipo);
            }
            return lista;
        }



        public List<tipoAutoEntidad> SeleccionarTipoAutoPorid(int id)
        {
            List<tipoAutoEntidad> lista = new List<tipoAutoEntidad>();

            DataSet ds = tipoAutoDatos.SeleccionarTipoAutoPorid(id);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                tipoAutoEntidad tipo = new tipoAutoEntidad();
                tipo.id = Convert.ToInt16(row["id"].ToString());
                tipo.descripcion = row["descripcion"].ToString();
                tipo.precio = Convert.ToDecimal(row["precio"].ToString());
                lista.Add(tipo);
            }
            return lista;
        }
    }
}
