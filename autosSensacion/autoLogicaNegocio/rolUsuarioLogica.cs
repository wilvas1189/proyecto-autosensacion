﻿using autoEntidades;
using autosDatos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoLogicaNegocio
{
    public class rolUsuarioLogica
    {
        public void Nuevo(rolUsuarioEntidad rol)
        {
            rolUsuariosDatos.Insertar(rol.descripcion);
        }

        public List<rolUsuarioEntidad> ObtenerTodos()
        {
            List<rolUsuarioEntidad> lista = new List<rolUsuarioEntidad>();

            DataSet ds = rolUsuariosDatos.SeleccionarTodos();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                rolUsuarioEntidad rol = new rolUsuarioEntidad();
                rol.id = Convert.ToInt16(row["id"].ToString());
                rol.descripcion = row["descripcion"].ToString();
                lista.Add(rol);
            }

            return lista;
        }

        public void Modificar(rolUsuarioEntidad rol)
        {
            rolUsuariosDatos.Modificar(rol.id, rol.descripcion);
        }
    }
}
