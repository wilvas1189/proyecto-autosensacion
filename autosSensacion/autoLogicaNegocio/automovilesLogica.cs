﻿using autoDatos;
using autoEntidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoLogicaNegocio
{
   public class automovilesLogica
    {
        public void Insertar(automovilEntidad automovil)
        {
            automovilesDatos.Insertar(automovil);
        }

        public List<automovilEntidad> ObtenerTodos()
        {
            List<automovilEntidad> lista = new List<automovilEntidad>();

            DataSet ds = automovilesDatos.SeleccionarTodosAutos();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                automovilEntidad rol = new automovilEntidad();
                rol.placa= row["placa"].ToString();
                rol.VIN= row["VIN"].ToString();
                rol.descripcion = row["descripcion"].ToString();
                rol.idMarcaAuto = Convert.ToInt16(row["idMarca"].ToString());
                rol.modelo= row["modelo"].ToString();
                rol.cilindraje=Convert.ToDecimal( row["cilindraje"].ToString());
                rol.numPasajeros= Convert.ToDecimal(row["numPasajeros"].ToString());
                rol.tipoAuto = row["descTipo"].ToString();
                rol.idTipoAuto = Convert.ToInt16(row["idTipo"].ToString());
                rol.estaActivo = Convert.ToBoolean(row["estaActivo"].ToString());
                if (rol.estaActivo==true)
                {
                    rol.activo = "Sí";
                }else
                {
                    rol.activo = "No";
                }
                rol.fotografia = (byte[])row["foto"];

                lista.Add(rol);
            }

            return lista;
        }



        public List<automovilEntidad> ObtenerTodosAutos()
        {
            List<automovilEntidad> lista = new List<automovilEntidad>();

            DataSet ds = automovilesDatos.SeleccionarTodosAutos();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                automovilEntidad rol = new automovilEntidad();
                rol.placa = row["placa"].ToString();
                rol.VIN = row["VIN"].ToString();
                rol.descripcion = row["descripcion"].ToString();
                rol.modelo = row["modelo"].ToString();
                rol.cilindraje = Convert.ToDecimal(row["cilindraje"].ToString());
                rol.numPasajeros = Convert.ToDecimal(row["numPasajeros"].ToString());
                rol.tipoAuto = row["tipoAuto"].ToString();
                rol.estaActivo = Convert.ToBoolean(row["estaActivo"].ToString());
                if (rol.estaActivo == true)
                {
                    rol.activo = "Sí";
                }
                else
                {
                    rol.activo = "No";
                }

                lista.Add(rol);
            }

            return lista;
        }


        public automovilEntidad ObtenerAutoPlaca(string placa, string vin)
        {
            automovilEntidad auto = new automovilEntidad();
            DataSet ds = automovilesDatos.SeleccionarAutoPlaca(placa, vin);

            foreach (DataRow row in ds.Tables[0].Rows)
            {

                auto.placa =(row["placa"].ToString());
                auto.VIN = row["VIN"].ToString();
                auto.idMarcaAuto =Convert.ToInt16( row["idMarcaAuto"].ToString());
                auto.modelo = row["modelo"].ToString();
                auto.cilindraje =Convert.ToDecimal( row["cilindraje"].ToString());
                auto.numPasajeros = Convert.ToDecimal(row["numPasajeros"].ToString());
                auto.idTipoAuto = Convert.ToInt16(row["idTipoAuto"].ToString());
                auto.estaActivo = Convert.ToBoolean(row["estaActivo"].ToString());
            }
            return auto;
        }

        //RETORNA UNA LISTA DE TODOS LOS AUTOS QUE SE PUEDEN RESERVAR
        public List<automovilEntidad> ObtenerTodosAutosReservar()
        {
            List<automovilEntidad> lista = new List<automovilEntidad>();

            DataSet ds = automovilesDatos.SeleccionarTodosAutosReservar();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                automovilEntidad auto = new automovilEntidad();
                auto.placa = row["placa"].ToString();
                auto.VIN = row["VIN"].ToString();
                auto.descripcion = row["descripcion"].ToString();
                auto.modelo = row["modelo"].ToString();
                auto.cilindraje = Convert.ToDecimal(row["cilindraje"].ToString());
                auto.numPasajeros = Convert.ToDecimal(row["numPasajeros"].ToString());
                auto.tipoAuto = row["tipoAuto"].ToString();

                lista.Add(auto);
            }

            return lista;
        }


        //RETORNA UNA LISTA DE TODOS LOS AUTOS QUE SE PUEDEN RESERVAR POR TIPO DE AUTO
        public List<automovilEntidad> ObtenerAutosPorTipoReservar(int tipoAuto)
        {
            List<automovilEntidad> lista = new List<automovilEntidad>();

            DataSet ds = automovilesDatos.SeleccionarPotTipoAutoReservar(tipoAuto);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                automovilEntidad auto = new automovilEntidad();
                auto.placa = row["placa"].ToString();
                auto.VIN = row["VIN"].ToString();
                auto.descripcion = row["descripcion"].ToString();
                auto.modelo = row["modelo"].ToString();
                auto.cilindraje = Convert.ToDecimal(row["cilindraje"].ToString());
                auto.numPasajeros = Convert.ToDecimal(row["numPasajeros"].ToString());
                auto.tipoAuto = row["tipoAuto"].ToString();


                lista.Add(auto);
            }

            return lista;
        }

        public List<automovilEntidad> SeleccionarAutoXId(string id)
        {
            List<automovilEntidad> lista = new List<automovilEntidad>();

            DataSet ds = automovilesDatos.SeleccionarAutoXId(id);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                automovilEntidad auto = new automovilEntidad();
                auto.placa = row["placa"].ToString();
                auto.VIN = row["VIN"].ToString();
                auto.descripcion = row["descripcion"].ToString();
                auto.modelo = row["modelo"].ToString();
                auto.cilindraje = Convert.ToDecimal(row["cilindraje"].ToString());
                auto.numPasajeros = Convert.ToDecimal(row["numPasajeros"].ToString());
                auto.tipoAuto = row["tipoAuto"].ToString();
                auto.idTipoAuto = Convert.ToInt16(row["idTipoAuto"].ToString());
                lista.Add(auto);
            }

            return lista;
        }



        public void Modificar(automovilEntidad autos)
        {
            if (autos.fotografia != null)
            {
                automovilesDatos.Modificar(autos);
            }
            else
            {
                automovilesDatos.ModificarSinFoto(autos);
            }
        }
        public   byte[] SeleccionarFotoAuto(string placa)
        {

             return automovilesDatos.SeleccionarFotoAuto(placa);
        }



        ///////////////////////////////REPORTES//////////////////////////////////////////////
        public static List<DTOReporteIngresosAuto> ReporteIngresosPorAuto(string placa)
        {
            List<DTOReporteIngresosAuto> lista = new List<DTOReporteIngresosAuto>();

            DataSet ds = automovilesDatos.ReporteIngresosPorAuto(placa);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                DTOReporteIngresosAuto report = new DTOReporteIngresosAuto();
                report.placa = row["placa"].ToString();
                report.Vin = row["VIN"].ToString();
                report.TipoAuto = row["TipoAuto"].ToString();
                report.Marca = row["marca"].ToString();
                report.Modelo = row["modelo"].ToString();
                report.idFactura = row["idFactura"].ToString();
                report.FechaFactura = DateTime.Parse( row["fechaFactura"].ToString());
                report.Monto = Decimal.Parse(row["monto"].ToString());
        lista.Add(report);
            }
            return lista;
        }
    }
}
