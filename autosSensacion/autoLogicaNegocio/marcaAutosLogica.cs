﻿using autoEntidades;
using autosDatos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoLogicaNegocio
{
   public class marcaAutosLogica
    {
        public List<marcaAutoEntidad> ObtenerTodos()
        {
            List<marcaAutoEntidad> lista = new List<marcaAutoEntidad>();

            DataSet ds = marcaAutoDatos.SeleccionarTodos();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                marcaAutoEntidad marca = new marcaAutoEntidad();
                marca.id = Convert.ToInt16(row["id"].ToString());
                marca.descripcion = row["descripcion"].ToString();
                lista.Add(marca);
            }

            return lista;
        }
    }
}
