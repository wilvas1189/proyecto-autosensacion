﻿using autoDatos;
using autoEntidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoLogicaNegocio
{
   public class empleadoLogica
    {
        public void Insertar(empleadoEntidad empleado)
        {
            empleadoDatos.Insertar(empleado);
        }

        public empleadoEntidad ObtenerUsuario(string id, string pass)
        {
            empleadoEntidad empl = new empleadoEntidad();
            DataSet ds = empleadoDatos.SeleccionarEmplIngreso(id, pass);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                empl.Nombre = row["Nombre"].ToString();
                empl.estaActivo=Convert.ToBoolean( row["estaActivo"].ToString());
                empl.idRolUsuario = Convert.ToInt16(row["idRolUsuario"].ToString());
                empl.idPersona = row["idPersona"].ToString();

            }
            return empl;
        }

        public void ModificarEmplado(personaEntidad persona, empleadoEntidad emp)
        {
            empleadoDatos.ModificarEmpleado(persona,emp);
        }


    }
}
