﻿using autoEntidades;
using autosDatos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoLogicaNegocio
{
    public class tipoGastosLogica
    {
        public void Nuevo(tipoGastoEntidad tipoGasto)
        {
            tipoGastosDatos.Insertar(tipoGasto.descripcion);
        }

        public List<tipoGastoEntidad> ObtenerTodos()
        {
            List<tipoGastoEntidad> lista = new List<tipoGastoEntidad>();

            DataSet ds = tipoGastosDatos.SeleccionarTodos();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                tipoGastoEntidad tipo = new tipoGastoEntidad();
                tipo.id = Convert.ToInt16(row["id"].ToString());
                tipo.descripcion = row["descripcion"].ToString();
                lista.Add(tipo);
            }

            return lista;
        }

        public tipoGastoEntidad ObtenerTodosFiltro(int id)
        {
            DataSet ds = tipoGastosDatos.SeleccionarTodosFiltro(id);
            tipoGastoEntidad gastos = new tipoGastoEntidad();
            foreach (DataRow row in ds.Tables[0].Rows)
            {

                gastos.id =Convert.ToInt16( row["id"].ToString());
                gastos.descripcion = row["descripcion"].ToString();
            }
            return gastos;
        }

        public void Modificar(tipoGastoEntidad tipo)
        {
            tipoGastosDatos.Modificar(tipo.id,tipo.descripcion);
        }
    }
}
