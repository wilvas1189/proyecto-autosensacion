﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="autosSensacion.Login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="UTF-8" />
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <meta name="description" content="Free Admin Template Based On Twitter Bootstrap 3.x" />
    <meta name="author" content="" />

    <meta name="msapplication-TileColor" content="#5bc0de" />
    <meta name="msapplication-TileImage" content="assets/img/metis-tile.png" />

    <!-- Bootstrap -->
    <link href="lib/bootstrap/css/bootstrap.css" rel="stylesheet" />

    <!-- Font Awesome -->
    <link href="lib/fontAwesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Metis core stylesheet -->
    <link href="css/main.css" rel="stylesheet" />
    <!-- metisMenu stylesheet -->
    <link href="lib/metisMenu/metisMenu.css" rel="stylesheet" />

    <!-- animate.css stylesheet -->
    <link href="lib/animacion/animate.css" rel="stylesheet" />


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

    <link href="css/style-switcher.css" rel="stylesheet" />
    <link href="css/theme.css" rel="stylesheet" />

</head>

<body class="login">
    <form  runat="server">
        <div class="form-signin">
            <div class="text-center">
                <img src="img/transport%20(3).png" />
                <h4>AUTOS SENSACIÓN</h4>
                <br />
            </div>
            <div class="tab-content">
                <div id="login" class="tab-pane active">

                    <p class="text-muted text-center">
                        Ingrese su nombre de usuario y la contraseña
               
                    </p>
                    <asp:TextBox ID="txtUsuario" runat="server" placeholder="Nombre" class="form-control top"></asp:TextBox>
                   <asp:TextBox ID="txtContraseña" runat="server" placeholder="Contraseña" TextMode="Password" class="form-control bottom"></asp:TextBox>

                    <asp:Button ID="btnIngresar" runat="server" Text="Ingresar" CssClass="btn btn-lg btn-success btn-block" OnClick="btnIngresar_Click" />

                </div>
                <div id="signup" class="tab-pane">

                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:TextBox ID="txtCedula" runat="server" placeholder="Cédula" class="form-control top"></asp:TextBox>
                    <asp:TextBox ID="txtNombre" runat="server" placeholder="Nombre" class="form-control top"></asp:TextBox>
                    <asp:TextBox ID="txtAp1" runat="server" placeholder="Apellido 1" class="form-control top"></asp:TextBox>
                    <asp:TextBox ID="txtAp2" runat="server" placeholder="Apellido 2" class="form-control top"></asp:TextBox>
                    <asp:TextBox ID="txtFecha" runat="server" placeholder="Fecha Nacimiento" class="form-control top"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender Format="dd/MM/yyyy"
                        BehaviorID="txtFecha_CalendarExtender" TargetControlID="txtFecha" ID="txtFecha_CalendarExtender" runat="server" />
                    <asp:TextBox ID="txtTelefono" runat="server" placeholder="Telefono" class="form-control middle"></asp:TextBox>
                    <asp:TextBox ID="txtEmail" runat="server" placeholder="Correo@domain.com" class="form-control middle"></asp:TextBox>
                    <asp:TextBox ID="txtPassword" runat="server" placeholder="Contraseña" TextMode="Password" class="form-control bottom"></asp:TextBox>
                    <asp:TextBox ID="txtConfirmacion" runat="server" placeholder="Confirmar Contraseña" TextMode="Password" class="form-control bottom"></asp:TextBox>
                    <br />
                    
                    <asp:Button ID="btnRegistrar" runat="server" Text="Registrar" style="color:white" CssClass="btn btn-lg btn-success btn-block" OnClick="btnRegistrar_Click" />



                </div>

            </div>
            <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
            <div class="text-center">
                <br />
                <ul class="list-inline">
                    <li><a class="text-muted" href="#login" data-toggle="tab">Ingresar</a></li>
                    <li><a class="text-muted" href="#signup" data-toggle="tab">Registrarse</a></li>
                </ul>
            </div>
        </div>

    </form>
    <!--jQuery -->
    <script src="lib/jquery/jquery.js"></script>

    <!--Bootstrap -->
    <script src="lib/bootstrap/js/bootstrap.js"></script>


    <script type="text/javascript">
        (function ($) {
            $(document).ready(function () {
                $('.list-inline li > a').click(function () {
                    var activeForm = $(this).attr('href') + ' > form';
                    //console.log(activeForm);
                    $(activeForm).addClass('animated fadeIn');
                    //set timer to 1 seconds, after that, unload the animate animation
                    setTimeout(function () {
                        $(activeForm).removeClass('animated fadeIn');
                    }, 1000);
                });
            });
        })(jQuery);
    </script>
</body>
</html>
