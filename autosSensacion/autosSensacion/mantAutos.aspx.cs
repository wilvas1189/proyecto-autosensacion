﻿using autoEntidades;
using autoWebServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace autosSensacion
{
    public partial class mantAutos : System.Web.UI.Page
    {
        public AutoWebService servicio = new AutoWebService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                llenarListas();
                lblMsj.Text = "";
               
            }
        }

        public void llenarListas()
        {
            List<marcaAutoEntidad> marcas = new List<marcaAutoEntidad>();
            marcas = servicio.ObtenerTodasMarcas();
            ddlMarca.DataSource = marcas;
            ddlMarca.DataTextField = "descripcion";
            ddlMarca.DataValueField = "id";
            ddlMarca.DataBind();

            List<tipoAutoEntidad> tipo = new List<tipoAutoEntidad>();
            tipo = servicio.ObtenerTodosTipo();
            ddlTipoAuto.DataSource = tipo;
            ddlTipoAuto.DataTextField = "descripcion";
            ddlTipoAuto.DataValueField = "id";
            ddlTipoAuto.DataBind();
        }

       
        protected void btnGuardar_Click(object sender, EventArgs e)
        {

            automovilEntidad autos = new automovilEntidad();
            autos = servicio.ObtenerAutoPlaca(txtPlaca.Text, txtVin.Text);

            if (autos.placa == txtPlaca.Text || autos.VIN == txtVin.Text)
            {
                lblMsj.Text = "* Ya existe un vehículo con el mismo número de placa o de VIN";
            }
            else
            {
                if (fileFoto.HasFile)
                {

                        using (BinaryReader reader = new BinaryReader(fileFoto.PostedFile.InputStream))
                        {
                            autos.fotografia = reader.ReadBytes(fileFoto.PostedFile.ContentLength);
                        }

                }

                autos.placa = txtPlaca.Text;
                autos.VIN = txtVin.Text;
                autos.idMarcaAuto = Convert.ToInt16(ddlMarca.SelectedValue);
                autos.modelo = txtModelo.Text;
                autos.cilindraje = Convert.ToDecimal(txtCilindraje.Text);
                autos.numPasajeros = Convert.ToDecimal(txtCantPasajeros.Text);
                autos.idTipoAuto = Convert.ToInt16(ddlTipoAuto.SelectedValue);
                if (rdbNo.Checked)
                {
                    autos.estaActivo = false;
                }
                if (rdbSi.Checked)
                {
                    autos.estaActivo = true;
                }

                servicio.insertarAutos(autos);
                lblMsj.ForeColor = System.Drawing.Color.Green;
                lblMsj.Text = "* Datos guardados correctamente";
            }
        }

       
    }
}