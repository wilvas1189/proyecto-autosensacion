﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterAdministrador.Master" AutoEventWireup="true" CodeBehind="mantEmpleados.aspx.cs" Inherits="autosSensacion.mantEmpleados" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="dark">
        <h5>MANTENIMIENTO EMPLEADOS</h5>
    </header>
    <div id="collapse2" class="body">
        <div class="form-group">

            <label class="control-label col-lg-4">Identificación</label>
            <div class="col-lg-4">
                <asp:TextBox ID="txtCedula" runat="server"></asp:TextBox>
                <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="#cc0000" runat="server" ErrorMessage="* Digite la identificación" ControlToValidate="txtCedula"></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-4">Nombre</label>
            <div class=" col-lg-4">
                <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox>
                <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ForeColor="#cc0000" runat="server" ErrorMessage="* Digite el nombre" ControlToValidate="txtNombre"></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-4">Apellido 1</label>
            <div class=" col-lg-4">
                <asp:TextBox ID="txtAp1" runat="server"></asp:TextBox>
                <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="#cc0000" runat="server" ErrorMessage="* Digite el primer apellido" ControlToValidate="txtAp1"></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-4">Apellido 2</label>
            <div class=" col-lg-4">
                <asp:TextBox ID="txtAp2" runat="server"></asp:TextBox>
                <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ForeColor="#cc0000" runat="server" ErrorMessage="* Digite el segundo Apellido" ControlToValidate="txtAp2"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-4">Fecha Nacimiento</label>
            <div class=" col-lg-4">
                <asp:TextBox ID="txtFecha" runat="server"></asp:TextBox>
                <ajaxToolkit:CalendarExtender Format="dd/MM/yyyy"
                    BehaviorID="txtFecha_CalendarExtender" TargetControlID="txtFecha" ID="txtFecha_CalendarExtender" runat="server" />
                <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ForeColor="#cc0000" runat="server" ErrorMessage="* Seleccione la Fecha" ControlToValidate="txtFecha"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-4">Telefono</label>
            <div class=" col-lg-4">
                <asp:TextBox ID="txtTelefono" runat="server"></asp:TextBox>
                <ajaxToolkit:MaskedEditExtender runat="server"
                    Mask="9-999-9999"
                    MaskType="Date"
                    CultureDatePlaceholder="" CultureTimePlaceholder="" CultureDecimalPlaceholder=""
                    CultureThousandsPlaceholder="" CultureDateFormat="" CultureCurrencySymbolPlaceholder=""
                    CultureAMPMPlaceholder="" Century="2000" BehaviorID="txtTelefono_MaskedEditExtender"
                    TargetControlID="txtTelefono" ID="txtTelefono_MaskedEditExtender"></ajaxToolkit:MaskedEditExtender>
                <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ForeColor="#cc0000" runat="server" ErrorMessage="* Digite el telefono" ControlToValidate="txtTelefono"></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-4">Correo</label>
            <div class=" col-lg-4">
                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ForeColor="#cc0000" runat="server" ErrorMessage="* Digite el correo" ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
                <br />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="El email no tiene formato correcto" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    Display="Dynamic" ForeColor="#cc0000"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-4">Rol Empleado</label>
            <div class=" col-lg-4">
                <asp:DropDownList ID="ddlRol" runat="server" AppendDataBoundItems="true">
                    <asp:ListItem>--Seleccione el Rol--</asp:ListItem>
                    <asp:ListItem Value="2">Empleado</asp:ListItem>
                    <asp:ListItem Value="3">Administrador </asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-4">Contraseña</label>
            <div class=" col-lg-4">
                <asp:TextBox ID="txtContraseña" runat="server" TextMode="Password"></asp:TextBox>
                <ajaxToolkit:PasswordStrength
                    runat="server"
                    StrengthIndicatorType="Text"
                    PreferredPasswordLength="10"
                    PrefixText="Longitud: "
                    MinimumNumericCharacters="0"
                    MinimumSymbolCharacters="0"
                    RequiresUpperAndLowerCaseCharacters="false"
                    TextStrengthDescriptions="Muy Débil;Débil;Medio;Fuerte;Excelente"
                    CalculationWeightings="50;15;15;20"
                    BehaviorID="txtContraseña_PasswordStrength"
                    TargetControlID="txtContraseña"
                    ID="txtContraseña_PasswordStrength"></ajaxToolkit:PasswordStrength>
                <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ForeColor="#cc0000" runat="server" ErrorMessage="* Digite la Contraseña" ControlToValidate="txtContraseña"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-4">Está Activo</label>
            <div class=" col-lg-4">
                <asp:RadioButton ID="rdbSi" Text="SI" GroupName="Activo" runat="server" />

                <asp:RadioButton ID="rdbNo" Text="NO" GroupName="Activo" runat="server" />

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-4"></label>
            <div class=" col-lg-4">
                <asp:Label ID="lblMsj" runat="server" Text="" ForeColor="#cc0000"></asp:Label>
            </div>
        </div>
        <div class="form-actions no-margin-bottom">
            <asp:Button ID="btnGuardar" class="btn btn-primary" runat="server" Text="Guardar" OnClick="btnGuardar_Click" />
        </div>

    </div>
</asp:Content>
