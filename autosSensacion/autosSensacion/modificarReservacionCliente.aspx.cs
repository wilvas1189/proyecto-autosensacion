﻿using autoEntidades;
using autoWebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace autosSensacion
{
    public partial class modificarReservacionCliente : System.Web.UI.Page
    {
        public AutoWebService servicio = new AutoWebService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                datosReservaciones();
            }
        }

        private void datosReservaciones()
        {
            //MIENTRAS SE SOLUCIONA EL LOGIN
            Session.Add("usuarioId","123");
            grvReservaciones.DataSource = servicio.SeleccionarReservacionesPorCliente(Session["usuarioId"].ToString());
            grvReservaciones.DataBind();
        }

        //METODO PARA CARGAR LOS AUTOS QUE SE MUESTRAN EN PANTALLA
        private void Refrescar(int idTipoAuto)
        {
            try
            {
                rblListaAutos.Items.Clear();
                List<automovilEntidad> listaAutos = new List<automovilEntidad>();

                if (idTipoAuto != 0)
                {
                    listaAutos = servicio.SeleccionarPotTipoAutoReservar(idTipoAuto);
                }
                else
                {
                    listaAutos = servicio.SeleccionarTodosAutosReservar();
                }

                int contador = 0;

                foreach (automovilEntidad item in listaAutos)
                {

                    ListItem carro = new ListItem("<label for='ContentPlaceHolder1_rblListaAutos_" + contador + "' > Marca: " + item.descripcion +
                        "<br/>Modelo: " + item.modelo +
                        "<br/>Cantidad Pasajeros: " + item.numPasajeros +
                        " <br/></label> <img src='manejadorImgFlota.ashx?placa=" + item.placa + "' width='200px' height='100px' alt='autoMovil'/>", item.placa);
                    rblListaAutos.Items.Add(carro);
                    if (contador == 0) { carro.Selected = true; }

                    contador += 1;
                }
                rblListaAutos.CellPadding = 60;
                rblListaAutos.CellSpacing = 60;
                rblListaAutos.CssClass = "table";

            }
            catch (Exception error)
            {

            }
        }


        protected void ddlTipoAutos_SelectedIndexChanged(object sender, EventArgs e)
        {
            Refrescar(Convert.ToInt16(ddlTipoAutos.SelectedValue));
        }

        protected void btnCalcular_Click(object sender, EventArgs e)
        {
            try
            {
                List<automovilEntidad> listaAutos = servicio.SeleccionarAutoXId(rblListaAutos.SelectedItem.Value);
                tipoAutoEntidad tipoAuto;
                automovilEntidad auto;

                if (listaAutos.Count > 0)
                {
                    auto = listaAutos.Find(a => a.placa == rblListaAutos.SelectedItem.Value);
                    tipoAuto = servicio.SeleccionarTipoAutoPorid(auto.idTipoAuto).Find(a => a.id == auto.idTipoAuto);

                    lblMarca.Text = auto.descripcion;
                    lblModelo.Text = auto.modelo;
                    lblTipoAuto.Text = auto.tipoAuto;
                    lblCantPasajeros.Text = auto.numPasajeros.ToString();
                    lblPrecio.Text = tipoAuto.precio.ToString();
                    lblNumeroDias.Text = hiCantDias.Value;
                    int dias = Convert.ToInt16(hiCantDias.Value);
                    lblTotal.Text = (tipoAuto.precio * dias).ToString();
                    this.lblFechIn.Text = txtFechaInicio.Text;
                    this.lblFechDev.Text = txtFechaFin.Text;
                }
            }
            catch (Exception error)
            {

            }
        }
        protected void grvReservaciones_RowEditing(object sender, GridViewEditEventArgs e)
        {

                int idReservacion = Convert.ToInt16(grvReservaciones.DataKeys[e.NewEditIndex].Values[0]);
                if (idReservacion > 0)
                {

                    reservacionEntidad reservacion = servicio.seleccionarReservacionXId(idReservacion);
                    hideIddReservacion.Value = idReservacion.ToString(); ;
                    txtFechaInicio.Text = string.Format("{0:dd/MM/yyyy}", reservacion.fechaEntrega);
                    txtFechaFin.Text = string.Format("{0:dd/MM/yyyy}", reservacion.fechaDevolucion);

                    List<tipoAutoEntidad> tipos = new List<tipoAutoEntidad>();
                    List<tipoAutoEntidad> datosServicio = new List<tipoAutoEntidad>();
                    datosServicio = servicio.ObtenerTodosTipoAuto();


                    tipos.Add(new tipoAutoEntidad { id = 0, descripcion = "Todos" });
                    foreach (tipoAutoEntidad item in datosServicio)
                    {
                        tipos.Add(item);
                    }
                    ddlTipoAutos.DataSource = tipos;
                    ddlTipoAutos.DataTextField = "descripcion";
                    ddlTipoAutos.DataValueField = "id";
                    ddlTipoAutos.DataBind();

                    Refrescar(0);
                    rblListaAutos.SelectedValue = reservacion.idAuto;
                
                    
            }

            popup.Show();
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {



            try
            {

                reservacionEntidad reservacion = servicio.seleccionarReservacionXId(Convert.ToInt16(hideIddReservacion.Value));
                //se anula la antigua factura
                servicio.ModificarFacturaAnular(reservacion.idFactura);
                //se crea el encabezado de la nueva factura
                encabezadoFacturaEntidad factura = new encabezadoFacturaEntidad();

                factura.id = servicio.ObtnerSigNumeroFactura();
                factura.idCliente = reservacion.idCliente;
                factura.idEmpleado = "WEB";
                factura.idNumTarjeta = servicio.SeleccionarTarjetaXIdFactura(reservacion.idFactura);
                factura.detallesFactura = new List<detalleFacturaEntidad>();
                factura.fecha = DateTime.Now;

                //se crean las lineas de detalle

                foreach (ListItem elemento in rblListaAutos.Items)
                {
                    if (elemento.Selected)
                    {
                        List<automovilEntidad> listaAutos = servicio.SeleccionarAutoXId(rblListaAutos.SelectedItem.Value);
                        tipoAutoEntidad tipoAuto;
                        automovilEntidad auto;
                        auto = listaAutos.Find(a => a.placa == elemento.Value);
                        tipoAuto = servicio.SeleccionarTipoAutoPorid(auto.idTipoAuto).Find(a => a.id == auto.idTipoAuto);

                        int dias = Convert.ToInt16(hiCantDias.Value);

                        detalleFacturaEntidad detalle = new detalleFacturaEntidad();
                        detalle.id = 0;
                        detalle.idAutomovil = elemento.Value;
                        detalle.idEncabezadoFactura = 0;
                        detalle.cantidadDias = dias;
                        detalle.precio = tipoAuto.precio * dias;
                        factura.detallesFactura.Add(detalle);
                    }
                }



                //se crea la reservacion
                string[] entrega = txtFechaInicio.Text.ToString().Split('/');
                reservacion.fechaEntrega = new DateTime(Convert.ToInt16(entrega[2]), Convert.ToInt16(entrega[1]), Convert.ToInt16(entrega[0]));
                string[] devolucion = txtFechaFin.Text.ToString().Split('/');
                reservacion.fechaDevolucion = new DateTime(Convert.ToInt16(devolucion[2]), Convert.ToInt16(devolucion[1]), Convert.ToInt16(devolucion[0]));
                reservacion.idAuto = rblListaAutos.SelectedItem.Value;
                reservacion.totalReservacion = lblTotal.Text.Equals("") ? 0 : Convert.ToDecimal(lblTotal.Text);
                reservacion.idFactura = factura.id;

                //guardar facturacion
                servicio.InsertarFacturaModificacion(factura);
                //guardar reservacion
                servicio.ModificarReservacion(reservacion);
            }
            catch (Exception error)
            {

           }
            finally {
                grvReservaciones.EditIndex = -1;
                datosReservaciones();
                popup.Hide();
            }


        }

        protected void cerrar_Click(object sender, ImageClickEventArgs e)
        {
            grvReservaciones.EditIndex = -1;
            datosReservaciones();
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            grvReservaciones.EditIndex = -1;
            datosReservaciones();
        }
    }
}