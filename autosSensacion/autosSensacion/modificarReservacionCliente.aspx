﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPrincipal.Master" AutoEventWireup="true" CodeBehind="modificarReservacionCliente.aspx.cs" Inherits="autosSensacion.modificarReservacionCliente" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/formularioCotizacion.css" rel="stylesheet" />
    <script src="lib/jquery.ui/JS/jquery-ui-1.9.2.custom.min.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    
    <link href="css/formularioCotizacion.css" rel="stylesheet" />
    <header>
        <h5>Modificar Reservaciones</h5>
    </header>
    <div id="collapse1" class="body">
        <div class="form-group">
                                    <asp:GridView ID="grvReservaciones" runat="server" DataKeyNames="id"
                                        CssClass="table table-striped responsive-table"
                                        GridLines="None"
                                        AutoGenerateColumns="false"
                                        OnRowEditing="grvReservaciones_RowEditing">
                                        <Columns>
                                   <asp:commandfield showeditbutton="true"
                                    edittext="Modificar"
                                    canceltext="Cancelar"
                                    updatetext="Guardar"
                                    headertext=""/>
                                            <asp:BoundField DataField="id" HeaderText="Id" ReadOnly="true"></asp:BoundField>
                                            <asp:BoundField DataField="fechaReservacion" HeaderText="Fecha" ReadOnly="true"></asp:BoundField>
                                            <asp:BoundField DataField="fechaEntrega" HeaderText="Entrega" ReadOnly="true"></asp:BoundField>
                                            <asp:BoundField DataField="fechaDevolucion" HeaderText="Devolucion" ReadOnly="true"></asp:BoundField>
                                            <asp:BoundField DataField="descripcion" HeaderText="Descripcion" ReadOnly="true"></asp:BoundField>
                                            <asp:BoundField DataField="totalReservacion" HeaderText="Total" ReadOnly="true"></asp:BoundField>
                                        </Columns>
                                    </asp:GridView>

            <asp:HiddenField ID="hideIddReservacion" value="" runat="server" />
                        <%-- MODAL PARA EDITAR LA RESERVACION --%>
                        <asp:Panel ID="pnlEdit" runat="server" Style="display: none; width: 80%; height: 70%; border-radius: 4px; padding: 10px; background-color: #e2dcdc" Font-Size="Small" ScrollBars="Both">
                            <%-- CONTENIDO DEL EDITOR --%>
                            <div style="float:right;"><asp:ImageButton runat="server" ID="cerrar" Width="25px" Height="25px" ImageUrl="~/img/iconos/cerrar.jpg" ToolTip="CERRAR"  OnClick="cerrar_Click"/></div>
                            <br/>
                            <div class="f1">

                                <h3>Hacer Reservación</h3>
                                <p>Llene el siguiente formulario para hacer su reservación.</p>
                                <div class="f1-steps">
                                    <div class="f1-progress">
                                        <div class="f1-progress-line" data-now-value="16.88" data-number-of-steps="3" style="width: 16.88%;"></div>
                                    </div>
                                    <div class="f1-step active">
                                        <div class="f1-step-icon"><i class="fa fa-calendar"></i></div>
                                        <p>Seleccionar Fechas</p>
                                    </div>
                                    <div class="f1-step">
                                        <div class="f1-step-icon"><i class="fa fa-automobile"></i></div>
                                        <p>Seleccionar Automóvil</p>
                                    </div>
                                    <div class="f1-step">
                                        <div class="f1-step-icon"><i class="fa fa-file"></i></div>
                                        <p>Resumen</p>
                                    </div>
                                </div>

                                <%-- primer parte --%>
                                <fieldset>
                                    <section class="columna">
                                        <asp:Label ID="lblFechaInicio" runat="server" Text="Fecha de Inicio"></asp:Label>
                                        <asp:TextBox ID="txtFechaInicio" runat="server" placeholder="dd/mm/aaaa" CssClass="fieldset__input js__datepicker"></asp:TextBox>
                                       
                                         <%-- Enabled="true"--%>

                                        <asp:Label ID="lblFechaFin" runat="server" Text="Fecha de devolución"></asp:Label>
                                        <asp:TextBox ID="txtFechaFin" runat="server"  placeholder="dd/mm/aaaa" CssClass="fieldset__input js__datepicker"></asp:TextBox>

                                        <%-- Enabled="False"--%>
                                        <br />
                                        <label id="msjFecha" style="color: red"></label>
                                        <br />
                                        <br />
                                        <p>
                                            El vehiculo se puede recoger desde las 6a.m. en la fecha de inicio y se debe devolver a las 8p.m. en la fecha de devolución.
                                        </p>
                                    </section>
                                    <%--Campo para guardar los dias de reservacion--%>
                                    <asp:HiddenField ID="hiCantDias" Value="0" runat="server" />

                                    <div class="f1-buttons">
                                        <asp:Button ID="Button2" runat="server" CssClass="btnForm btn-submit" OnClick="btnCancelar_Click" Text="Cancelar"></asp:Button>
                                        <button type="button" onclick="establecerDias(this.form);" class="btn btn-next">Siguiente</button>
                                    </div>
                                </fieldset>

                                <%-- Segunda parte SE ESCOGE EL CARRO A UTILIZAR--%>
                                <fieldset>
                                    <br />
                                    <br />
                                    <asp:Label ID="Label2" runat="server" Text="Todos nuestros vehiculos cuentan con:   " />
                                    <asp:Label ID="Label3" runat="server">Maletero <span class="glyphicon glyphicon-briefcase"></span></asp:Label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label4" runat="server">Sistema Multimedia <span class="glyphicon glyphicon-music"></span></asp:Label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label5" runat="server">Asistencia en carretera <span class="glyphicon glyphicon glyphicon-road"></span> </asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label6" runat="server">Aire Acondicionado <span class="glyphicon glyphicon-refresh"></span></asp:Label>
                                    <br />
                                        <asp:Label ID="Label1" runat="server" Text="Seleccionar por: "></asp:Label>

                                        <asp:DropDownList ID="ddlTipoAutos" Style="color: black; align-items: center; border-radius: 4px; height: 25px; width: 150px;" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoAutos_SelectedIndexChanged">
                                        </asp:DropDownList>


                                        <div class="f1-buttons">
                                            <asp:Button ID="Button3" runat="server" CssClass="btnForm btn-submit" Text="Cancelar" OnClick="btnCancelar_Click"></asp:Button>
                                            <button type="button" class="btn btn-previous">Anterior</button>
                                            <asp:Button runat="server" ID="btnCalcular2" CssClass="btnForm btn-next" Text="Siguiente" OnClick="btnCalcular_Click"></asp:Button>
                                        </div>
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
  
                                                    <asp:RadioButtonList ID="rblListaAutos" align="center" runat="server">
                                                    </asp:RadioButtonList>

                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlTipoAutos" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>

                                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel2">
                                            <ProgressTemplate>
                                                <img src="img/iconos/cargando.gif" />
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                        <%--<asp:Timer ID="Timer1" Interval="4000" OnTick="Timer1_Tick" runat="server"></asp:Timer>--%>
                &nbsp;&nbsp;&nbsp;&nbsp;<br />

                                        <div class="f1-buttons">
                                            <asp:Button ID="Button1" runat="server" CssClass="btnForm btn-submit" Text="Cancelar" OnClick="btnCancelar_Click"></asp:Button>
                                            <button type="button" class="btn btn-previous">Anterior</button>
                                            <asp:Button runat="server" ID="btnCalcular" CssClass="btnForm btn-next" Text="Siguiente" OnClick="btnCalcular_Click"></asp:Button>
                                        </div>
 

                                </fieldset>

                                <%-- Resumen de la reservacion--%>
                                <fieldset>
                                    <div class="container" style="vertical-align: central">
                                        <asp:UpdatePanel ID="upPanConfirmacion" runat="server">
                                            <ContentTemplate>

                                                <asp:Panel ID="Panel3" runat="server">
                                                    <%-- Si la seleccion de la lista de radios cambia se actualiza el valor--%>
                                                    <table class="table-condensed" style="width: 100%; vertical-align: central; align-self: center;">
                                                        <tr>

                                                            <td style="background-color: #33cc33" colspan="4">Vehiculo</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Marca: </td>
                                                            <td>
                                                                <asp:Label ID="lblMarca" runat="server" Text=""></asp:Label></td>

                                                        </tr>
                                                        <tr>

                                                            <td>Modelo: </td>
                                                            <td>
                                                                <asp:Label ID="lblModelo" runat="server" Text=""></asp:Label></td>

                                                        </tr>

                                                        <tr>
                                                            <td>Tipo de Automovil: </td>
                                                            <td>
                                                                <asp:Label ID="lblTipoAuto" runat="server" Text=""></asp:Label></td>
                                                        </tr>

                                                        <tr>
                                                            <td>Cantidad de Pasajeros: </td>
                                                            <td>
                                                                <asp:Label ID="lblCantPasajeros" runat="server" Text=""></asp:Label></td>



                                                            <tr>
                                                                <td style="background-color: #33cc33" colspan="4">Monto  a Cancelar</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Fecha Inicio: </td>
                                                                <td>
                                                                    <asp:Label ID="lblFechIn" runat="server" Text=""></asp:Label></td>

                                                            </tr>
                                                            <tr>
                                                                <td>Fecha Devolucion: </td>
                                                                <td>
                                                                    <asp:Label ID="lblFechDev" runat="server" Text=""></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Precio por dia: </td>
                                                                <td>
                                                                    <asp:Label ID="lblPrecio" runat="server" Text=""></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Cantidad de dias: </td>
                                                                <td>
                                                                    <asp:Label ID="lblNumeroDias" runat="server" Text=""></asp:Label></td>
                                                            </tr>

                                                            <tr>
                                                                <td>Total</td>
                                                                <td>
                                                                    <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label></td>
                                                            </tr>
                                                    </table>
                                                </asp:Panel>

                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnCalcular" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnCalcular2" EventName="Click" />
                                                
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="upPanConfirmacion">
                                            <ProgressTemplate>
                                                <img src="img/iconos/cargando.gif" />
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>

                                        <div class="f1-buttons">
                                            <div class="btn btn-next">
                                                <asp:Button ID="btnCancelar" runat="server" CssClass="btn btn-submit" Text="Cancelar" OnClick="btnCancelar_Click"></asp:Button>
                                            </div>
                                            <button type="button" class="btn btn-previous">Anterior</button>
                                            <div class="btn btn-next">
                                                <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-next" Text="Guardar"  OnClick="btnGuardar_Click"></asp:Button>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                                    </asp:Panel>
                        <%--FIN DEL MODAL--%>
                        <asp:LinkButton ID="lnkFake" runat="server" Text="" ></asp:LinkButton>
                        <ajaxToolkit:ModalPopupExtender ID="popup" runat="server" DropShadow="false"
                            PopupControlID="pnlEdit" TargetControlID="lnkFake" >
                        </ajaxToolkit:ModalPopupExtender>
<%--                    </ContentTemplate>
                </asp:UpdatePanel>--%>



           <%-- </div>--%>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

       <script type="text/javascript" src="lib/jquery/jquery.js"></script>
    
        <script src="js/formularioPasos.js"></script>
     
<script src="lib/jquery.ui/JS/jquery-ui-1.9.2.custom.min.js"></script>
    <script>

        var desde = new Date();
        var hast = new Date();

        $(function () {
            $.datepicker.setDefaults($.datepicker.regional["es"]);
            var dateFormat = "dd/mm/yy",

            

              from = $("#ContentPlaceHolder1_txtFechaInicio")
                .datepicker({
                    defaultDate: "+1w",
                    dateFormat:"dd/mm/yy",
                    changeMonth: true,
                    changeYear: true
                    
                })
                .on("change", function () {

                    if (esFechaValida(this.value)) {
                        to.datepicker("option", "minDate", getDate(this));
                        $("#msjFecha").text("");
                       
                    } else {
                        this.value = null;
                        $("#msjFecha").text("Las fechas deben estar con el formato dd/mm/aaaa. Ejemplo 23/04/2017");
                        $("#ContentPlaceHolder1_txtFechaInicio").text("");
                        to.datepicker("option", "minDate", getDate(this));
                    }

                }),



              to = $("#ContentPlaceHolder1_txtFechaFin").datepicker({
                  defaultDate: "+1w",
                  dateFormat: "dd/mm/yy",
                  changeMonth: true,
                  changeYear: true
              })
              .on("change", function () {

                  if (esFechaValida(this.value)) {
                      from.datepicker("option", "maxDate", getDate(this));
                      $("#msjFecha").text("");
                  } else {
                      this.value = null;
                      $("#msjFecha").text("Las fechas deben estar con el formato dd/mm/aaaa. Ejemplo 23/04/2017");
                      $("#ContentPlaceHolder1_txtFechaFin").text("");
                      from.datepicker("option", "maxDate", getDate(this));
                  }
                  
              });

            //funcion que revisa la fecha ingresada para asignarlo al otro elemento fecha
            function getDate( element ) {
                var date;
                try {
                    date = $.datepicker.parseDate( dateFormat, element.value );
                } catch( error ) {
                    date = null;
                }
 
                return date;
            }



            function esFechaValida(dateString) {
                // revisar el patrón
                if (!/^\d{2}\/\d{2}\/\d{4}$/.test(dateString))
                    return false;

                // convertir los numeros a enteros
                var parts = dateString.split("/");
                var dia = parseInt(parts[0], 10);
                var mes = parseInt(parts[1], 10);
                var ano = parseInt(parts[2], 10);

                // Revisar los rangos de año y mes
                if ((ano < 1000) || (ano > 3000) || (mes == 0) || (mes > 12))
                    return false;

                var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

                // Ajustar para los años bisiestos
                if (ano % 400 == 0 || (ano % 100 != 0 && ano % 4 == 0))
                    monthLength[1] = 29;

                // Revisar el rango del dia
                return dia > 0 && dia <= monthLength[mes - 1];
            };

  } );
  </script>
    
    	<script type="text/javascript">
			
		 
		function establecerDias(frm) {
		    var dtElem1 = frm.elements['ContentPlaceHolder1_txtFechaInicio'];
		    var dtElem2 = frm.elements['ContentPlaceHolder1_txtFechaFin'];
		    var resultElem = frm.elements['ContentPlaceHolder1_hiCantDias'];
			 
		// Retorna si el elemento no existe
			if(!dtElem1 || !dtElem2 || !resultElem) {
		return;
			}
			 
			//tomando en cuenta que el divisor es '/'.
			var x = dtElem1.value;
			var y = dtElem2.value;
			var arr1 = x.split('/');
			var arr2 = y.split('/');
			 
		// Si hay algun error asigna el valor a 0
		if(!arr1 || !arr2 || arr1.length != 3 || arr2.length != 3) {
		resultElem.value = "0";
		return;
			}
			 
		var dt1 = new Date();
		dt1.setFullYear(arr1[2], arr1[1], arr1[0]);
		var dt2 = new Date();
		dt2.setFullYear(arr2[2], arr2[1], arr2[0]);

		resultElem.value = Math.ceil( (dt2.getTime() - dt1.getTime()) / (60 * 60 * 24 * 1000))+1;
		}


		</script>

</asp:Content>
