﻿using autoEntidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using autoWebServices;

namespace autosSensacion
{

    public partial class reporteFactCliente : System.Web.UI.Page
    {
        AutoWebService servicio = new AutoWebService();


        protected void Page_PreInit(object sender, EventArgs e)
        {

            if (Session["rol"] != null)
            {
                int rol = Convert.ToInt16(Session["rol"].ToString());

                if (rol == 3)
                {
                    this.MasterPageFile = "~/masterAdministrador.Master";
                }
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {



           
            if (!IsPostBack)
            {


                servicio.ObtenerTodosClientes();

                List<personaEntidad> pivote = new List<personaEntidad>();
                List<personaEntidad> clientes = new List<personaEntidad>();

                pivote = servicio.ObtenerTodosClientes();
                foreach (personaEntidad item in pivote)
                {
                    personaEntidad cliente = new personaEntidad();
                    cliente.id = item.id;
                    cliente.Nombre = (item.id + "-" + item.Nombre + " " + item.Apellido1 + " " + item.Apellido2);
                    clientes.Add(cliente);
                }

                ddlCliente.DataSource = clientes;
                ddlCliente.DataTextField = "Nombre";
                ddlCliente.DataValueField = "id";
                ddlCliente.DataBind();

            }
        }

        protected void btnMostrar_Click(object sender, EventArgs e)
        {
            rptFactCliente.LocalReport.Refresh();
        }
    }
}