﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using autoWebServices;
namespace autosSensacion
{
    /// <summary>
    /// Pagina que pemite desplegar las imagenes en las demas paginas
    /// </summary>
    public class manejadorImgFlota : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            AutoWebService servicio = new AutoWebService();

            string placa = context.Request.Params["placa"];

            context.Response.Clear();
            Byte[] bytes = servicio.SeleccionarFotoAuto(placa);
            context.Response.ContentType = "image/jpg";
            context.Response.BinaryWrite(bytes);
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}