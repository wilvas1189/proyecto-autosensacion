﻿using autoEntidades;
using autoWebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace autosSensacion
{
    public partial class cotizacionDefault : System.Web.UI.Page
    {
        AutoWebService servicio = new AutoWebService();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<tipoAutoEntidad> tipos = new List<tipoAutoEntidad>();
                List<tipoAutoEntidad> datosServicio = new List<tipoAutoEntidad>();
                datosServicio = servicio.ObtenerTodosTipoAuto();


                tipos.Add(new tipoAutoEntidad { id = 0, descripcion = "Todos" });
                foreach (tipoAutoEntidad item in datosServicio)
                {
                    tipos.Add(item);
                }
                ddlTipoAutos.DataSource = tipos;
                ddlTipoAutos.DataTextField = "descripcion";
                ddlTipoAutos.DataValueField = "id";
                ddlTipoAutos.DataBind();

                Refrescar(0);
            }
        }



        //METODO PARA CARGAR LOS AUTOS QUE SE MUESTRAN EN PANTALLA
        private void Refrescar(int idTipoAuto)
        {
            try
            {
                rblListaAutos.Items.Clear();
                List<automovilEntidad> listaAutos = new List<automovilEntidad>();

                if (idTipoAuto != 0)
                {
                    listaAutos = servicio.SeleccionarPotTipoAutoReservar(idTipoAuto);
                }
                else
                {
                    listaAutos = servicio.SeleccionarTodosAutosReservar();
                }

                int contador = 0;

                foreach (automovilEntidad item in listaAutos)
                {

                    ListItem carro = new ListItem("<label for='ContentPlaceHolder1_rblListaAutos_" + contador + "' > Marca: " + item.descripcion +
                        "<br/>Modelo: " + item.modelo +
                        "<br/>Cantidad Pasajeros: " + item.numPasajeros +
                        " <br/></label> <img src='manejadorImgFlota.ashx?placa=" + item.placa + "' width='350px' height='200px' alt='autoMovil'/>", item.placa);
                    rblListaAutos.Items.Add(carro);
                    if (contador == 0) { carro.Selected = true; }

                    contador += 1;
                }
                rblListaAutos.CellPadding = 60;
                rblListaAutos.CellSpacing = 60;
                rblListaAutos.CssClass = "table";

            }
            catch (Exception error)
            {

            }
        }


        protected void ddlTipoAutos_SelectedIndexChanged(object sender, EventArgs e)
        {
            Refrescar(Convert.ToInt16(ddlTipoAutos.SelectedValue));
        }

        protected void btnCalcular_Click(object sender, EventArgs e)
        {
            try
            {
                List<automovilEntidad> listaAutos = servicio.SeleccionarAutoXId(rblListaAutos.SelectedItem.Value);
                tipoAutoEntidad tipoAuto;
                automovilEntidad auto;
                if (listaAutos.Count > 0)
                {
                    auto = listaAutos.Find(a => a.placa == rblListaAutos.SelectedItem.Value);
                    tipoAuto = servicio.SeleccionarTipoAutoPorid(auto.idTipoAuto).Find(a => a.id == auto.idTipoAuto);

                    lblMarca.Text = auto.descripcion;
                    lblModelo.Text = auto.modelo;
                    lblTipoAuto.Text = auto.tipoAuto;
                    lblCantPasajeros.Text = auto.numPasajeros.ToString();
                    lblPrecio.Text = tipoAuto.precio.ToString();
                    lblNumeroDias.Text = hiCantDias.Value;
                    int dias = Convert.ToInt16(hiCantDias.Value);
                    lblTotal.Text = (tipoAuto.precio * dias).ToString();
                    this.lblFechIn.Text = txtFechaInicio.Text;
                    this.lblFechDev.Text = txtFechaFin.Text;
                }
            }
            catch (Exception error)
            {

            }
        }
    }
}