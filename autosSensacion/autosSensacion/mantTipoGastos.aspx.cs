﻿using autoEntidades;
using autoWebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace autosSensacion
{
    public partial class mantTipoGastos : System.Web.UI.Page
    {
        public AutoWebService servicio = new AutoWebService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblMsj.Text = "";
                txtDescripcion.Text = "";
            }
        }

        

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
           
           tipoGastoEntidad tipo = new tipoGastoEntidad();
           tipo.descripcion = txtDescripcion.Text;
           servicio.insertarTipoGasto(tipo);
           lblMsj.Text = "Datos guardados correctamente";
            

        }

       
    }
}