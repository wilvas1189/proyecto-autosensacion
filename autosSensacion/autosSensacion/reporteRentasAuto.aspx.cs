﻿using autoEntidades;
using autoWebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace autosSensacion
{
    public partial class reporteRentasAuto : System.Web.UI.Page
    {
        public AutoWebService servicio = new AutoWebService();


        protected void Page_PreInit(object sender, EventArgs e)
        {

            if (Session["rol"] != null)
            {
                int rol = Convert.ToInt16(Session["rol"].ToString());

                if (rol == 3)
                {
                    this.MasterPageFile = "~/masterAdministrador.Master";
                }
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                List<automovilEntidad> pivote = new List<automovilEntidad>();
                List<automovilEntidad> autos = new List<automovilEntidad>();

                pivote = servicio.ObtenerTodosAutos();
                foreach (automovilEntidad item in pivote)
                {
                    automovilEntidad auto = new automovilEntidad();
                    auto.placa = item.placa;
                    auto.descripcion = (item.placa + "-" + item.modelo);
                    autos.Add(auto);
                }
                ddlPlaca.DataSource = autos;
                ddlPlaca.DataTextField = "descripcion";
                ddlPlaca.DataValueField = "placa";
                ddlPlaca.DataBind();

            }
        }

        protected void btnMostrar_Click(object sender, EventArgs e)
        {
            ReportViewer1.LocalReport.Refresh();
        }
    }
}