﻿using autoEntidades;
using autoWebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace autosSensacion
{
    public partial class mantClientes : System.Web.UI.Page
    {
        public AutoWebService servicio = new AutoWebService();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                personaEntidad persona = new personaEntidad();
                persona = servicio.ObtenerPersona(txtCedula.Text);
                clienteEntidad cliente = new clienteEntidad();
                if (persona.id != null)
                {
                    lblMsj.Text = "* La persona ya se encuentra registrada";
                }
                else
                {
                    persona.id = txtCedula.Text;
                    persona.Nombre = txtNombre.Text;
                    persona.Apellido1 = txtAp1.Text;
                    persona.Apellido2 = txtAp2.Text;
                    persona.fechaNacimiento = Convert.ToDateTime(txtFecha.Text);
                    persona.telContacto = txtTelefono.Text;
                    persona.correo = txtEmail.Text;

                    cliente.idPersona = txtCedula.Text;
                    cliente.fechaIngreso = DateTime.Today;
                    cliente.contrasenna = txtContraseña.Text;
                    cliente.idRolUsuario = 1;

                    servicio.insertarPersona(persona);
                    servicio.insertarCliente(cliente);
                    lblMsj.Text = "* Datos registrados correctamente";
                }
            }
        }
    }
}