﻿using autoEntidades;
using autoWebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace autosSensacion
{
    public partial class modiTipoGastos : System.Web.UI.Page
    {
        public AutoWebService servicio = new AutoWebService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                datosTipoGasto();
            }
        }
        private void datosTipoGasto()
        {
            grvTipoGastos.DataSource = servicio.ObtenerTodosTipoGasto();
            grvTipoGastos.DataBind();
        }

        protected void grvTipoGastos_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

            grvTipoGastos.EditIndex = -1;
            datosTipoGasto();
        }

        protected void grvTipoGastos_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grvTipoGastos.EditIndex = e.NewEditIndex;
            datosTipoGasto();
        }

        protected void grvTipoGastos_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            tipoGastoEntidad tipo = new tipoGastoEntidad();

            tipo.id = Convert.ToInt16(grvTipoGastos.DataKeys[e.RowIndex].Values[0]);
            tipo.descripcion = ((TextBox)grvTipoGastos.Rows[e.RowIndex].FindControl("txtDescripcionE")).Text;


            servicio.modificarTipoGasto(tipo);

            grvTipoGastos.EditIndex = -1;
            datosTipoGasto();
        }
    }
}