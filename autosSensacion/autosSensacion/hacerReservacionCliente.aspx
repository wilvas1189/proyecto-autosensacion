﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPrincipal.Master" AutoEventWireup="true" CodeBehind="hacerReservacionCliente.aspx.cs" Inherits="autosSensacion.hacerReservacionCliente" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/formularioPasos.css" rel="stylesheet" />
    <link href="lib/jquery.ui/CSS/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="form-box">

        <asp:PlaceHolder ID="PlaceHolder1" runat="server">
        <div class="f1">

            <h3>Hacer Reservación</h3>
            <p>Siga los pasos para realizar su cotización.</p>
            <div class="f1-steps">
                <div class="f1-progress">
                    <div class="f1-progress-line" data-now-value="12.66" data-number-of-steps="4" style="width: 12.66%;"></div>
                </div>
                <div class="f1-step active">
                    <div class="f1-step-icon"><i class="fa fa-calendar"></i></div>
                    <p>Seleccionar Fechas</p>
                </div>
                <div class="f1-step">
                    <div class="f1-step-icon"><i class="fa fa-automobile"></i></div>
                    <p>Seleccionar Automóvil</p>
                </div>
                <div class="f1-step">
                    <div class="f1-step-icon"><i class="fa fa-book"></i></div>
                    <p>Resumen</p>
                </div>

                <div class="f1-step">
                    <div class="f1-step-icon"><i class="fa fa-money"></i></div>
                    <p>Información de Pago</p>
                </div>
            </div>


            <%-- primer parte --%>
            <fieldset>
                <section class="columna">
                    <asp:Label ID="lblFechaInicio" runat="server" Text="Fecha de Inicio"></asp:Label>
                    <asp:TextBox ID="txtFechaInicio" runat="server"  placeholder="dd/mm/aaaa" CssClass="fieldset__input js__datepicker"></asp:TextBox>
                    <%-- Enabled="true"--%>

                    <asp:Label ID="lblFechaFin" runat="server" Text="Fecha de devolución"></asp:Label>
                    <asp:TextBox ID="txtFechaFin" runat="server" placeholder="dd/mm/aaaa" CssClass="fieldset__input js__datepicker"></asp:TextBox>
                    <%-- Enabled="False"--%>
                    <br />
                    <label id="msjFecha" style="color: red"></label>
                    <br />
                    <br />
                    <p>
                        El vehiculo se puede recoger desde las 6a.m. en la fecha de inicio y se debe devolver a las 8p.m. en la fecha de devolución.
                    </p>
                </section>
                <%--Campo para guardar los dias de reservacion--%>
                <asp:HiddenField ID="hiCantDias" Value="0" runat="server" />
                
                <div class="f1-buttons">
                    <button type="button" onclick="establecerDias(this.form);"  class="btn btn-next">Siguiente</button>
                </div>
            </fieldset>





            <%-- Segunda parte SE ESCOGE EL CARRO A UTILIZAR--%>
            <fieldset>
                <br />
                <br />
                <asp:Label ID="Label2" runat="server" Text="Todos nuestros vehiculos cuentan con:   " />
                <asp:Label ID="Label3" runat="server">Maletero <span class="glyphicon glyphicon-briefcase"></span></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label4" runat="server">Sistema Multimedia <span class="glyphicon glyphicon-music"></span></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label5" runat="server">Asistencia en carretera <span class="glyphicon glyphicon glyphicon-road"></span> </asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label6" runat="server">Aire Acondicionado <span class="glyphicon glyphicon-refresh"></span></asp:Label>
                <br />
                <br />
                <asp:Panel ID="Panel1" runat="server">
                    <asp:Label ID="Label1" runat="server" Text="Seleccionar por: "></asp:Label>

                    <asp:DropDownList ID="ddlTipoAutos" Style="color: black; align-items: center; border-radius: 4px; height: 25px; width: 150px;" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoAutos_SelectedIndexChanged">
                    </asp:DropDownList>
                </asp:Panel>


                <div class="container">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>

                            <asp:Panel ID="Panel2" runat="server" HorizontalAlign="Center">

                                <asp:RadioButtonList ID="rblListaAutos" align="center" runat="server">
                                </asp:RadioButtonList>

                            </asp:Panel>

                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlTipoAutos" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>

                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                        <ProgressTemplate>
                            <img src="img/iconos/cargando.gif" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <%--<asp:Timer ID="Timer1" Interval="4000" OnTick="Timer1_Tick" runat="server"></asp:Timer>--%>

                </div>

                &nbsp;&nbsp;&nbsp;&nbsp;<br />

                <div class="f1-buttons">
                    <button type="button" class="btn btn-previous">Anterior</button>
                    <asp:Button runat="server" ID="btnSigElegirAuto"  CssClass="btnForm btn-next"  Text="Siguiente" OnClick="btnSigElegirAuto_Click"></asp:Button>
                </div>
            </fieldset>






            <%-- Resumen de la reservacion--%>
            <fieldset style="text-align:center;">
                <div  >
                    <asp:UpdatePanel ID="upPanConfirmacion" runat="server">
                        <ContentTemplate>

                            <asp:Panel ID="Panel3" runat="server" >
                                <%-- Si la seleccion de la lista de radios cambia se actualiza el valor--%>
                                <table class="table-responsive" style="width:80%">
                                    <tr>
                                        <td colspan="2" style="background-color:#33cc33">Informacion del Cliente</td>
                                    </tr>
                                    <tr>
                                        <td>Id Cliente:  <asp:Label ID="lblIdCliente" runat="server" Text=""></asp:Label></td>
                                        <td>Nombre del Cliente: <asp:Label ID="lblNombreCliente" runat="server" Text=""></asp:Label> </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="background-color:#33cc33">Vehiculo</td>
                                    </tr>
                                    <tr>
                                        <td>Marca: <asp:Label ID="lblMarca" runat="server" Text=""></asp:Label> </td>
                                        <td>Modelo: <asp:Label ID="lblModelo" runat="server" Text=""></asp:Label> </td>
                                    </tr>
                                    <tr>
                                        <td>Tipo de Automovil: </td>
                                        <td><asp:Label ID="lblTipoAuto" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Cantidad de Pasajeros: </td>
                                        <td><asp:Label ID="lblCantPasajeros" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="background-color:#33cc33">Monto  a Cancelar</td>
                                    </tr>
                                    <tr>
                                        <td>Fecha Inicio: <asp:Label ID="lblFechIn" runat="server" Text=""></asp:Label></td>
                                        <td>Fecha Devolucion: <asp:Label ID="lblFechDev" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Precio por dia: 
                                        </td>
                                        <td style="background-color:#72ed60"><asp:Label ID="lblPrecio" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Cantidad de dias: </td>
                                        <td style="background-color:#72ed60"><asp:Label ID="lblNumeroDias" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td style="background-color:#bf4437"><asp:Label ID="lblTotal" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                </table>
                            </asp:Panel>

                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSigElegirAuto" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="upPanConfirmacion">
                        <ProgressTemplate>
                            <img src="img/iconos/cargando.gif" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>



                </div>
                <div class="f1-buttons">
                    <button type="button" class="btn btn-previous">Anterior</button>
                    <button type="button" class="btn btn-next">Siguiente</button>
                </div>

            </fieldset>






            <%-- Cuarta parte --%>
             <%-- Tercera ASIGNAR FORMA DE PAGO--%>
            <fieldset style="text-align:center">
                    <div class="pymnts">
                        <div class="pymnt-itm card active">
                            <h2>Tarjeta de crédito</h2>
                            <%--<label id="msjTarjeta" style="color: red"></label>--%>
                            <div class="pymnt-cntnt">
                                <div class="card-expl">
                                    <div class="credit" style="vertical-align: central">
                                        <h4>Tarjetas de crédito        </h4>
                                        <img src="img/iconos/cards1.png" height="40" width="200" />
                                    </div>
                                </div>
                                <div class="sctn-row">
                                    <div class="sctn-col l">
                                        <label>Nombre del titular</label><asp:TextBox ID="txtNombreTitular" runat="server" Text=""  placeholder="Como aparece en la tarjeta"></asp:TextBox>
                                        <aj
                                    </div>
                                    <div class="sctn-col">
                                        <label>Número de tarjeta</label><asp:TextBox ID="txtNumTarjeta" runat="server" Text="" placeholder="Número de tarjeta" autocomplete="off"></asp:TextBox>
                                        <ajaxToolkit:MaskedEditExtender ID="maskNumTarjeta" TargetControlID="txtNumTarjeta" runat="server" MaskType="None" Mask="9999-9999-9999-9999" />
                                        <ajaxToolkit:MaskedEditValidator ID="maskValiNumTarjeta" ControlExtender="maskNumTarjeta" ValidationGroup="inforTarjeta" ControlToValidate="txtNumTarjeta" runat="server" IsValidEmpty="false"></ajaxToolkit:MaskedEditValidator>
                                        
                                    </div>
                                </div>
                                <div class="sctn-row">
                                    <div class="sctn-col l">
                                        <label>Fecha de expiración</label>
                                        <div class="sctn-col half l">
                                            <asp:TextBox ID="txtMesExp" runat="server" placeholder="MM" Text="" autocomplete="off"></asp:TextBox>
                                            <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Mask="99" MaskType="None" TargetControlID="txtMesExp" />
                                            <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlExtender="MaskedEditExtender1" ValidationGroup="inforTarjeta" ControlToValidate="txtMesExp" MaximumValue="12" IsValidEmpty="false"></ajaxToolkit:MaskedEditValidator>
                                        </div>
                                        <div class="sctn-col half l">
                                            <asp:TextBox ID="txtAnoExp" runat="server" placeholder="AA" Text=""  autocomplete="off"></asp:TextBox>
                                            <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Mask="99" MaskType="Number" TargetControlID="txtAnoExp" />
                                            <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator2" runat="server" ControlExtender="MaskedEditExtender2" ValidationGroup="inforTarjeta" ControlToValidate="txtAnoExp" MinimumValue="17" MaximumValue="99" IsValidEmpty="false"></ajaxToolkit:MaskedEditValidator>

                                        </div>
                                    </div>
                                    <div class="sctn-col cvv">
                                        <label>Código de seguridad</label>
                                        <br />
                                        <div class="sctn-col half l">
                                            <asp:TextBox ID="txtCVV" runat="server" placeholder="3 dígitos" Text=""  autocomplete="off"></asp:TextBox><asp:Image runat="server" Width="25px" Height="40px" ImageUrl="~/img/iconos/cvv.png"/>
                                            <ajaxToolkit:MaskedEditExtender ID="maskEdiVVC" runat="server" Mask="999" MaskType="None" TargetControlID="txtCVV" />
                                            <ajaxToolkit:MaskedEditValidator ID="maskValVVC" runat="server" ControlExtender="maskEdiVVC" ValidationGroup="inforTarjeta" ControlToValidate="txtAnoExp" MinimumValue="17" MaximumValue="99" IsValidEmpty="false"></ajaxToolkit:MaskedEditValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="openpay">
                                    <label id="msjTarjeta" style="color: red"></label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="f1-buttons">
                                <button type="button" class="btn btn-previous">Anterior</button>
                                <asp:Button runat="server" ID="btnFinalizar"  CssClass="btnForm btn-submit"  Text="Completar"  OnClientClick="return validarInfoPago(this.form);" OnClick="btnFinalizar_Click" CausesValidation="true" ValidationGroup="inforTarjeta"></asp:Button>
                            </div>
            </fieldset>
            
        </div>
        </asp:PlaceHolder>
    </div>




</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <script src="js/formularioPasos.js"></script>

<script src="lib/jquery.ui/JS/jquery-ui-1.9.2.custom.min.js"></script>
    <script>
        $(function () {
            $.datepicker.setDefaults($.datepicker.regional["es"]);
            var dateFormat = "dd/mm/yy",

              from = $("#ContentPlaceHolder1_txtFechaInicio")
                .datepicker({
                    defaultDate: "+1w",
                    dateFormat:"dd/mm/yy",
                    changeMonth: true,
                    changeYear: true,
                    minDate:new Date(),
                    
                })
                .on("change", function () {

                    if (esFechaValida(this.value)) {
                        to.datepicker("option", "minDate", getDate(this));
                        $("#msjFecha").text("");
                       
                    } else {
                        this.value = null;
                        $("#msjFecha").text("Las fechas deben estar con el formato dd/mm/aaaa. Ejemplo 23/04/2017");
                        $("#ContentPlaceHolder1_txtFechaInicio").text("");
                        to.datepicker("option", "minDate", getDate(this));
                    }

                }),



              to = $("#ContentPlaceHolder1_txtFechaFin").datepicker({
                  defaultDate: "+1w",
                  dateFormat: "dd/mm/yy",
                  changeMonth: true,
                  changeYear: true,
                  minDate:new Date(),
              })
              .on("change", function () {

                  if (esFechaValida(this.value)) {
                      from.datepicker("option", "maxDate", getDate(this));
                      $("#msjFecha").text("");
                  } else {
                      this.value = null;
                      $("#msjFecha").text("Las fechas deben estar con el formato dd/mm/aaaa. Ejemplo 23/04/2017");
                      $("#ContentPlaceHolder1_txtFechaFin").text("");
                      from.datepicker("option", "maxDate", getDate(this));
                  }
                  
              });

            //funcion que revisa la fecha ingresada para asignarlo al otro elemento fecha
            function getDate( element ) {
                var date;
                try {
                    date = $.datepicker.parseDate( dateFormat, element.value );
                } catch( error ) {
                    date = null;
                }
 
                return date;
            }



            function esFechaValida(dateString) {
                // revisar el patrón
                if (!/^\d{2}\/\d{2}\/\d{4}$/.test(dateString))
                    return false;

                // convertir los numeros a enteros
                var parts = dateString.split("/");
                var dia = parseInt(parts[0], 10);
                var mes = parseInt(parts[1], 10);
                var ano = parseInt(parts[2], 10);

                // Revisar los rangos de año y mes
                if ((ano < 1000) || (ano > 3000) || (mes == 0) || (mes > 12))
                    return false;

                var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

                // Ajustar para los años bisiestos
                if (ano % 400 == 0 || (ano % 100 != 0 && ano % 4 == 0))
                    monthLength[1] = 29;

                // Revisar el rango del dia
                return dia > 0 && dia <= monthLength[mes - 1];
            };

  } );
  </script>
    
    	<script type="text/javascript">
			
		 
		function establecerDias(frm) {
		    var dtElem1 = frm.elements['ContentPlaceHolder1_txtFechaInicio'];
		    var dtElem2 = frm.elements['ContentPlaceHolder1_txtFechaFin'];
		    var resultElem = frm.elements['ContentPlaceHolder1_hiCantDias'];
			 
		// Retorna si el elemento no existe
			if(!dtElem1 || !dtElem2 || !resultElem) {
		return;
			}
			 
			//tomando en cuenta que el divisor es '/'.
			var x = dtElem1.value;
			var y = dtElem2.value;
			var arr1 = x.split('/');
			var arr2 = y.split('/');
			 
		// Si hay algun error asigna el valor a 0
		if(!arr1 || !arr2 || arr1.length != 3 || arr2.length != 3) {
		resultElem.value = "0";
		return;
			}
			 
		var dt1 = new Date();
		dt1.setFullYear(arr1[2], arr1[1], arr1[0]);
		var dt2 = new Date();
		dt2.setFullYear(arr2[2], arr2[1], arr2[0]);

		resultElem.value = Math.ceil( (dt2.getTime() - dt1.getTime()) / (60 * 60 * 24 * 1000))+1;
		}


		function validarInfoPago(frm) {
		    var continuar = true;

		    var nombre = frm.elements['ContentPlaceHolder1_txtNombreTitular'];  
		    var numeroTarjeta =frm.elements['ContentPlaceHolder1_txtNumTarjeta'];  
		    var mes = frm.elements['ContentPlaceHolder1_txtMesExp']; 
		    var ano = frm.elements['ContentPlaceHolder1_txtAnoExp']; 
		    var cvv = frm.elements['ContentPlaceHolder1_txtCVV']; 
		    var cadenaErrores = "Por favor corrija para continuar:<br/>"

		    if(nombre.value==""|| nombre.value==null){
		        $(nombre).addClass('input-error');
		        continuar = false; cadenaErrores += "El nombre es requerido.<br/>";
		    }else{$(nombre).removeClass('input-error')}

		    if (numeroTarjeta.value.length<16) {
		        $(numeroTarjeta).addClass('input-error');
		        continuar = false; cadenaErrores += "El numeroTarjeta es de 16 digitos.<br/>";
		    } else { $(numeroTarjeta).removeClass('input-error') }

	
		    if (mes.value == "" || mes.value.length<2) {
		        $(mes).addClass('input-error');
		        continuar = false; cadenaErrores += "El mes es requerido, digitelo como aparece en la Tarjeta. Ejemplo 01.<br/>";
		    } else {
		        var mesint = parseInt(mes.value);
		        if (mesint > 12) {
		            $(mes).addClass('input-error');
		            continuar = false; cadenaErrores += "El mes debe ser un numero menor o igual a 12.<br/>";
		        } else {
		            $(mes).removeClass('input-error')
		        }
		    }

		    if (ano.value == "" || ano.value.length<2) {
		        $(ano).addClass('input-error');
		        continuar = false; cadenaErrores += "El año es requerido,digitelo como aparece en la Tarjeta. Ejemplo 01.<br/>";
		    } else {
		        var anoint = parseInt(ano.value);
		        var fecha = new Date();
		        var stringFecha = fecha.getFullYear().toString();
		        var anoComparacion = parseInt(stringFecha.substring(2,4));
		        if (anoint < anoComparacion) {
		            $(ano).addClass('input-error');
		            continuar = false; cadenaErrores += "El año de expiración no puede ser menor al año actual.<br/>";
		        } else {
		            $(ano).removeClass('input-error');
		        }
		    }

		    if (cvv.value == "" || cvv.value.length<3) {
		        $(cvv).addClass('input-error');
		        continuar = false; cadenaErrores += "El codigo de seguridad es requerido.Es de 3 digitos.<br/>";
		    } else { $(cvv).removeClass('input-error') }

		    if (continuar == false) { $("#msjTarjeta").html(cadenaErrores); } else { $("#msjTarjeta").html(""); }
            
		    return continuar;
		}
		</script>


</asp:Content>
