﻿using autoEntidades;
using autoWebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace autosSensacion
{
    public partial class modiClientes : System.Web.UI.Page
    {
        public AutoWebService servicio = new AutoWebService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                datosCliente();
            }
        }
        private void datosCliente()
        {
            grvClientes.DataSource = servicio.ObtenerTodosClientes();
            grvClientes.DataBind();
        }
    

        protected void grvClientes_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grvClientes.EditIndex = -1;
            datosCliente();
        }

        protected void grvClientes_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grvClientes.EditIndex = e.NewEditIndex;
            datosCliente();
        }

        protected void grvClientes_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grvClientes_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            personaEntidad per = new personaEntidad();

            per.id = Convert.ToString(grvClientes.DataKeys[e.RowIndex].Values[0]);
            per.Nombre = ((TextBox)grvClientes.Rows[e.RowIndex].FindControl("txtNombreE")).Text;
            per.telContacto = ((TextBox)grvClientes.Rows[e.RowIndex].FindControl("txtTelefonoE")).Text;
            per.correo = ((TextBox)grvClientes.Rows[e.RowIndex].FindControl("txtCorreoE")).Text;
           
            servicio.modificarClientes(per);

            grvClientes.EditIndex = -1;
            datosCliente();

        }
    }
}