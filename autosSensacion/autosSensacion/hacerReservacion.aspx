﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPrincipal.Master" AutoEventWireup="true" CodeBehind="hacerReservacion.aspx.cs" Inherits="autosSensacion.hacerReservacion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <header class="dark">
        <h5>RESERVACIÓN</h5>
    </header>
    <div id="collapse2" class="body">
        <div class="form-group">
            <label class="control-label col-lg-4">Fecha a Entregar</label>
            <div class="col-lg-4">
                <asp:TextBox ID="txtFecha" runat="server"></asp:TextBox>

                <ajaxToolkit:MaskedEditExtender runat="server"
                    Mask="99/99/9999"
                    MaskType="Date"
                    CultureDatePlaceholder="" CultureTimePlaceholder=""
                    CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                    CultureDateFormat="" CultureCurrencySymbolPlaceholder="" CultureAMPMPlaceholder=""
                    Century="2000" BehaviorID="txtFecha_MaskedEditExtender" TargetControlID="txtFecha"
                    ID="txtFecha_MaskedEditExtender"></ajaxToolkit:MaskedEditExtender>

                <ajaxToolkit:CalendarExtender runat="server" BehaviorID="txtFecha_CalendarExtender"
                    TargetControlID="txtFecha" ID="txtFecha_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="ibFecha"></ajaxToolkit:CalendarExtender>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-4">Fecha de Devolución</label>
            <div class="col-lg-4">
                <asp:TextBox ID="txtFechaDev" runat="server"></asp:TextBox>

                <ajaxToolkit:MaskedEditExtender runat="server"
                    Mask="99/99/9999"
                    MaskType="Date"
                    CultureDatePlaceholder="" CultureTimePlaceholder=""
                    CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                    CultureDateFormat="" CultureCurrencySymbolPlaceholder="" CultureAMPMPlaceholder=""
                    Century="2000" BehaviorID="txtFechaDev_MaskedEditExtender" TargetControlID="txtFechaDev"
                    ID="MaskedEditExtender1"></ajaxToolkit:MaskedEditExtender>

                <ajaxToolkit:CalendarExtender runat="server" BehaviorID="txtFechaDev_CalendarExtender"
                    TargetControlID="txtFechaDev" ID="CalendarExtender1" Format="dd/MM/yyyy" PopupButtonID="ibFecha"></ajaxToolkit:CalendarExtender>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-4">Selección del Automóvil</label>
            <div class="col-lg-4">
                <asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-4">Total</label>
            <div class=" col-lg-4">
                <asp:TextBox ID="txtTotal" ReadOnly="true" runat="server"></asp:TextBox>
            </div>
        </div>

        <div class="form-actions no-margin-bottom">
            <asp:Button ID="btnReservar" class="btn btn-primary" runat="server" Text="Reservar" />
        </div>

    </div>
</asp:Content>
