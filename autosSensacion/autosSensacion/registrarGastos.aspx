﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterAdministrador.Master" AutoEventWireup="true" CodeBehind="registrarGastos.aspx.cs" Inherits="autosSensacion.registrarGastos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <header class="dark">
        <h5>REGISTRAR GASTOS</h5>
    </header>
    <div id="collapse2" class="body">
        <div class="form-group">

            <label class="control-label col-lg-4">Descripción</label>
            <div class="col-lg-4">
                <asp:TextBox ID="txtDescripcion" runat="server"></asp:TextBox>
                 <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="#cc0000" runat="server" ErrorMessage="* Digite la descripción" ControlToValidate="txtDescripcion"></asp:RequiredFieldValidator>
            </div>
        </div>

          <div class="form-group">

            <label class="control-label col-lg-4">Monto</label>
            <div class="col-lg-4">
                <asp:TextBox ID="txtMonto" runat="server"></asp:TextBox>
                 <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ForeColor="#cc0000" runat="server" ErrorMessage="* Digite el monto" ControlToValidate="txtMonto"></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-4">Fecha del Gasto</label>
            <div class=" col-lg-4">
                <asp:TextBox ID="txtFecha" runat="server"></asp:TextBox>
                 <ajaxToolkit:CalendarExtender Format="dd/MM/yyyy"
                    BehaviorID="txtFecha_CalendarExtender" TargetControlID="txtFecha" ID="txtFecha_CalendarExtender" runat="server" />
                <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ForeColor="#cc0000" runat="server" ErrorMessage="* Seleccione la Fecha" ControlToValidate="txtFecha"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-4">Empleado</label>
            <div class=" col-lg-4">
                <asp:DropDownList ID="ddlEmpleado" runat="server" AppendDataBoundItems="true">
                    <asp:ListItem Value="0" >--Seleccione el Empleado--</asp:ListItem>
                </asp:DropDownList>
                <br />
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="#cc0000" runat="server" InitialValue="0"
                  ErrorMessage="* Seleccione el Empleado" ControlToValidate="ddlEmpleado"></asp:RequiredFieldValidator>

               </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-4">Tipo Gasto</label>
            <div class=" col-lg-4">
                <asp:DropDownList ID="ddlTipoGasto" runat="server" AppendDataBoundItems="true">
                    <asp:ListItem Value="0">--Seleccione el Tipo--</asp:ListItem>
                </asp:DropDownList>
                <br />
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ForeColor="#cc0000" runat="server" InitialValue="0"
                  ErrorMessage="* Seleccione el Tipo de Gasto" ControlToValidate="ddlTipoGasto"></asp:RequiredFieldValidator>
            </div>
        </div>

         <div class="form-group">
            <label class="control-label col-lg-4"></label>
            <div class=" col-lg-4">
                <asp:Label ID="lblMsj" runat="server" Text="" ForeColor="#cc0000" ></asp:Label>
            </div>
        </div>
        <div class="form-actions no-margin-bottom">
            <asp:Button ID="btnGuardar" class="btn btn-primary" runat="server" Text="Guardar" OnClick="btnGuardar_Click" />
        </div>

    </div>

</asp:Content>
