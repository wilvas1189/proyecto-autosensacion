﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterAdministrador.Master" AutoEventWireup="true" CodeBehind="mantTipoGastos.aspx.cs" Inherits="autosSensacion.mantTipoGastos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="dark">
        <h5>MANTENIMIENTO TIPOS DE GASTOS</h5>
    </header>
    <div id="collapse2" class="body">
        <div class="form-group">

            <label class="control-label col-lg-4">Descripción</label>
            <div class="col-lg-4">
                <asp:TextBox ID="txtDescripcion" runat="server"></asp:TextBox>
                 <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ForeColor="#cc0000" runat="server" ErrorMessage="*Digite la descripción" ControlToValidate="txtDescripcion"></asp:RequiredFieldValidator> 
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-4"></label>
            <div class=" col-lg-4">
                <asp:Label ID="lblMsj" runat="server" Text="" ForeColor="#cc0000" ></asp:Label>
            </div>
        </div>
        <div class="form-actions no-margin-bottom">
            <asp:Button ID="btnGuardar" class="btn btn-primary" runat="server" Text="Guardar" OnClick="btnGuardar_Click" />
        </div>

    </div>
</asp:Content>
