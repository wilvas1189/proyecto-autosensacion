﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPrincipal.Master" AutoEventWireup="true" CodeBehind="reservacionesTodas.aspx.cs" Inherits="autosSensacion.reservacionesTodas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


        <header class="dark">
        <h5>Todas Mis Reservaciones</h5>
    </header>
            <div id="collapse2" class="body">



                                <asp:GridView ID="grvReservaciones" runat="server" DataKeyNames="id"
                                        CssClass="table table-striped responsive-table"
                                        GridLines="None"
                                        AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundField DataField="id" HeaderText="Id" ReadOnly="true"></asp:BoundField>
                                            <asp:BoundField DataField="fechaReservacion" HeaderText="Fecha" ReadOnly="true"></asp:BoundField>
                                            <asp:BoundField DataField="fechaEntrega" HeaderText="Entrega" ReadOnly="true"></asp:BoundField>
                                            <asp:BoundField DataField="fechaDevolucion" HeaderText="Devolucion" ReadOnly="true"></asp:BoundField>
                                            <asp:BoundField DataField="descripcion" HeaderText="Descripcion" ReadOnly="true"></asp:BoundField>
                                            <asp:BoundField DataField="totalReservacion" HeaderText="Total" ReadOnly="true"></asp:BoundField>
                                            
                                        </Columns>
                                    </asp:GridView>


            </div>
    </asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

</asp:Content>
