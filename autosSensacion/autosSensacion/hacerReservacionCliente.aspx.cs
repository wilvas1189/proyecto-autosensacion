﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using autoWebServices;
using autoEntidades;



namespace autosSensacion
{
    public partial class hacerReservacionCliente : System.Web.UI.Page
    {
        public AutoWebService servicio = new AutoWebService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<tipoAutoEntidad> tipos = new List<tipoAutoEntidad>();
                List<tipoAutoEntidad> datosServicio = new List<tipoAutoEntidad>();
                datosServicio = servicio.ObtenerTodosTipoAuto();


                tipos.Add(new tipoAutoEntidad { id=0, descripcion="Todos"});
                foreach (tipoAutoEntidad item in datosServicio )
                {
                    tipos.Add(item);
                }
                ddlTipoAutos.DataSource = tipos;
                ddlTipoAutos.DataTextField = "descripcion";
                ddlTipoAutos.DataValueField = "id";
                ddlTipoAutos.DataBind();

                Refrescar(0);
            }
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {

        }
        protected void ddlTipoAutos_SelectedIndexChanged(object sender, EventArgs e)
        {
            Refrescar(Convert.ToInt16(ddlTipoAutos.SelectedValue));
        }



        //METODO PARA CARGAR LOS AUTOS QUE SE MUESTRAN EN PANTALLA
        private void Refrescar(int idTipoAuto)
        {
            try { 
            rblListaAutos.Items.Clear();
            List<automovilEntidad> listaAutos= new List<automovilEntidad>();

            if (idTipoAuto != 0)
            {
                listaAutos = servicio.SeleccionarPotTipoAutoReservar(idTipoAuto);
            }
            else {
                listaAutos = servicio.SeleccionarTodosAutosReservar();
            }

            int contador = 0;

            foreach (automovilEntidad item in listaAutos) {

                ListItem carro = new ListItem("<label for='ContentPlaceHolder1_rblListaAutos_"+contador+"' > Marca: " + item.descripcion +
                    "<br/>Modelo: " + item.modelo +
                    "<br/>Cantidad Pasajeros: " + item.numPasajeros +
                    " <br/></label> <img src='manejadorImgFlota.ashx?placa="+item.placa+"' width='350px' height='200px' alt='autoMovil'/>", item.placa);
                rblListaAutos.Items.Add(carro);
                if (contador == 0) { carro.Selected = true; }

                contador += 1;
            }
            rblListaAutos.CellPadding = 60;
            rblListaAutos.CellSpacing = 60;
            rblListaAutos.CssClass = "table";

        }
            catch (Exception error) {

            }
}

        //METODO DEL BOTON QUE ACCIONA HACER EL RESUMEN DE LA RESERVACION
        protected void btnSigElegirAuto_Click(object sender, EventArgs e)
        {

            try { 

            personaEntidad cliente = servicio.SeleccionarPersonaXId(Session["idUsuario"].ToString()).Find(a=> a.id== Session["idUsuario"].ToString());
            List<automovilEntidad> listaAutos = servicio.SeleccionarAutoXId(rblListaAutos.SelectedItem.Value);
            tipoAutoEntidad tipoAuto;
            automovilEntidad auto;
            if (listaAutos.Count > 0) {
                auto = listaAutos.Find(a=> a.placa == rblListaAutos.SelectedItem.Value);
                tipoAuto = servicio.SeleccionarTipoAutoPorid(auto.idTipoAuto).Find(a=> a.id==auto.idTipoAuto);

                lblIdCliente.Text = cliente.id;
                lblNombreCliente.Text = cliente.Nombre + " " + cliente.Apellido1 + " " + cliente.Apellido2;
                lblMarca.Text = auto.descripcion;
                lblModelo.Text = auto.modelo;
                lblTipoAuto.Text = auto.tipoAuto;
                lblCantPasajeros.Text = auto.numPasajeros.ToString() ;
                lblPrecio.Text = String.Format("{0:000.00}",tipoAuto.precio);
                lblNumeroDias.Text = hiCantDias.Value;
                int dias = Convert.ToInt16(hiCantDias.Value);
                lblTotal.Text = String.Format("{0:000.00}",(tipoAuto.precio * dias)) ;
                this.lblFechIn.Text = txtFechaInicio.Text;
                this.lblFechDev.Text = txtFechaFin.Text;
            }
            }
            catch (Exception error)
            {

            }
        }

        //BOTON QUE FORMALIZA Y EJECUTA EL COBRO 
        protected void btnFinalizar_Click(object sender, EventArgs e)
        {

            try
            {

                //se crea el encabezado
                encabezadoFacturaEntidad factura = new encabezadoFacturaEntidad();
               
            factura.id = servicio.ObtnerSigNumeroFactura();
            factura.idCliente = Session["idUsuario"].ToString();
            factura.idEmpleado = "WEB";
            factura.idNumTarjeta = Convert.ToDecimal(txtNumTarjeta.Text);
            factura.tarjeta.numTarjeta= Convert.ToDecimal(txtNumTarjeta.Text);
            factura.tarjeta.nombreTitular = txtNombreTitular.Text;
            factura.tarjeta.mes = Convert.ToInt32(txtMesExp.Text);
            factura.tarjeta.ano = Convert.ToInt32(txtAnoExp.Text);
            factura.tarjeta.cvv = Convert.ToInt32(txtCVV.Text);
            factura.detallesFactura = new List<detalleFacturaEntidad>();
            factura.fecha = DateTime.Now;
                 
                //se crean las lineas de detalle
                
                foreach (ListItem elemento in rblListaAutos.Items) {
                    if (elemento.Selected)
                    {
                        List<automovilEntidad> listaAutos = servicio.SeleccionarAutoXId(rblListaAutos.SelectedItem.Value);
                        tipoAutoEntidad tipoAuto;
                        automovilEntidad auto;
                        auto = listaAutos.Find(a => a.placa == elemento.Value);
                       tipoAuto = servicio.SeleccionarTipoAutoPorid(auto.idTipoAuto).Find(a => a.id == auto.idTipoAuto);

                        int dias = Convert.ToInt16(hiCantDias.Value);

                        detalleFacturaEntidad detalle = new detalleFacturaEntidad();
                        detalle.id = 0;
                        detalle.idAutomovil = elemento.Value;
                        detalle.idEncabezadoFactura = 0;
                        detalle.cantidadDias = dias;
                        detalle.precio = tipoAuto.precio * dias;
                    factura.detallesFactura.Add(detalle);
                        }
                }



                //se crea la reservacion
                reservacionEntidad reservacion = new reservacionEntidad();
                reservacion.id = 0;
                reservacion.idCliente = Session["idUsuario"].ToString();
                reservacion.fechaReservacion = DateTime.Now;
                string[] entrega = txtFechaInicio.Text.ToString().Split('/');
                reservacion.fechaEntrega = new DateTime(Convert.ToInt16(entrega[2]), Convert.ToInt16(entrega[1]), Convert.ToInt16(entrega[0]));
                string[] devolucion = txtFechaFin.Text.ToString().Split('/');
                reservacion.fechaDevolucion = new DateTime(Convert.ToInt16(devolucion[2]), Convert.ToInt16(devolucion[1]), Convert.ToInt16(devolucion[0]));
                reservacion.idAuto = rblListaAutos.SelectedItem.Value;
                reservacion.totalReservacion = lblTotal.Text.Equals("") ? 0 : Convert.ToDecimal(lblTotal.Text);
                reservacion.cancelada = false;
                reservacion.vencida = false;
                reservacion.idFactura = factura.id;





                //guardar facturacion
                servicio.insertarFactura(factura, factura.tarjeta);
                //guardar reservacion
                servicio.insertarReservacion(reservacion);


                Label lblResultado = new Label();
            lblResultado.ForeColor = System.Drawing.Color.Green;
            lblResultado.Text = "Reservacion completada.";


            HyperLink link = new HyperLink();
            link.NavigateUrl = "principalCliente.aspx";
            link.Text = "Volver a la pantalla principal";


            LiteralControl brSaltoLinea = new LiteralControl();
            brSaltoLinea.Text = "<br/>";

            Image miImagen = new Image();
            miImagen.ImageUrl = "~/img/iconos/correcto.jpg";
            miImagen.Width = 25;
            miImagen.Height = 25;

            PlaceHolder1.Controls.Clear();
            PlaceHolder1.Controls.Add(miImagen);
            PlaceHolder1.Controls.Add(lblResultado);
            PlaceHolder1.Controls.Add(brSaltoLinea);
            PlaceHolder1.Controls.Add(link);





        }
            catch (Exception error)
            {


                //SI OCURRE UN ERROR SE LIMPIA LA PANTALLA Y APARECE UN MENSAJE INFORMANDO QUE EL PROCESO FALLO
                //Y SE AGREGAN DOS LINKS UNO PARA INTENTARLO NUEVAMENTE Y OTRO PARA VOLVER A LA PANTALLA PRINCIPAL
        Label lblResultado = new Label();
        lblResultado.ForeColor = System.Drawing.Color.Red;
                lblResultado.Text = "Ha ocurrido un error al enviar el formulario. Intentelo nuevamente.";

                Image miImagen = new Image();
        miImagen.ImageUrl = "~/img/iconos/incorrecto.jpg";
                miImagen.Width = 25;
                miImagen.Height = 25;

                LiteralControl brSaltoLinea = new LiteralControl();
        brSaltoLinea.Text = "<br/>";

                LiteralControl brSaltoLinea2 = new LiteralControl();
        brSaltoLinea2.Text = "<br/>";

                HyperLink link = new HyperLink();
        link.NavigateUrl = "hacerReservacionCliente.aspx";
                link.Text = "Intentarlo Nuevamente";

                HyperLink link2 = new HyperLink();
        link.NavigateUrl = "principalCliente.aspx";
                link.Text = "Volver a la pantalla principal";

                PlaceHolder1.Controls.Clear();
                PlaceHolder1.Controls.Add(miImagen);
                PlaceHolder1.Controls.Add(lblResultado);
                PlaceHolder1.Controls.Add(brSaltoLinea);
                PlaceHolder1.Controls.Add(link);
                PlaceHolder1.Controls.Add(brSaltoLinea2);
                PlaceHolder1.Controls.Add(link2);


            }
}
    }
}