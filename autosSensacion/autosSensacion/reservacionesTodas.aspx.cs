﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using autoWebServices;

namespace autosSensacion
{
    
    public partial class reservacionesTodas : System.Web.UI.Page
    {

        AutoWebService servicio = new AutoWebService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["idUsuario"] != null)
            {
                grvReservaciones.DataSource = servicio.SeleccionarReservacionesPorCliente(Session["idUsuario"].ToString());
                grvReservaciones.DataBind();
            }
        }
    }
}