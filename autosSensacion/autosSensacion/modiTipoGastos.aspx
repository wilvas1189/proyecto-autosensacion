﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterAdministrador.Master" AutoEventWireup="true" CodeBehind="modiTipoGastos.aspx.cs" Inherits="autosSensacion.modiTipoGastos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <header>
        <h5>DATOS REGISTRADOS TIPO DE GASTOS</h5>
    </header>
    <div id="collapse2" class="body">
        <div class="form-group">

            <div id="stripedTable" class="body collapse in">
                <table>
                    <tr>
                        <td>
                            <asp:GridView ID="grvTipoGastos" runat="server" DataKeyNames="id" 
                                CssClass="table table-striped responsive-table"
                                GridLines="None"
                                AutoGenerateColumns="false"
                                AutoGenerateEditButton="true"
                                OnRowCancelingEdit="grvTipoGastos_RowCancelingEdit"
                                OnRowEditing="grvTipoGastos_RowEditing"
                                OnRowUpdating="grvTipoGastos_RowUpdating">

                                <Columns>
                                    <asp:TemplateField HeaderText="Descripci&#243;n">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescripcionE" runat="server" Text='<%# Eval("descripcion") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtDescripcionE" runat="server" Text='<%# Bind("descripcion") %>'></asp:TextBox>
                                            <br />
                                            <asp:RequiredFieldValidator ForeColor="red" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Debe digitar la descripción" ControlToValidate="txtDescripcionE"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                            </asp:GridView>

                        </td>

                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
