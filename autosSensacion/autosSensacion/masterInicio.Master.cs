﻿using autoEntidades;
using autoWebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace autosSensacion
{
    public partial class masterInicio : System.Web.UI.MasterPage
    {
        public AutoWebService servicio = new AutoWebService();
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Clear();
        }


        protected void btnRegistrar_Click(object sender, EventArgs e)
        {

            try {
                personaEntidad persona = new personaEntidad();
                persona = servicio.ObtenerPersona(txtCedula.Text);
                clienteEntidad cliente = new clienteEntidad();
                if (persona.id != null)
                {
                    lblMensjaje.Text = "* La persona ya se encuentra registrada";
                }
                else
                {
                    if (txtPassword.Text == txtConfirmacion.Text)
                    {
                        persona.id = txtCedula.Text;
                        persona.Nombre = txtNombre.Text;
                        persona.Apellido1 = txtAp1.Text;
                        persona.Apellido2 = txtAp2.Text;
                        persona.fechaNacimiento = Convert.ToDateTime(txtFecha.Text);
                        persona.telContacto = txtTelefono.Text;
                        persona.correo = txtEmail.Text;

                        cliente.idPersona = txtCedula.Text;
                        cliente.fechaIngreso = DateTime.Today;
                        cliente.contrasenna = txtPassword.Text;
                        cliente.idRolUsuario = 1;

                        servicio.insertarPersona(persona);
                        servicio.insertarCliente(cliente);

                        Session.Add("idUsuario", persona.id);
                        Session.Add("nombre",( persona.Nombre+" "+persona.Apellido1+" "+persona.Apellido2));
                        Response.Redirect("principalCliente.aspx");
                    }
                    else
                    {
                        lblMensjaje.Text = "* La contraseña no coincide";
                    }
                }
            }
            catch (Exception error) {

            }
            
        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                personaEntidad per = new personaEntidad();
                empleadoEntidad emp = new empleadoEntidad();

                per = servicio.ObtenerCliente(txtUsuario.Text, txtcontrasena.Text);
                emp = servicio.ObtenerEmpleado(txtUsuario.Text, txtcontrasena.Text);
                if (per.id != null)
                {
                    Session.Add("idUsuario", per.id);
                    Session.Add("nombre", per.Nombre);
                    Response.Redirect("principalCliente.aspx");

                }
                else
                {
                    if (emp.idPersona != null && emp.estaActivo == true && emp.idRolUsuario == 3)
                    {
                        Session.Add("idUsuario", emp.idPersona);
                        Session.Add("nombre", emp.Nombre);
                        Session.Add("rol", emp.idRolUsuario);
                        Response.Redirect("principalAdministrador.aspx");
                    }
                    else
                    {
                        if (emp.idPersona != null && emp.estaActivo == true && emp.idRolUsuario == 1)
                        {
                            Session.Add("idUsuario", emp.idPersona);
                            Session.Add("nombre", emp.Nombre);
                            Session.Add("rol", emp.idRolUsuario);
                            Response.Redirect("principalEmpleado.aspx");
                        }
                        else
                        {
                            lblMensjaje.Text = "Usuario o contraseña no validos";
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}