﻿using autoEntidades;
using autoWebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace autosSensacion
{
    public partial class reporteGastos : System.Web.UI.Page
    {
        public AutoWebService servicio = new AutoWebService();

        protected void Page_PreInit(object sender, EventArgs e)
        {

            if (Session["rol"] != null)
            {
                int rol = Convert.ToInt16(Session["rol"].ToString());

                if (rol == 3)
                {
                    this.MasterPageFile = "~/masterAdministrador.Master";
                }
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                List<tipoGastoEntidad> tiposGastos = new List<tipoGastoEntidad>();
                tiposGastos = servicio.ObtenerTodosTipoGasto();
                ddlGastos.DataSource = tiposGastos;
                ddlGastos.DataTextField = "descripcion";
                ddlGastos.DataValueField = "id";
                ddlGastos.DataBind();
                
            }
        }

        protected void btnMostrar_Click(object sender, EventArgs e)
        {
            ReportViewer1.LocalReport.Refresh();
        }
    }
}