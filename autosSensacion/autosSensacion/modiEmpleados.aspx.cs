﻿using autoEntidades;
using autoWebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace autosSensacion
{
    public partial class modiEmpleados : System.Web.UI.Page
    {
        public AutoWebService servicio = new AutoWebService();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                datosEmpleado();
            }
        }
        private void datosEmpleado()
        {
            grvEmpleados.DataSource = servicio.ObtenerTodosEmpleados();
            grvEmpleados.DataBind();
        }
        protected void grvEmpleados_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grvEmpleados.EditIndex = -1;
            datosEmpleado();
        }

      

        protected void grvEmpleados_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grvEmpleados.EditIndex = e.NewEditIndex;
            datosEmpleado();
        }

        protected void grvEmpleados_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
               
                DropDownList ddlEstado = (e.Row.FindControl("ddlEstaActivoE") as DropDownList);
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {

                    string idE = (DataBinder.Eval(e.Row.DataItem, "estaActivo")).ToString();
                    if (idE == "No")
                    {
                        ddlEstado.SelectedValue = "2";
                    }
                    else
                    {
                        ddlEstado.SelectedValue = "1";
                    }
                }
            }
        }

        protected void grvEmpleados_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            personaEntidad per = new personaEntidad();
            empleadoEntidad emp = new empleadoEntidad();

            per.id = Convert.ToString(grvEmpleados.DataKeys[e.RowIndex].Values[0]);
            per.Nombre = ((TextBox)grvEmpleados.Rows[e.RowIndex].FindControl("txtNombreE")).Text;
            per.telContacto = ((TextBox)grvEmpleados.Rows[e.RowIndex].FindControl("txtTelefonoE")).Text;
            per.correo = ((TextBox)grvEmpleados.Rows[e.RowIndex].FindControl("txtCorreoE")).Text;

            int valor = Convert.ToInt16(((DropDownList)grvEmpleados.Rows[e.RowIndex].FindControl("ddlEstaActivoE")).SelectedValue);
            if (valor == 1)
            {
                emp.estaActivo = true;
            }
            else
            {
                emp.estaActivo = false;
            }

            servicio.modificarEmpleados(per,emp);

            grvEmpleados.EditIndex = -1;
            datosEmpleado();


        }
    }
}