﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterAdministrador.Master" AutoEventWireup="true" CodeBehind="modiEmpleados.aspx.cs" Inherits="autosSensacion.modiEmpleados" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <header>
        <h5>DATOS REGISTRADOS EMPLEADOS</h5>
    </header>
    <div id="collapse1" class="body">
        <div class="form-group">
            <div id="stripedTable" class="body collapse in">
                <table>
                    <tr>
                        <td>
                            <asp:GridView ID="grvEmpleados" runat="server" DataKeyNames="id" 
                                CssClass="table table-striped responsive-table"
                                GridLines="None"
                                AutoGenerateColumns="false"
                                AutoGenerateEditButton="true"
                                OnRowCancelingEdit="grvEmpleados_RowCancelingEdit"
                                OnRowEditing="grvEmpleados_RowEditing"
                                OnRowDataBound="grvEmpleados_RowDataBound"
                                OnRowUpdating="grvEmpleados_RowUpdating">
                                <Columns>
                                    <asp:BoundField DataField="id" HeaderText="Identificaci&#243;n" ReadOnly="true"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Nombre">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNombreE" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtNombreE" runat="server" Text='<%# Bind("Nombre") %>' Width="70px"></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>  
                                    <asp:TemplateField HeaderText="Telefono">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTelefonoE" runat="server" Text='<%# Eval("telContacto") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtTelefonoE" runat="server" Text='<%# Bind("telContacto") %>' Width="60px"></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Correo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCorreoE" runat="server" Text='<%# Eval("correo") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                           <asp:TextBox ID="txtCorreoE" runat="server" Text='<%# Bind("correo") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Activo">
                                         <ItemTemplate>
                                            <asp:Label ID="lblEstaActivoE" runat="server" Text='<%# Eval("estaActivo") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlEstaActivoE" runat="server">
                                                <asp:ListItem Value="1"> Sí</asp:ListItem>
                                                <asp:ListItem Value="2"> No </asp:ListItem>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>

                    </tr>
                </table>
            </div>
        </div>
    </div>


</asp:Content>
