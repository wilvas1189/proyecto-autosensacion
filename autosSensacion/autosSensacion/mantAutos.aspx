﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterAdministrador.Master" AutoEventWireup="true" CodeBehind="mantAutos.aspx.cs" Inherits="autosSensacion.mantAutos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="dark">
        <h5>MANTENIMIENTO AUTOS</h5>
    </header>
    <div id="collapse2" class="body">
        <div class="form-group">

            <label class="control-label col-lg-4">Número Placa</label>
            <div class="col-lg-4">
                <asp:TextBox ID="txtPlaca" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="#cc0000" runat="server" ErrorMessage="* Digite la placa" ControlToValidate="txtPlaca"></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-4">Fotografia</label>
            <div class=" col-lg-4">
                <asp:FileUpload ID="fileFoto" runat="server" ToolTip="Fotografia" />
                <asp:RegularExpressionValidator ID="REGEXFileUploadLogo" runat="server" ErrorMessage="Solo Imagenes jpg o png" ControlToValidate="fileFoto" ValidationExpression= "(.*).(.jpg|.JPG|.jpeg|.JPEG|.png|.PNG)$" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                    ControlToValidate="fileFoto" ErrorMessage="La fotografia es requerida!" ForeColor="#cc0000" > </asp:RequiredFieldValidator>
                <br />
            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-lg-4">VIN</label>
            <div class=" col-lg-4">
                <asp:TextBox ID="txtVin" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ForeColor="#cc0000"  runat="server" ErrorMessage="* Digite el VIN" ControlToValidate="txtVin"></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-4">Marca</label>
            <div class=" col-lg-4">
                <asp:DropDownList ID="ddlMarca" runat="server" AppendDataBoundItems="true">
                    <asp:ListItem >--Seleccione la Marca--</asp:ListItem>
                </asp:DropDownList>
               </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-4">Modelo</label>
            <div class=" col-lg-4">
                <asp:TextBox ID="txtModelo" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ForeColor="#cc0000" runat="server" ErrorMessage="*Digite el modelo" ControlToValidate="txtModelo"></asp:RequiredFieldValidator>
            </div>
            </div>
        <div class="form-group">
            <label class="control-label col-lg-4">Cilindraje</label>
            <div class=" col-lg-4">
                <asp:TextBox ID="txtCilindraje" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ForeColor="#cc0000" runat="server" ErrorMessage="*Digite el cilindraje" ControlToValidate="txtCilindraje"></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-4">Número Pasajeros</label>
            <div class=" col-lg-4">
                <asp:TextBox ID="txtCantPasajeros" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ForeColor="#cc0000" runat="server" ErrorMessage="*Digite la cantidad de pasajeros" ControlToValidate="txtCantPasajeros"></asp:RequiredFieldValidator> 
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-4">Tipo Auto</label>
            <div class=" col-lg-4">
                <asp:DropDownList ID="ddlTipoAuto" runat="server" AppendDataBoundItems="true">
                    <asp:ListItem Value="0">--Seleccione el Tipo--</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="requireTipo" runat="server" ControlToValidate="ddlTipoAuto" InitialValue="0"  ForeColor="#cc0000" ErrorMessage="Seleccione el tipo de Automovil"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-4">Está Activo</label>
            <div class=" col-lg-4">
                <asp:RadioButton ID="rdbSi" Text="SI" GroupName="Activo" runat="server" />

                <asp:RadioButton ID="rdbNo" Text="NO" GroupName="Activo" runat="server" />
            </div>
        </div>
         <div class="form-group">
            <label class="control-label col-lg-4"></label>
            <div class=" col-lg-4">
                <asp:Label ID="lblMsj" runat="server" Text="" ForeColor="#cc0000" ></asp:Label>
            </div>
        </div>
        <div class="form-actions no-margin-bottom" style="float:right">
            <asp:Button ID="btnGuardar" class="btn btn-primary" BackColor="#33cc33" BorderColor="#33cc33" runat="server" Text="Guardar" OnClick="btnGuardar_Click" />
        </div>

    </div>

  
</asp:Content>
