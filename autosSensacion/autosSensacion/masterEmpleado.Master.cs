﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace autosSensacion
{
    public partial class masterEmpleado : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime fecha = DateTime.Now;
            lblFecha.Text = fecha.ToShortDateString();

            if (Session["idUsuario"] != null)
            {
                lblUsuario.Text = Session["nombre"].ToString();

            }
            else
            {
                Response.Redirect("default.aspx");
            }
        }
    }
}