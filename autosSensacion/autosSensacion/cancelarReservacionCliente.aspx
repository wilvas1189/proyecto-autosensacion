﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPrincipal.Master" AutoEventWireup="true" CodeBehind="cancelarReservacionCliente.aspx.cs" Inherits="autosSensacion.cancelarReservacionCliente" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">





</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    


         <header>
        <h5>Cancelar Reservaciones</h5>
    </header>
    <div id="collapse1" class="body" style="text-align:center">
        <div class="form-group">
            <div id="stripedTable" class="body collapse in">
                <table>
                    <tr>
                        <td>
                            <asp:GridView ID="grvCancelarReservacion" runat="server" DataKeyNames="id" 
                                CssClass="table table-striped responsive-table"
                                GridLines="None"
                                AutoGenerateColumns="false"
                                OnRowCancelingEdit="grvCancelarReservacion_RowCancelingEdit"
                                OnRowEditing="grvCancelarReservacion_RowEditing"
                                AllowCustomPaging="true"
                               
                                OnRowDataBound="grvCancelarReservacion_RowDataBound"
                                OnRowUpdating="grvCancelarReservacion_RowUpdating" Font-Size="Small" HeaderStyle-BackColor="#33CC33" PagerSettings-NextPageText="SIG" PagerSettings-PreviousPageText="ANT" AllowPaging="True" HorizontalAlign="Center">
                                <Columns>
                                    <asp:commandfield showeditbutton="true"
                                    edittext="Cancelar Reservación"
                                    canceltext="Cancelar"
                                    updatetext="Guardar"
                                    headertext=""/>
                                    <asp:TemplateField HeaderText="Cancelada">
                                         <ItemTemplate>
                                            <asp:Label ID="lblEstaActivoE" runat="server" Text='<%# Eval("stringCancelada") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddCanceladaE" runat="server">
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="id" HeaderText="Id" ReadOnly="true"></asp:BoundField>
                                    <asp:BoundField DataField="fechaReservacion" HeaderText="Fecha de Reservación" ReadOnly="true" ></asp:BoundField>
                                    <asp:BoundField DataField="fechaEntrega" HeaderText="Fecha de Entrega" ReadOnly="true" ></asp:BoundField>
                                    <asp:BoundField DataField="fechaDevolucion" HeaderText="Fecha de Devolución" ReadOnly="true" ></asp:BoundField>
                                    <asp:BoundField DataField="descripcion" HeaderText="Descripcion" ReadOnly="true" ></asp:BoundField>
                                    <asp:BoundField DataField="totalReservacion" HeaderText="Total" ReadOnly="true" ></asp:BoundField>

                                </Columns>
<HeaderStyle BackColor="#33CC33"></HeaderStyle>
                                <RowStyle Font-Size="Small" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="btn" style="background-color:#33CC33;"><a href="principalCliente.aspx" style="color:white">Volver a Inicio</a></div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">




</asp:Content>
