﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPrincipal.Master" AutoEventWireup="true" CodeBehind="principalCliente.aspx.cs" Inherits="autosSensacion.principalCliente" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="carousel-inner">   
     <div class="item active">
          <img src="img/carousel/autoN.png" alt="" style="width:100%;"/>
          <div class="container">
            <div class="carousel-caption">
              <h1>¡Bienvenidos a Autos Sensación!</h1>
              <p class="lead" >
                  Autos Sensacion es la compañía Alajuelense dedicada alquiler de autos en Costa Rica sin costos ocultos, con protección total, y seguro con cero deducible. Con la flota joven de vehiculos que no alcanzan 24 meses de antigüedad.
			   </p>
             
            </div>
          </div>
        </div>
         </div>
</asp:Content>
