﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterAdministrador.Master" AutoEventWireup="true" CodeBehind="modiAutos.aspx.cs" Inherits="autosSensacion.modiAutos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <header>
        <h5>DATOS REGISTRADOS AUTOS</h5>
    </header>
    <div id="collapse1" class="body">
        <div class="form-group">
            <div id="stripedTable" class="body collapse in">
                <table>
                    <tr>
                        <td>
                            <asp:GridView ID="grvAutos" runat="server" DataKeyNames="placa"
                                CssClass="table table-striped responsive-table"
                                GridLines="None"
                                AutoGenerateColumns="false"
                                AutoGenerateEditButton="true"
                                OnRowCancelingEdit="grvAutos_RowCancelingEdit"
                                OnRowEditing="grvAutos_RowEditing"
                                OnRowDataBound="grvAutos_RowDataBound"
                                OnRowUpdating="grvAutos_RowUpdating">
                                <Columns>
                                    <asp:BoundField DataField="Placa" HeaderText="Placa" ReadOnly="true"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Marca">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMarcaE" runat="server" Text='<%# Eval("descripcion") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlMarcaE" runat="server"></asp:DropDownList>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Modelo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblModeloE" runat="server" Text='<%# Eval("modelo") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtModeloE" runat="server" Text='<%# Bind("modelo") %>' Width="75px"></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cilindraje">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCilindrajeE" runat="server" Text='<%# Eval("cilindraje") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtCilindrajeE" runat="server" Text='<%# Bind("cilindraje") %>' Width="65px"></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Espacios">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPasajerosE" runat="server" Text='<%# Eval("numPasajeros") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtPasajerosE" runat="server" Text='<%# Bind("numPasajeros") %>' Width="55px"></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tipo Auto">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTipoAutoE" runat="server" Text='<%# Eval("tipoAuto") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlTipoAutoE" runat="server"></asp:DropDownList>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Activo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEstaActivoE" runat="server" Text='<%# Eval("activo") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlEstaActivoE" runat="server">
                                                <asp:ListItem Value="1"> Sí</asp:ListItem>
                                                <asp:ListItem Value="2"> No </asp:ListItem>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Foto">
                                        <ItemTemplate>
                                            <asp:Image runat="server" ></asp:Image>
                                            <img alt="" src="<%# Eval("Placa","manejadorImgFlota.ashx?placa={0}") %>" width="75" height="50" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:FileUpload ID="fileFoto" runat="server" />
                                            <asp:RegularExpressionValidator ID="REGEXFileUploadLogo" runat="server" ErrorMessage="Solo Imagenes jpg" ControlToValidate="fileFoto" ValidationExpression= "(.*).(.jpg|.JPG)$" />
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                             ControlToValidate="fileFoto" ErrorMessage="La fotografia es requerida!" ForeColor="#cc0000" > </asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>

                    </tr>
                </table>
            </div>
        </div>
    </div>


</asp:Content>
