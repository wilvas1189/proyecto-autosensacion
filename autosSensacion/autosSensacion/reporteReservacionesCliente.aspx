﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterEmpleado.Master" AutoEventWireup="true" CodeBehind="reporteReservacionesCliente.aspx.cs" Inherits="autosSensacion.reporteReservacionesCliente" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    
             <header class="dark">
        <h5>REPORTES</h5>
    </header>




            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
            <div style="text-align:center">
                <asp:Label runat="server" Text="Clientes:  ">

                </asp:Label> <asp:DropDownList ID="ddlCliente" runat="server" Width="200px" AppendDataBoundItems="true">
                                <asp:ListItem Value="0">--Seleccione al Cliente--</asp:ListItem>
                             </asp:DropDownList>
                    <br/>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  ErrorMessage="Seleccione el cliente"
                     ForeColor="#cc0000" InitialValue="0"
                     ControlToValidate="ddlCliente"></asp:RequiredFieldValidator>
            </div>

            <div class="form-group">
                    <label class="control-label col-lg-4">Desde:</label>
                    <div class="col-lg-4">
                        <asp:TextBox ID="txtFechaInicio" TextMode="Date" runat="server"></asp:TextBox>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ForeColor="#cc0000" runat="server" ErrorMessage="* Seleccione la Fecha" ControlToValidate="txtFechaInicio"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-4">Hasta:</label>
                    <div class="col-lg-4">
                        <asp:TextBox ID="txtFechaFin" runat="server" TextMode="Date"></asp:TextBox>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ForeColor="#cc0000" runat="server" ErrorMessage="* Seleccione la Fecha" ControlToValidate="txtFechaFin"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-4">
                        <asp:Button ID="btnMostrar" runat="server" Text="Mostrar" OnClick="btnMostrar_Click" />
                    </div>
                </div>



                    <asp:Panel ID="pnlReporte" runat="server" ScrollBars="Both">

                        <rsweb:ReportViewer ID="rptFactCliente" runat="server" Width="90%" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">
                            <LocalReport ReportPath="rptReservacionesCliente.rdlc" >
                                <DataSources>
                                    <rsweb:ReportDataSource Name="dsReservCliente" DataSourceId="ObjectDataSource1"></rsweb:ReportDataSource>
                                </DataSources>
                            </LocalReport>
                        </rsweb:ReportViewer>
                    <asp:ObjectDataSource runat="server" SelectMethod="ReporteReservacionesPorCliente" TypeName="autoWebServices.AutoWebServiceReport" ID="ObjectDataSource1">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlCliente" PropertyName="SelectedValue" DefaultValue="" Name="idCliente" Type="String"></asp:ControlParameter>
                            <asp:ControlParameter ControlID="txtFechaInicio" PropertyName="Text" DefaultValue="" Name="inicio" Type="DateTime"></asp:ControlParameter>
                            <asp:ControlParameter ControlID="txtFechaFin" PropertyName="Text" DefaultValue="" Name="fin" Type="DateTime"></asp:ControlParameter>

                        </SelectParameters>
                    </asp:ObjectDataSource>
                    </asp:Panel>





                    </ContentTemplate>
            </asp:UpdatePanel>






</asp:Content>
