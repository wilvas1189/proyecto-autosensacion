﻿using autoWebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace autosSensacion
{
    public partial class cancelarReservacionCliente : System.Web.UI.Page
    {
        public AutoWebService servicio = new AutoWebService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                datosReservaciones();
            }
        }
        private void datosReservaciones()
        {
            Session.Add("usuario", "123");
            grvCancelarReservacion.DataSource = servicio.SeleccionarReservacionesNoCanceladasPorCliente(Session["usuario"].ToString());
            grvCancelarReservacion.DataBind();
        }

        protected void grvCancelarReservacion_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grvCancelarReservacion.EditIndex = -1;
            datosReservaciones();
        }

        protected void grvCancelarReservacion_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grvCancelarReservacion.EditIndex = e.NewEditIndex;
            datosReservaciones();
        }

        protected void grvCancelarReservacion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlCancelada = (e.Row.FindControl("ddCanceladaE") as DropDownList);
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    List<ListItem> items = new List<ListItem>();

                    string idE = (DataBinder.Eval(e.Row.DataItem, "cancelada")).ToString();
                    if (Convert.ToBoolean(idE) == false)
                    {
                        ListItem si = new ListItem("Si", "Si");
                        ListItem no = new ListItem("No", "No");
                        no.Selected = true;
                        items.Add(si);
                        items.Add(no);
                        ddlCancelada.DataSource = items;
                        ddlCancelada.DataBind();
                        ddlCancelada.SelectedValue = no.Value;
                    }
                    else
                    {
                        ListItem si = new ListItem("Si", "Si");
                        si.Selected = true;
                        items.Add(si);
                        ddlCancelada.DataSource = items;
                        ddlCancelada.DataBind();
                    }
                }
            }
        }

        protected void grvCancelarReservacion_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int id = Convert.ToInt16((grvCancelarReservacion.DataKeys[e.RowIndex].Values[0]).ToString());
            string respuesta = ((DropDownList)grvCancelarReservacion.Rows[e.RowIndex].FindControl("ddCanceladaE")).SelectedValue;

            if (respuesta.Equals("Si"))
            {
                servicio.CancelarReservacion(id, true);
            }

            grvCancelarReservacion.EditIndex = -1;
            datosReservaciones();
        }
    }
}