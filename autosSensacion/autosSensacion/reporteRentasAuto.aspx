﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterEmpleado.Master" AutoEventWireup="true" CodeBehind="reporteRentasAuto.aspx.cs" Inherits="autosSensacion.reporteRentasAuto" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <header class="dark">
        <h5>REPORTES</h5>
    </header>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="collapse2" class="body">
                <div class="form-group">
                    <label class="control-label col-lg-4">Placa del Automóvil:</label>
                    <div class="col-lg-4">
                        <asp:DropDownList ID="ddlPlaca" runat="server" Width="200px" AppendDataBoundItems="true">
                            <asp:ListItem Value="0">--Seleccione la Placa--</asp:ListItem>
                        </asp:DropDownList>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="#cc0000" runat="server" InitialValue="0"
                            ErrorMessage="* Seleccione la Placa" ControlToValidate="ddlPlaca">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4">Fecha Inicio:</label>
                    <div class="col-lg-4">
                        <asp:TextBox ID="txtFechaInicio" runat="server"></asp:TextBox>
                        
                        <ajaxToolkit:CalendarExtender Format="dd/MM/yyyy"
                            BehaviorID="txtFechaInicio_CalendarExtender" TargetControlID="txtFechaInicio" ID="txtFechaInicio_CalendarExtender" runat="server" />
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ForeColor="#cc0000" runat="server" ErrorMessage="* Seleccione la Fecha" ControlToValidate="txtFechaInicio"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-4">Fecha Fin:</label>
                    <div class="col-lg-4">
                        <asp:TextBox ID="txtFechaFin" runat="server"></asp:TextBox>

                        <ajaxToolkit:CalendarExtender Format="dd/MM/yyyy"
                            BehaviorID="txtFechaFin_CalendarExtender" TargetControlID="txtFechaFin" ID="txtFechaFin_CalendarExtender" runat="server" />
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="#cc0000" runat="server" ErrorMessage="* Seleccione la Fecha" ControlToValidate="txtFechaFin"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-4"></label>
                    <div class="col-lg-4">
                        <asp:Button ID="btnMostrar" runat="server" Text="Mostrar" OnClick="btnMostrar_Click" />
                    </div>
                </div>

                <div class="form-group">
                    <asp:Panel runat="server" ID="pnl1" ScrollBars="Both">

                     <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">
                        <LocalReport ReportPath="reporteRentaAutos.rdlc">
                            <DataSources>
                                <rsweb:ReportDataSource Name="dsRenta" DataSourceId="ObjectDataSource1"></rsweb:ReportDataSource>
                            </DataSources>
                        </LocalReport>
                    </rsweb:ReportViewer>

                    <asp:ObjectDataSource runat="server" SelectMethod="ObtenerAlquilerFiltro" TypeName="autoWebServices.AutoWebService" ID="ObjectDataSource1">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlPlaca" PropertyName="SelectedValue" DefaultValue="" Name="placa" Type="String"></asp:ControlParameter>
                            <asp:ControlParameter ControlID="txtFechaInicio" PropertyName="Text" DefaultValue="" Name="inicio" Type="DateTime"></asp:ControlParameter>
                            <asp:ControlParameter ControlID="txtFechaFin" PropertyName="Text" DefaultValue="" Name="fin" Type="DateTime"></asp:ControlParameter>

                        </SelectParameters>
                    </asp:ObjectDataSource>
                        </asp:Panel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
