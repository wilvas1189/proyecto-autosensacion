﻿using autoEntidades;
using autoWebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace autosSensacion
{
    public partial class registrarGastos : System.Web.UI.Page
    {
        public AutoWebService servicio = new AutoWebService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                llenarListas();
                lblMsj.Text = "";

            }
        }

        private void llenarListas()
        {
            List<personaEntidad> empleados = new List<personaEntidad>();
            empleados = servicio.ObtenerTodosEmpleados();
            ddlEmpleado.DataSource = empleados;
            ddlEmpleado.DataTextField = "Nombre";
            ddlEmpleado.DataValueField = "id";
            ddlEmpleado.DataBind();

            List<tipoGastoEntidad> gastos = new List<tipoGastoEntidad>();
            gastos = servicio.ObtenerTodosTipoGasto();
            ddlTipoGasto.DataSource = gastos;
            ddlTipoGasto.DataTextField = "descripcion";
            ddlTipoGasto.DataValueField = "id";
            ddlTipoGasto.DataBind();

        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            gastoEntidad gastos = new gastoEntidad();

            gastos.monto =Convert.ToDouble( txtMonto.Text);
            gastos.descripcion = txtDescripcion.Text;
            gastos.fechaGasto = Convert.ToDateTime(txtFecha.Text);
            gastos.fechaRegistro = DateTime.Today;
            gastos.idEmpleado = ddlEmpleado.SelectedValue;
            gastos.idTipoGasto =Convert.ToInt16( ddlTipoGasto.SelectedValue);

            servicio.insertarGasto(gastos);
            lblMsj.Text = "Datos guardados correctamente";

        }
    }
}