﻿using autoEntidades;
using autoWebServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace autosSensacion
{
    public partial class modiAutos : System.Web.UI.Page
    {
        public AutoWebService servicio = new AutoWebService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                datosAutomoviles();
            }
        }
        private void datosAutomoviles()
        {
            grvAutos.DataSource = servicio.ObtenerTodosAutos();
            grvAutos.DataBind();
        }

        protected void grvAutos_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grvAutos.EditIndex = -1;
            
            datosAutomoviles();
        }

        protected void grvAutos_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grvAutos.EditIndex = e.NewEditIndex;
            datosAutomoviles();
        }

        protected void grvAutos_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlMarcas = (e.Row.FindControl("ddlMarcaE") as DropDownList);
                DropDownList ddlTipoAutos = (e.Row.FindControl("ddlTipoAutoE") as DropDownList);
                DropDownList ddlEstado = (e.Row.FindControl("ddlEstaActivoE") as DropDownList);
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {

                    ddlMarcas.DataSource = servicio.ObtenerTodasMarcas();
                    ddlMarcas.DataTextField = "descripcion";
                    ddlMarcas.DataValueField = "id";
                    ddlMarcas.DataBind();
                    string id = (DataBinder.Eval(e.Row.DataItem, "idMarcaAuto")).ToString();
                    ddlMarcas.SelectedValue = id;

                    ddlTipoAutos.DataSource = servicio.ObtenerTodosTipo();
                    ddlTipoAutos.DataTextField = "descripcion";
                    ddlTipoAutos.DataValueField = "id";
                    ddlTipoAutos.DataBind();
                    string idA = (DataBinder.Eval(e.Row.DataItem, "idTipoAuto")).ToString();
                    ddlTipoAutos.SelectedValue = idA;

                    string idE = (DataBinder.Eval(e.Row.DataItem, "estaActivo")).ToString();
                    if(Convert.ToBoolean(idE) == false)
                    {
                        ddlEstado.SelectedValue = "2";
                    }else
                    {
                        ddlEstado.SelectedValue = "1";
                    }
                }
            }

        }

        protected void grvAutos_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            automovilEntidad autos = new automovilEntidad();

            FileUpload foto = (FileUpload)grvAutos.Rows[e.RowIndex].FindControl("fileFoto");
            if (foto.HasFile)
            {
                using (BinaryReader reader = new BinaryReader(foto.PostedFile.InputStream))
                {
                    autos.fotografia = reader.ReadBytes(foto.PostedFile.ContentLength);
                }
            }
            else {
                autos.fotografia = null;
            }



            autos.placa = Convert.ToString(grvAutos.DataKeys[e.RowIndex].Values[0]);
            autos.idMarcaAuto = Convert.ToInt16(((DropDownList)grvAutos.Rows[e.RowIndex].FindControl("ddlMarcaE")).SelectedValue);
            autos.modelo = ((TextBox)grvAutos.Rows[e.RowIndex].FindControl("txtModeloE")).Text;
            autos.cilindraje = Convert.ToDecimal(((TextBox)grvAutos.Rows[e.RowIndex].FindControl("txtCilindrajeE")).Text);
            autos.numPasajeros = Convert.ToDecimal(((TextBox)grvAutos.Rows[e.RowIndex].FindControl("txtPasajerosE")).Text);
            autos.idTipoAuto = Convert.ToInt16(((DropDownList)grvAutos.Rows[e.RowIndex].FindControl("ddlTipoAutoE")).SelectedValue);

            int valor = Convert.ToInt16(((DropDownList)grvAutos.Rows[e.RowIndex].FindControl("ddlEstaActivoE")).SelectedValue);
            if (valor == 1)
            {
                autos.estaActivo = true;
            }
            else
            {
                autos.estaActivo = false;
            }
            servicio.modificarAutomoviles(autos);

            grvAutos.EditIndex = -1;
            datosAutomoviles();
        }
    }
}