﻿using autoEntidades;
using autoWebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace autosSensacion
{
    public partial class Login : System.Web.UI.Page
    {
        public AutoWebService servicio = new AutoWebService();

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["nombreEmpleado"] = null;
        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                personaEntidad per = new personaEntidad();
                empleadoEntidad emp = new empleadoEntidad();

                per = servicio.ObtenerCliente(txtUsuario.Text, txtContraseña.Text);
                emp = servicio.ObtenerEmpleado(txtUsuario.Text, txtContraseña.Text);
                if (per.Nombre != null)
                {
                    Session["nombreEmpleado"] = txtUsuario.Text;
                   
                    Response.Redirect("principalCliente.aspx");

                }
                else
                {
                    if (emp.Nombre != null && emp.estaActivo==true)
                    {
                        Session["nombreEmpleado"] = txtUsuario.Text;
                        Response.Redirect("principalAdministrador.aspx");
                    }
                    else
                    {
                        lblMensaje.Text = "Usuario o contraseña no validos";
                        //Response.Redirect("Login.aspx");
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                personaEntidad persona = new personaEntidad();
                persona = servicio.ObtenerPersona(txtCedula.Text);
                clienteEntidad cliente = new clienteEntidad();
                if (persona.id != null)
                {
                    lblMensaje.Text = "* La persona ya se encuentra registrada";
                }
                else
                {
                    if (txtPassword.Text==txtConfirmacion.Text)
                    {
                        persona.id = txtCedula.Text;
                        persona.Nombre = txtNombre.Text;
                        persona.Apellido1 = txtAp1.Text;
                        persona.Apellido2 = txtAp2.Text;
                        persona.fechaNacimiento = Convert.ToDateTime(txtFecha.Text);
                        persona.telContacto = txtTelefono.Text;
                        persona.correo = txtEmail.Text;

                        cliente.idPersona = txtCedula.Text;
                        cliente.fechaIngreso = DateTime.Today;
                        cliente.contrasenna = txtPassword.Text;
                        cliente.idRolUsuario = 1;

                        servicio.insertarPersona(persona);
                        servicio.insertarCliente(cliente);
                        lblMensaje.Text = "* Datos registrados correctamente";
                    }
                    else
                    {
                        lblMensaje.Text = "* La contraseña no coincide";
                    }
                }
            }
        }
    }
}