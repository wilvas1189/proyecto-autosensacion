﻿using autoEntidades;
using autoWebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace autosSensacion
{
    public partial class mantEmpleados : System.Web.UI.Page
    {
        public AutoWebService servicio = new AutoWebService();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (IsValid) {
                personaEntidad persona = new personaEntidad();
                persona = servicio.ObtenerPersona(txtCedula.Text);

                empleadoEntidad empleado = new empleadoEntidad();
                if (persona.id != null)
                {
                    lblMsj.Text = "La persona ya se encuentra en el registro";
                }
                else
                {
                    persona.id = txtCedula.Text;
                    persona.Nombre = txtNombre.Text;
                    persona.Apellido1 = txtAp1.Text;
                    persona.Apellido2 = txtAp2.Text;
                    persona.fechaNacimiento = Convert.ToDateTime(txtFecha.Text);
                    persona.telContacto = txtTelefono.Text;
                    persona.correo = txtEmail.Text;

                    empleado.idPersona = txtCedula.Text;
                    empleado.fechaContratacion = DateTime.Today;
                    empleado.contrasenna = txtContraseña.Text;
                    if (rdbSi.Checked)
                    {
                        empleado.estaActivo = true;
                    }
                    else
                    {
                        empleado.estaActivo = false;
                    }
                    empleado.idRolUsuario =Convert.ToInt16(ddlRol.SelectedValue);

                    servicio.insertarPersona(persona);
                    servicio.insertarEmpleado(empleado);

                    lblMsj.Text = "Datos insertados Correctamente";
                   
                }
            }

        }
    }
}