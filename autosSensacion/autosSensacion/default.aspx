﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterInicio.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="autosSensacion._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <!-- Carousel
    ================================================== -->
    <div style="margin-bottom:0px;" id="myCarousel" class="carousel slide">
	<!-- Inicio contenido del carousel -->
      <div class="carousel-inner">
	  
	  		<!-- Elemento del carousel 1-->
        <div class="item active">
          <img src="img/carousel/img1.jpg" alt=""/>
          <div class="container">
            <div class="carousel-caption">
              <h1>¡Bienvenidos a Autos Sensación!</h1>
              <p class="lead" >
                  Autos Sensacion es la compañía Alajuelense dedicada alquiler de autos en Costa Rica sin costos ocultos, con protección total, y seguro con cero deducible. Con la flota joven de vehiculos que no alcanzan 24 meses de antigüedad.
			   </p>
              <a class="btn btn-primary btn-large" style="background-color:#33cc33; border-color:#33cc33;color:white"  href="cotizacionDefault.aspx"><i class="icon-calendar"></i> Cotizar</a>
            </div>
          </div>
        </div>
		
		<!-- Elemento del carousel 2-->
        <div class="item">
          <img src="img/carousel/img2.jpg" alt="">
          <div class="container">
            <div class="carousel-caption">
              <h1>¡Sobre Nosotros!</h1>
              <p class="lead" >
                  MISIÓN: Brindar servicio de alquiler de vehiculos de acuerdo a las necesidades de nuestros Clientes, dando una sensación de confort y excelente servicio al cliente.
				</p>
                <p class="lead" >
                  VISIÓN: Ser líder y estar a la vanguardia en el mercado de alquiler de automoviles en Costa Rica y a mediano plazo a nivel centroamericano.
				</p>
              <a class="btn btn-primary btn-large" style="background-color:#33cc33; border-color:#33cc33;color:white"  href="cotizacionDefault.aspx"><i class="icon-calendar"></i> Cotizar</a>
            </div>
          </div>
        </div>
		
		<!-- Elemento del carousel 3-->
        <div class="item">
          <img src="img/carousel/img3.jpg"" alt="">
          <div class="container">
             <div class="carousel-caption">
              <h1>Servicios</h1>
              <p class="lead">

			  </p>
              <a class="btn btn-primary btn-large" style="background-color:#33cc33; border-color:#33cc33;color:white"  href="cotizacionDefault.aspx"><i class="icon-calendar"></i> Cotizar</a>
            </div>
          </div>
        </div>
      </div>
	  <!-- Fin contenido del carousel -->
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
    </div>
	<!-- Fin del carousel -->

</asp:Content>
