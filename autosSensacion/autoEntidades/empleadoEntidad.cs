﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoEntidades
{
  public class empleadoEntidad
    {
        public string idPersona { get; set; }
        public DateTime fechaContratacion { get; set; }
        public bool estaActivo { get; set; }
        public int idRolUsuario { get; set; }
        public string contrasenna { get; set; }

        public string Nombre { get; set; }

    }
}
