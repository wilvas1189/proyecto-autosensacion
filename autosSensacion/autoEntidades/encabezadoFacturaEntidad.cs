﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoEntidades
{
  public  class encabezadoFacturaEntidad
    {
        public int id { get; set; }
        public string idEmpleado { get; set; }
        public string idCliente { get; set; }
        public DateTime fecha { get; set; }
        public decimal idNumTarjeta { get; set; }

        public tarjetaPagoEntidad tarjeta { set; get; }
        public List<detalleFacturaEntidad> detallesFactura { set; get; }

        public encabezadoFacturaEntidad()
        {
            detallesFactura = new List<detalleFacturaEntidad>();
            tarjeta = new tarjetaPagoEntidad();
        }

        //public List<detalleFacturaEntidad> getDetallesFactura() {
        //    return this.detallesFactura;
        //}


        //public void setDetallesFactura(List<detalleFacturaEntidad> Lista)
        //{
        //    this.detallesFactura = Lista;
        //}
    }
}
