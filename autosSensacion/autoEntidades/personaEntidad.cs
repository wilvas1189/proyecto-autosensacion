﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoEntidades
{
   public class personaEntidad
    {
        public string id { get; set; }
        public string Nombre { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public DateTime fechaNacimiento { get; set; }
        public string telContacto { get; set; }
        public string correo { get; set; }


        public Boolean activo { get; set; }
        public string estaActivo { get; set; }
        public string contrasenna { get; set; }
        
    }
}
