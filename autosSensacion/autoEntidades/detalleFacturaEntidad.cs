﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoEntidades
{
   public class detalleFacturaEntidad
    {
        public int id { get; set; }
        public string idAutomovil { get; set; }
        public int idEncabezadoFactura { get; set; }
        public int cantidadDias { get; set; }
        public decimal precio { get; set; }
    }
}
