﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoEntidades
{
    public class tarjetaPagoEntidad
    {
        public decimal numTarjeta { get; set; }
        public string nombreTitular { get; set; }
        public int mes { get; set; }
        public int ano { get; set; }
        public int cvv { get; set; }
    }
}
