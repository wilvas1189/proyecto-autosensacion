﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoEntidades
{
    public class DTOReporteReservacionesCliente
    {
        public string idCliente { get; set; }
        public string Nombre { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public string idReservacion { get; set; }
        public DateTime fechaReservacion { get; set; }

        public string fechaEntrega { set; get; }

        public string fechaDevolion { get; set; }
        public string placa { get; set; }
        public string marcaAuto { get; set; }
        public string modeloAuto { get; set; }
        public string tipoAuto { get; set; }

        public string cancelada { set; get; }
        public string vencida { set; get; }
        public decimal totalReservacion { set; get; }

    }
}
