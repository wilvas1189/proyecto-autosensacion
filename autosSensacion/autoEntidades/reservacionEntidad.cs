﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoEntidades
{
   public class reservacionEntidad
    {
        public int id { get; set; }
        public string idCliente { get; set; }
        public DateTime fechaReservacion { get; set; }
        public DateTime fechaEntrega { get; set; }
        public DateTime fechaDevolucion { get; set; }
        public string idAuto { get; set; }
        public decimal totalReservacion { get; set; }
        public bool cancelada { get; set; }
        public bool vencida { get; set; }
        public string descripcion { set; get; }

        public string stringCancelada { set; get; }

        public int idFactura { set; get; }

        public string marcaAuto { get; set; }
        public string tipoAuto { get; set; }
        public string cantDias { get; set; }
    }
}
