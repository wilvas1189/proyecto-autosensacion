﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoEntidades
{
    public class DTOReporteFactCliente
    {
        public string idCliente { get; set; }
        public string Nombre { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public string telContacto { get; set; }
        public string correo { get; set; }

        public string numFactura { set; get; }
        public DateTime fechaFactura { set; get; }
        
        public string idEmpleado { get; set; }
        public string fecha { get; set; }
        public string numeroTarjeta { get; set; }
        public string idDetalle { get; set; }
        public string idAutomovil { get; set; }

        public string tipoAuto { set; get; }
        public string marca { set; get; }
        public string modelo { set; get; }
        public  string activa { set; get; }
        public decimal monto { set; get; }
    }
}
