﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoEntidades
{
   public class automovilEntidad
    {
        public string placa { get; set; }
        public string VIN { get; set; }
        public int idMarcaAuto { get; set; }
        public string modelo { get; set; }
        public decimal cilindraje { get; set; }
        public decimal numPasajeros { get; set; }
        public int idTipoAuto { get; set; }
        public bool estaActivo { get; set; }

        public string activo { get; set; }

        public string descripcion { get; set; }
        public string tipoAuto { get; set; }

        public byte[] fotografia { set; get; }
        public string fotoString { set; get; }
     }
}
