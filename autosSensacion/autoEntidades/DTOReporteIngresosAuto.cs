﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoEntidades
{
    public class DTOReporteIngresosAuto
    {

        public string placa { get; set; }
        public string Vin { get; set; }
        public string TipoAuto { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string idFactura { get; set; }
        public DateTime FechaFactura { get; set; }
        public decimal Monto { set; get; }
    }
}
