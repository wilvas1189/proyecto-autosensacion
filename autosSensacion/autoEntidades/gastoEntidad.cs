﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autoEntidades
{
   public class gastoEntidad
    {
        public int id { get; set; }
        public string descripcion { get; set; }
        public double monto { get; set; }
        public DateTime fechaGasto { get; set; }
        public DateTime fechaRegistro { get; set; }
        public string idEmpleado { get; set; }
        public int idTipoGasto { get; set; }

        public string Nombre { get; set; }
        public string tipoGasto { get; set; }
    }
}
